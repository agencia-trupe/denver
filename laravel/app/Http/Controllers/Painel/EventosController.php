<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EventosRequest;
use App\Http\Controllers\Controller;

use App\Models\Evento;

class EventosController extends Controller
{
    public function index()
    {
        $registros = Evento::ordenados()->get();

        return view('painel.eventos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.eventos.create');
    }

    public function store(EventosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Evento::upload_capa();

            Evento::create($input);

            return redirect()->route('painel.eventos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Evento $registro)
    {
        return view('painel.eventos.edit', compact('registro'));
    }

    public function update(EventosRequest $request, Evento $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Evento::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.eventos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Evento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.eventos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
