<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
            'texto_1_pt' => 'required',
            'texto_1_en' => '',
            'texto_1_es' => '',
            'texto_2_pt' => 'required',
            'texto_2_en' => '',
            'texto_2_es' => '',
            'texto_3_pt' => 'required',
            'texto_3_en' => '',
            'texto_3_es' => '',
            'missao_pt' => 'required',
            'missao_en' => '',
            'missao_es' => '',
            'visao_pt' => 'required',
            'visao_en' => '',
            'visao_es' => '',
            'valores_pt' => 'required',
            'valores_en' => '',
            'valores_es' => '',
            'box_titulo_pt' => 'required',
            'box_titulo_en' => '',
            'box_titulo_es' => '',
            'box_texto_pt' => 'required',
            'box_texto_en' => '',
            'box_texto_es' => '',
        ];
    }
}
