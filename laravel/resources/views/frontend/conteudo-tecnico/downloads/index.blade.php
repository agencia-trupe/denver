@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="downloads">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.downloads') }}
        </h1>

        <div class="grid">
            <div class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('conteudo-tecnico.downloads', $cat->slug) }}" @if($cat->id === $categoria->id) class="active" @endif>
                    {{ tobj($cat, 'titulo') }}
                </a>
                @endforeach
            </div>

            @foreach($downloads as $download)
            @if($download->categoria->exige_cadastro)
                <a href="{{ route('conteudo-tecnico.downloads.lead', $download->id) }}" class="handle-downloads-lead">
            @else
                <a href="{{ asset('arquivos/downloads/'.$download->arquivo) }}" target="_blank">
            @endif
                    <img src="{{ asset('assets/img/downloads/'.$download->capa) }}" alt="">
                    <span>{{ t('conteudo-tecnico.fazer-download') }}</span>
                    <p>{{ tobj($download, 'titulo') }}</p>
                </a>
            @endforeach
        </div>

        {!! $downloads->render() !!}
    </div>

@endsection
