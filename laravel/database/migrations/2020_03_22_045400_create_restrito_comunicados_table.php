<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestritoComunicadosTable extends Migration
{
    public function up()
    {
        Schema::create('restrito_comunicados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('perfil');
            $table->string('data');
            $table->string('titulo');
            $table->string('imagem');
            $table->string('arquivo');
            $table->boolean('ativo')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('restrito_comunicados');
    }
}
