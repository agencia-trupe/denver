<?php

namespace App\Helpers;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($input, $path = 'arquivos/')
    {
        if (! $file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_'.date('YmdHis');
        $fileName .= str_random(10);
        $fileName .= '.'.$file->getClientOriginalExtension();

        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    public static function fileStorageUpload($input, $path = 'arquivos/')
    {
        if (! $file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_'.date('YmdHis');
        $fileName .= str_random(10);
        $fileName .= '.'.$file->getClientOriginalExtension();

        $file->move(storage_path('app/'.$path), $fileName);

        return $fileName;
    }

    public static function parseLink($url)
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? 'http://'.$url : $url;
    }

    public static function mesExtenso($mes)
    {
        $meses = [
            'pt' => [
                'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
                'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
            ],
            'en' => [
                'January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'
            ],
            'es' => [
                'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
            ],
        ];

        return $meses[app()->getLocale()][$mes - 1];
    }

    public static function mesAbreviado($mes)
    {
        return substr(self::mesExtenso($mes), 0, 3);
    }

    public static function fetchCoordinates($address)
    {
        $address = str_replace(' ', '+', $address);

        $url = 'https://maps.google.com/maps/api/geocode/json?key='.env('GOOGLE_API_KEY').'&address='.urlencode($address);

        try {
            $responseGoogle = file_get_contents($url);
        } catch(\Exception $e) {
            throw new \Exception('Ocorreu um problema ao encontrar o CEP.');
        }

        $responseGoogle = json_decode($responseGoogle);

        if (! count($responseGoogle->results)) {
            throw new \Exception('CEP não encontrado.');
        }

        $coordinates = $responseGoogle->results[0]->geometry->location;

        return $coordinates;
    }

    public static function restritoPerfis()
    {
        return [
            'tecnico' => 'Representante Técnico',
            'varejo'  => 'Representante Varejo',
        ];
    }
}
