@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Revendedores /</small> Editar Revendedor</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.revendedores.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.revendedores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
