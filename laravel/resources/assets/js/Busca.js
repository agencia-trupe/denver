export default function Busca() {
    $('.busca').click(event => {
        event.preventDefault();

        $('.busca-modal').fadeIn(() => {
            $('.busca-modal input[type=text]').focus();
        });

        $('body').css('overflow', 'hidden');
    });

    $('.busca-modal').click(event => {
        event.preventDefault();

        $('.busca-modal').fadeOut();
        $('body').css('overflow', 'auto');
    });

    $('.busca-modal form').click(event => {
        event.stopPropagation();
    });
}
