<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('data_pt');
            $table->string('data_en');
            $table->string('data_es');
            $table->string('capa');
            $table->text('titulo_pt');
            $table->text('titulo_en');
            $table->text('titulo_es');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->string('telefone');
            $table->string('site');
            $table->boolean('exibir_formulario')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('eventos');
    }
}
