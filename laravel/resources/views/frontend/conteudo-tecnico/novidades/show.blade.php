@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="novidades">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.novidades') }}
        </h1>

        <div class="container">
            <div class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('conteudo-tecnico.novidades', $cat->slug) }}" @if($cat->id === $categoria->id) class="active" @endif>
                    {{ tobj($cat, 'titulo') }}
                </a>
                @endforeach
            </div>

            <div class="novidade">
                <div class="capa">
                    <img src="{{ asset('assets/img/novidades/'.$novidade->capa) }}" alt="">
                    <div>
                        <span>{{ tobj($novidade->categoria, 'titulo') }}</span>
                        <h2>{{ tobj($novidade, 'titulo') }}</h2>
                    </div>
                </div>

                <div class="texto">{!! tobj($novidade, 'texto') !!}</div>
            </div>
        </div>

        @if(count($relacionados))
        <div class="relacionados">
            <h4>{{ t('conteudo-tecnico.mais-novidades') }}</h4>

            <div class="thumbs">
                @foreach($relacionados as $novidade)
                <a href="{{ route('conteudo-tecnico.novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/novidades/thumbs/'.$novidade->capa) }}" alt="">
                    </div>
                    <div class="info">
                        <span>{{ tobj($novidade->categoria, 'titulo') }}</span>
                        <p>{{ tobj($novidade, 'titulo') }}</p>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
        @endif
    </div>

@endsection
