<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalheTecnico extends Model
{
    protected $table = 'detalhes_tecnicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('detalhes_tecnicos_categoria_id', $categoria_id);
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'titulo_'.$locale
        ];

        return $query->where(function($q) use ($fields, $value) {
            foreach($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('titulo_'.$locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = asset('arquivos/detalhes-tecnicos/'.$this->arquivo);
        $obj->label = tobj($this, 'titulo');

        return $obj;
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\DetalheTecnicoCategoria', 'detalhes_tecnicos_categoria_id');
    }
}
