-- alterações 12/08/2021
--
-- Table structure for table `aceite_de_cookies`
--

DROP TABLE IF EXISTS `aceite_de_cookies`;
CREATE TABLE `aceite_de_cookies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aceite_de_cookies`
--

LOCK TABLES `aceite_de_cookies` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `configuracoes`
--
ALTER TABLE configuracoes CHANGE analytics analytics_ua varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL;
ALTER TABLE configuracoes ADD COLUMN analytics_g varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL AFTER analytics_ua;
ALTER TABLE configuracoes ADD COLUMN codigo_gtm varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL AFTER analytics_g;

-- alterações 27/10/2021
alter table produtos add column ordem int(11) NOT NULL DEFAULT '0' after id;