@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Área Restrita /</small>
            Downloads
            <a href="{{ route('painel.restrito.downloads.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Download</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/restrito/downloads']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.restrito.downloads.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os registros.
            </p>
        </div>
        @endif
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="restrito_downloads">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Perfil</th>
                <th>Título</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                @if(!$filtro)
                <td>
                    @if($registro->categoria)
                    {{ $registro->categoria->titulo }}
                    @else
                    <span class="label label-warning">sem categoria</span>
                    @endif
                </td>
                @endif
                <td>
                    @foreach(explode(',', $registro->perfil) as $perfil)
                    <small style="display:block">{{ Tools::restritoPerfis()[$perfil] }}</small>
                    @endforeach
                </td>
                <td>{{ $registro->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.restrito.downloads.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.restrito.downloads.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
