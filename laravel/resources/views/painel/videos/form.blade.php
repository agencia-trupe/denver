@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/videos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video_tipo', 'Vídeo (Tipo)') !!}
    {!! Form::select('video_tipo', [
        'youtube' => 'YouTube',
        'vimeo'   => 'Vimeo',
    ], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Vídeo (código)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.videos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
