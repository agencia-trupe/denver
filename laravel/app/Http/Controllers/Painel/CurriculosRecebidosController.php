<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use App\Models\Curriculo;

class CurriculosRecebidosController extends Controller
{
    public function index()
    {
        $registros = Curriculo::orderBy('created_at', 'DESC')->paginate(20);

        return view('painel.contato.curriculos.index', compact('registros'));
    }

    public function show($arquivo)
    {
        $filePath = storage_path('app/curriculos/'.$arquivo);

        if (file_exists($filePath)) {
            return response()->file($filePath);
        }

        return response('Arquivo não encontrado.', 404);
    }

    public function destroy(Curriculo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.contato.curriculos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
