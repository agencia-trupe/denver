<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests\Restrito\TreinamentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Restrito\Treinamento;

use App\Helpers\Tools;

class TreinamentosController extends Controller
{
    public function index()
    {
        $registros = Treinamento::ordenados()->get();

        return view('painel.restrito.treinamentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.restrito.treinamentos.create');
    }

    public function store(TreinamentosRequest $request)
    {
        try {

            $input = $request->all();

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['capa'])) $input['capa'] = Treinamento::upload_capa();

            Treinamento::create($input);

            return redirect()->route('painel.restrito.treinamentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Treinamento $registro)
    {
        return view('painel.restrito.treinamentos.edit', compact('registro'));
    }

    public function update(TreinamentosRequest $request, Treinamento $registro)
    {
        try {

            $input = $request->all();

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['capa'])) $input['capa'] = Treinamento::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.restrito.treinamentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Treinamento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.restrito.treinamentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
