<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultorVirtualTable extends Migration
{
    public function up()
    {
        Schema::create('consultor_virtual', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('tipo');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->timestamps();
        });

        Schema::create('consultor_virtual_indicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consultoria_id')->unsigned();
            $table->foreign('consultoria_id')->references('id')->on('consultor_virtual')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->timestamps();
        });

        Schema::create('consultor_virtual_indicacao_produto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('indicacao_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->timestamps();
            $table->foreign('indicacao_id')->references('id')->on('consultor_virtual_indicacoes')->onDelete('cascade');
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('consultor_virtual_indicacao_produto');
        Schema::drop('consultor_virtual_indicacoes');
        Schema::drop('consultor_virtual');
    }
}
