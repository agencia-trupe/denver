<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoAplicacao;

class ProdutosController extends Controller
{
    private $aplicacoes;

    public function __construct()
    {
        $this->aplicacoes = ProdutoAplicacao::orderBy('titulo_pt', 'ASC')->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $registros = Produto::ordenados();
        $aplicacoes = $this->aplicacoes;

        if ($busca = $request->query('busca')) {
            $registros = $registros->where('titulo_pt', 'LIKE', "%$busca%");
        }
        if ($aplicacaoId = $request->query('aplicacao')) {
            $registros = $registros->whereHas('aplicacoes', function($q) use ($aplicacaoId) {
                return $q->where('produto_aplicacao_id', $aplicacaoId);
            });
        }

        $registros = $registros->get();

        return view('painel.produtos.index', compact('registros', 'aplicacoes'));
    }

    public function create()
    {
        $aplicacoes = $this->aplicacoes;

        return view('painel.produtos.create', compact('aplicacoes'));
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input      = $request->except('aplicacoes');
            $aplicacoes = ($request->get('aplicacoes') ?: []);

            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            $registro = Produto::create($input);
            $registro->aplicacoes()->sync($aplicacoes);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $registro)
    {
        $aplicacoes = $this->aplicacoes;

        return view('painel.produtos.edit', compact('registro', 'aplicacoes'));
    }

    public function update(ProdutosRequest $request, Produto $registro)
    {
        try {

            $input = $request->except('aplicacoes');

            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            $registro->update($input);

            $aplicacoes = ($request->get('aplicacoes') ?: []);
            $registro->aplicacoes()->sync($aplicacoes);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $registro)
    {
        try {

            $registro->aplicacoes()->detach();
            $registro->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
