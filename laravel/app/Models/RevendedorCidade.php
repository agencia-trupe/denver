<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RevendedorCidade extends Model
{
    protected $table = 'revendedores_cidades';

    protected $guarded = ['id'];

    public function revendedores()
    {
        return $this
            ->belongsToMany('App\Models\Revendedor', 'revendedor_cidade', 'cidade_id', 'revendedor_id')
            ->orderBy('titulo', 'asc');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\RevendedorEstado', 'estado_id');
    }
}
