@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.videos.store', $produto->id], 'files' => true]) !!}

        @include('painel.produtos.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
