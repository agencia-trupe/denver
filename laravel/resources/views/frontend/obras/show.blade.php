@extends('frontend.common.template')

@section('content')

    <div class="obras">
        <div class="center">
            <h1 class="main-title">{{ t('nav.obras') }}</h1>

            <div class="obras-show">
                @if(count($obra->imagens))
                <div class="imagens">
                    @foreach($obra->imagens as $imagem)
                    <img src="{{ asset('assets/img/obras/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
                @endif
                <div class="descricao">
                    <h2>{{ tobj($obra, 'titulo') }}</h2>
                    <p>{!! tobj($obra, 'descricao') !!}</p>

                    @if(count($obra->produtos))
                    <div class="obra-produtos">
                        @foreach($obra->produtos as $relation)
                        <a href="{{ route('produtos.show', $relation->produto->slug) }}">
                            {{ tobj($relation->produto, 'titulo') }}
                        </a>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>

            <h3>{{ t('obras.confira') }}</h3>
            <div class="grid-show">
                @foreach($obras as $obra)
                <a href="{{ route('obras', $obra->slug) }}">
                    <img src="{{ asset('assets/img/obras/'.$obra->capa) }}" alt="">
                    <div class="overlay">{{ tobj($obra, 'titulo') }}</div>
                </a>
                @endforeach
            </div>
            <a href="{{ route('obras') }}" class="mais">
                {{ t('obras.mais') }} &raquo;
            </a>
        </div>
    </div>

@endsection
