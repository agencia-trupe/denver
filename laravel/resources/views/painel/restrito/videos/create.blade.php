@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Vídeos /</small> Adicionar Vídeo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.videos.store', 'files' => true]) !!}

        @include('painel.restrito.videos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
