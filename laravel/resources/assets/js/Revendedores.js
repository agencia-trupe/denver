export default function() {
    const $selectEstado = $('.revendedores select[name=estado]');
    const $radioTipo = $('.revendedores input[name=tipo]');
    const $selectCidade = $('.revendedores select[name=cidade]');

    $radioTipo.change(function changeTipo() {
        $('.revendedores select')
            .val('')
            .trigger('change');
    });

    $selectEstado.change(function changeEstado() {
        const estado = $(this).val();
        const tipo = $('.revendedores input[name=tipo]:checked').val();

        if (!estado) {
            return $selectCidade
                .attr('disabled', true)
                .find('option:not(:first-child)')
                .remove();
        }

        $selectCidade.attr('disabled', true);

        return $.post($(this).data('route'), { estado, tipo })
            .done(cidades => {
                $selectCidade
                    .find('option:not(:first-child)')
                    .remove()
                    .append.apply(
                        $selectCidade,
                        cidades.map(({ id, nome }) =>
                            $(`<option value="${id}">${nome}</option>`)
                        )
                    );
            })
            .fail(() => alert('Ocorreu um erro.'))
            .always(() => $selectCidade.attr('disabled', false));
    });

    $selectCidade.change(function changeCidade() {
        const cidade = $(this).val();

        if (!cidade) return;

        const route = $(this).data('route');
        const estado = $selectEstado.val();
        const tipo = $('.revendedores input[name=tipo]:checked').val();

        if (tipo && estado) {
            window.location = `${route}?tipo=${tipo}&estado=${estado}&cidade=${cidade}`;
        }
    });

    if (window.REVENDEDORES_SCROLL) {
        $('html, body').animate(
            {
                scrollTop: $('.revendedores')
                    .prev()
                    .offset().top,
            },
            300
        );
    }
}
