<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NovidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\Novidade;
use App\Models\NovidadeCategoria;

class NovidadesController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = NovidadeCategoria::ordenados()->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (NovidadeCategoria::find($filtro)) {
            $registros = Novidade::where('novidades_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Novidade::ordenados()->get();
        }

        return view('painel.novidades.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.novidades.create', compact('categorias'));
    }

    public function store(NovidadesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Novidade::upload_capa();

            Novidade::create($input);

            return redirect()->route('painel.novidades.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Novidade $registro)
    {
        $categorias = $this->categorias;

        return view('painel.novidades.edit', compact('registro', 'categorias'));
    }

    public function update(NovidadesRequest $request, Novidade $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Novidade::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.novidades.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Novidade $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.novidades.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
