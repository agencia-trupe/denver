@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Novidades /</small> Editar Novidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.novidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.novidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
