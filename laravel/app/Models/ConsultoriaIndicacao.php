<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultoriaIndicacao extends Model
{
    protected $table = 'consultor_virtual_indicacoes';

    protected $guarded = ['id'];

    public function produtos()
    {
        return $this
            ->belongsToMany(
                'App\Models\Produto',
                'consultor_virtual_indicacao_produto',
                'indicacao_id',
                'produto_id'
            )->ordenados();
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
