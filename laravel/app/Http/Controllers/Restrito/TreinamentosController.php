<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Models\Restrito\Treinamento;

class TreinamentosController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            $treinamento = Treinamento::findOrFail($id);

            return $treinamento->hasPerfil
                ? view('frontend.restrito.treinamentos.show', compact('treinamento'))
                : abort('404');
        }

        $treinamentos = Treinamento::perfil()->ordenados()->get();

        return view('frontend.restrito.treinamentos.index', compact('treinamentos'));
    }
}
