<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\InteresseRecebido;
use Illuminate\Database\Eloquent\Model;

class InteressesRecebidosController extends Controller
{
    public function index(Model $model, Request $request)
    {
        return view('painel.interesses.index', [
            'routeName'  => $request->route()->parameterNames()[0],
            'interesses' => $model->interesses()->orderBy('created_at', 'DESC')->paginate(15),
            'evento'     => $model
        ]);
    }

    public function destroy(Model $model, InteresseRecebido $interesse)
    {
        try {

            $interesse->delete();
            return back()->with('success', 'Interesse excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir interesse: '.$e->getMessage()]);

        }
    }
}
