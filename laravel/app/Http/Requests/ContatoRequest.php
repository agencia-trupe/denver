<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required',
            'email_curriculos' => 'email|required',
            'email_curriculos_restrito' => 'email|required',
            'telefone_duvidas_tecnicas' => 'required',
            'telefone_vendas' => 'required',
            'telefone_demais_regioes' => 'required',
            'telefone_sao_paulo' => 'required',
            'endereco_sao_paulo' => 'required',
            'google_maps_sao_paulo' => 'required',
            'telefone_feira_de_santana' => 'required',
            'endereco_feira_de_santana' => 'required',
            'google_maps_feira_de_santana' => 'required',
        ];
    }
}
