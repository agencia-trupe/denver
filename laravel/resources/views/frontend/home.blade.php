@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="hero" style="background-image:url({{ asset('assets/img/background/'.$bg->imagem) }})">
            <div class="center">
                <div class="actions">
                    <div>
                        <span>{{ t('home.problema') }}</span>
                        <div class="select">
                            <div class="select-title">{{ t('home.local') }}</div>
                            <ul>
                                @foreach($problemas as $problema)
                                <li>
                                    <a href="{{ route('conteudo-tecnico.consultor-virtual', ['problema', $problema->id]) }}">
                                        {{ tobj($problema, 'titulo') }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div>
                        <span>{{ t('home.linhas') }}</span>
                        <div class="select">
                            <div class="select-title">{{ t('home.tipo') }}</div>
                            <ul>
                                @foreach($aplicacoes as $aplicacao)
                                <li>
                                    <a href="{{ route('produtos', $aplicacao->slug) }}">
                                        {{ tobj($aplicacao, 'titulo') }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div>
                        <span>{{ t('home.nomes-produtos') }}</span>
                        <form action="{{ route('produtos') }}" method="GET">
                            <input type="text" name="busca" placeholder="{{ t('home.produto') }}">
                        </form>
                    </div>
                </div>

                <div class="banners">
                    @foreach($banners as $banner)
                        @if($banner->link)
                        <a href="{{ Tools::parseLink($banner->link) }}" class="banner">
                            <img src="{{ asset('assets/img/banners/'.tobj($banner, 'imagem')) }}" alt="">
                        </a>
                        @else
                        <div class="banner">
                            <img src="{{ asset('assets/img/banners/'.tobj($banner, 'imagem')) }}" alt="">
                        </div>
                        @endif
                    @endforeach
                    <div class="cycle-pager"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('outside-content')

    <div class="home">
        <div class="solucoes">
            <div class="center">
                <h2>{{ t('home.solucoes') }}</h2>

                <div class="grid">
                    @foreach($solucoes as $solucao)
                    <a href="{{ Tools::parseLink($solucao->link) }}">
                        <h3>{{ tobj($solucao, 'titulo') }}</h3>
                        <img src="{{ asset('assets/img/solucoes/'.$solucao->imagem) }}" alt="">
                        <p>{{ tobj($solucao, 'subtitulo') }}</p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="obras-home">
            <div class="center">
                <h2>{{ t('nav.obras') }}</h2>

                <div class="grid">
                    @foreach($obras as $obra)
                    <a href="{{ route('obras', $obra->slug) }}">
                        <img src="{{ asset('assets/img/obras/'.$obra->capa) }}" alt="">
                        <p>{{ tobj($obra, 'titulo') }}</p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="conteudo-tecnico-home">
            <div class="center">
                <h2>{{ t('home.conteudo-tecnico') }}</h2>

                <div class="links">
                    @foreach([
                        'novidades' => route('conteudo-tecnico.novidades'),
                        'calendario' => route('conteudo-tecnico.calendario-de-eventos'),
                        'videos' => route('conteudo-tecnico.videos'),
                        'downloads' => route('conteudo-tecnico.downloads'),
                        'faq' => route('conteudo-tecnico.perguntas-frequentes'),
                        'consultor' => route('conteudo-tecnico.consultor-virtual'),
                    ] as $c => $route)
                    <a href="{{ $route }}">
                        <img src="{{ asset('assets/img/layout/ico-aprenda-'.$c.'.svg') }}" alt="">
                        <span>{{ t('nav.'.$c) }}</span>
                    </a>
                    @endforeach
                </div>

                @if(count($novidades))
                <div class="novidades-home">
                    @foreach($novidades as $novidade)
                    <a href="{{ route('conteudo-tecnico.novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}">
                        <img src="{{ asset('assets/img/novidades/thumbs/'.$novidade->capa) }}" alt="">
                        <div class="overlay">
                            <div>
                                <span>{{ tobj($novidade->categoria, 'titulo') }}</span>
                                <p>{{ tobj($novidade, 'titulo') }}</p>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
                @endif
            </div>
        </div>

        <div class="newsletter">
            <div class="center">
                <div>
                    <h3>{{ t('newsletter.chamada') }}</h3>
                    <p>{{ t('newsletter.descricao') }}</p>
                </div>

                <form action="{{ route('newsletter') }}" method="POST" id="form-newsletter">
                    <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
                    <select name="perfil" required>
                        <option value="">{{ t('newsletter.perfil') }}</option>
                        @foreach(\App\Models\Newsletter::perfis() as $perfil)
                        <option value="{{ $perfil }}">{{ t('newsletter.perfis.'.$perfil) }}</option>
                        @endforeach
                    </select>
                    <button type="submit"></button>
                </form>
            </div>
        </div>
    </div>

    @if($popup->isActive())
        <script>
            var DENVER_POPUP = {
                type: '{{ $popup->getType() }}',
                media: '{{ $popup->getMedia() }}'
            };
        </script>
    @endif

@endsection
