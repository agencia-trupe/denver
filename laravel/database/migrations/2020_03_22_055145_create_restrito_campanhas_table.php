<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestritoCampanhasTable extends Migration
{
    public function up()
    {
        Schema::create('restrito_campanhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('perfil');
            $table->string('titulo');
            $table->string('capa');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('restrito_campanhas');
    }
}
