<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests\Restrito\VideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Restrito\Video;

use App\Helpers\Tools;

class VideosController extends Controller
{
    public function index()
    {
        $registros = Video::ordenados()->get();

        return view('painel.restrito.videos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.restrito.videos.create');
    }

    public function store(VideosRequest $request)
    {
        try {

            $input = $request->all();

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['capa'])) $input['capa'] = Video::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileStorageUpload('arquivo', 'restrito/videos/');
            }

            Video::create($input);

            return redirect()->route('painel.restrito.videos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Video $registro)
    {
        return view('painel.restrito.videos.edit', compact('registro'));
    }

    public function update(VideosRequest $request, Video $registro)
    {
        try {

            $input = $request->all();

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['capa'])) $input['capa'] = Video::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileStorageUpload('arquivo', 'restrito/videos/');
            }

            $registro->update($input);

            return redirect()->route('painel.restrito.videos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Video $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.restrito.videos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
