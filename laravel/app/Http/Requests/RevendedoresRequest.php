<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RevendedoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'tipo' => 'required|array|in:lojas,construtoras',
            'descricao' => 'required',
            'e_mail' => '',
            'site' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'lat' => 'latitude',
            'lng' => 'longitude',
        ];
    }
}
