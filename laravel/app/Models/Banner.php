<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem_pt()
    {
        return CropImage::make('imagem_pt', [
            'width'  => 940,
            'height' => 380,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_en()
    {
        return CropImage::make('imagem_en', [
            'width'  => 940,
            'height' => 380,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_es()
    {
        return CropImage::make('imagem_es', [
            'width'  => 940,
            'height' => 380,
            'path'   => 'assets/img/banners/'
        ]);
    }
}
