<div class="title">
    <h2>
        <span>Olá {{ auth('restrito')->user()->nome }}!</span>
        <div>
            Acesso restrito
            <a href="{{ route('restrito.logout') }}" class="btn-logout">
                SAIR
                <i></i>
            </a>
        </div>
    </h2>
</div>
