import Clipboard from './modules/Clipboard';
import DataTables from './modules/DataTables';
import DatePicker from './modules/DatePicker';
import DeleteButton from './modules/DeleteButton';
import Filtro from './modules/Filtro';
import GeneratorFields from './modules/GeneratorFields';
import ImagesUpload from './modules/ImagesUpload';
import MonthPicker from './modules/MonthPicker';
import MultiSelect from './modules/MultiSelect';
import OrderImages from './modules/OrderImages';
import OrderTable from './modules/OrderTable';
import TextEditor from './modules/TextEditor';
import RevendedoresCidades from './modules/RevendedoresCidades';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    },
});

Clipboard();
DataTables();
DatePicker();
DeleteButton();
Filtro();
GeneratorFields();
ImagesUpload();
MonthPicker();
MultiSelect();
OrderImages();
OrderTable();
TextEditor();
RevendedoresCidades();
