@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Institucional /</small>
            Empresa
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.empresa.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.empresa.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
