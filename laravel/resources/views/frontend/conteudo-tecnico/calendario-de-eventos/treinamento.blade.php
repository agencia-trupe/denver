@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="calendario-de-eventos">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.calendario') }}
        </h1>

        <div class="treinamento">
            <h3>{{ t('conteudo-tecnico.treinamentos') }}</h3>

            <div class="wrapper">
                <div class="data">
                    <?php list($dia, $mes, $ano) = explode('/', $treinamento->data); ?>
                    <span>{{ $dia }}</span>
                    <div>
                        <span>
                            {{ Tools::mesAbreviado($mes) }}
                        </span>
                        <span>
                            {{ $ano }}
                        </span>
                    </div>
                </div>

                <h2>{!! tobj($treinamento, 'titulo') !!}</h2>

                <div class="treinamento-content">
                    <div class="descricao">{!! tobj($treinamento, 'descricao') !!}</div>

                    @if($treinamento->telefone || $treinamento->site)
                    <div class="contato-info">
                        <h4>{{ t('nav.contato') }}</h4>
                        @if($treinamento->telefone)
                        <p class="telefone">{{ $treinamento->telefone }}</p>
                        @endif
                        @if($treinamento->site)
                        <a href="{{ Tools::parseLink($treinamento->site) }}" class="site" target="_blank">
                            {{ $treinamento->site }}
                        </a>
                        @endif
                    </div>
                    @endif

                    @if($treinamento->exibir_formulario)
                    <form action="{{ route('conteudo-tecnico.calendario-de-eventos.treinamento.interesse', $treinamento->id) }}" id="form-interesse">
                        <h4>{{ t('conteudo-tecnico.interesse') }}</h4>
                        <div>
                            <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required>
                            <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
                            <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}">
                            <button type="submit">
                                {{ t('contato.enviar') }}
                                {!! file_get_contents(resource_path('assets/img/ico-carta-enviar.svg')) !!}
                            </button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
