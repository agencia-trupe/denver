@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Banners /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.banners.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.banners.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
