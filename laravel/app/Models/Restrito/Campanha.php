<?php

namespace App\Models\Restrito;

use App\Helpers\CropImage;

class Campanha extends Model
{
    protected $table = 'restrito_campanhas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 140,
            'height' => 180,
            'path'   => 'assets/img/campanhas/'
        ]);
    }

    public function getFilePathAttribute()
    {
        return storage_path('app/restrito/campanhas/'.$this->arquivo);
    }
}
