<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDownloadsTableAddCadastroFrase extends Migration
{
    public function up()
    {
        Schema::table('downloads_categorias', function (Blueprint $table) {
            $table->boolean('exige_cadastro')->after('slug')->default(false);
        });

        Schema::table('downloads', function (Blueprint $table) {
            $table->text('texto_email')->after('arquivo');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::table('downloads_categorias', function (Blueprint $table) {
            $table->dropColumn('exige_cadastro');
        });

        Schema::table('downloads', function (Blueprint $table) {
            $table->dropColumn('texto_email');
            $table->dropColumn('deleted_at');
        });
    }
}
