<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('busca', 'HomeController@busca')->name('busca');
Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');

Route::get('empresa', 'EmpresaController@index')->name('empresa');

// Route::get('produtos/calculadora-de-consumo/{produto_slug?}', 'ProdutosController@calculadora')
//     ->name('produtos.calculadora');
Route::get('produtos/detalhes/{produto_slug}', 'ProdutosController@show')->name('produtos.show');
Route::get('produtos/{aplicacao_slug?}', 'ProdutosController@index')->name('produtos');

Route::get('detalhes-tecnicos/{detalhes_tecnicos_slug?}', 'DetalhesTecnicosController@index')
    ->name('detalhes-tecnicos');

Route::get('soprema-em-grandes-obras/{obra_slug?}', 'ObrasController@index')
    ->name('obras');

Route::get('conteudo-tecnico', function () {
    return redirect()->route('conteudo-tecnico.novidades');
})->name('conteudo-tecnico');

Route::get('conteudo-tecnico/novidades/{novidade_categoria_slug?}', 'NovidadesController@index')
    ->name('conteudo-tecnico.novidades');
Route::get('conteudo-tecnico/novidades/{novidade_categoria_slug?}/{novidade_slug}', 'NovidadesController@show')
    ->name('conteudo-tecnico.novidades.show');

Route::get('conteudo-tecnico/calendario-de-eventos', 'CalendarioDeEventosController@index')
    ->name('conteudo-tecnico.calendario-de-eventos');
Route::get('conteudo-tecnico/calendario-de-eventos/evento/{eventos}', 'CalendarioDeEventosController@evento')
    ->name('conteudo-tecnico.calendario-de-eventos.evento');
Route::post('conteudo-tecnico/calendario-de-eventos/evento/{eventos}', 'CalendarioDeEventosController@interesse')
    ->name('conteudo-tecnico.calendario-de-eventos.evento.interesse');
Route::get('conteudo-tecnico/calendario-de-eventos/treinamento/{treinamentos}', 'CalendarioDeEventosController@treinamento')
    ->name('conteudo-tecnico.calendario-de-eventos.treinamento');
Route::post('conteudo-tecnico/calendario-de-eventos/treinamento/{treinamentos}', 'CalendarioDeEventosController@interesse')
    ->name('conteudo-tecnico.calendario-de-eventos.treinamento.interesse');

Route::get('conteudo-tecnico/videos', 'VideosController@index')->name('conteudo-tecnico.videos');
Route::get('conteudo-tecnico/downloads/{download_categoria_slug?}', 'DownloadsController@index')->name('conteudo-tecnico.downloads');
Route::get('conteudo-tecnico/downloads/lead/{downloads}', 'DownloadsController@lead')->name('conteudo-tecnico.downloads.lead');
Route::post('conteudo-tecnico/downloads/lead/{downloads}', 'DownloadsController@leadPost')->name('conteudo-tecnico.downloads.lead.post');
Route::get('conteudo-tecnico/perguntas-frequentes/{faq_categoria_slug?}', 'PerguntasFrequentesController@index')->name('conteudo-tecnico.perguntas-frequentes');
Route::get('conteudo-tecnico/consultor-virtual/{tipo?}/{consultoria?}/{indicacao?}', 'ConsultorVirtualController@index')->name('conteudo-tecnico.consultor-virtual');

Route::get('contato', 'ContatoController@index')
    ->name('contato');
Route::post('contato', 'ContatoController@post')
    ->name('contato.post');
Route::post('contato/curriculo', 'ContatoController@postCurriculo')
    ->name('contato.post-curriculo');
Route::post('contato/representante', 'ContatoController@postRepresentante')
    ->name('contato.post-representante');
Route::get('contato/onde-encontrar', 'ContatoController@ondeEncontrar')
    ->name('contato.onde-encontrar');
Route::post('contato/onde-encontrar/cidades', 'ContatoController@buscaCidades')
    ->name('contato.onde-encontrar.cidades');

Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')
    ->name('politica-de-privacidade');
Route::get('termos-de-uso', 'TermosDeUsoController@index')
    ->name('termos-de-uso');

Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

Route::get('curriculos/arquivo/{arquivo}', function ($arquivo) {
    $filePath = storage_path('app/curriculos/' . $arquivo);

    if (file_exists($filePath)) {
        return response()->file($filePath);
    }

    return response('Arquivo não encontrado.', 404);
})->name('curriculos.public');
