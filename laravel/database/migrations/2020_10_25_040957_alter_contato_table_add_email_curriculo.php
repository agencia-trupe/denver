<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContatoTableAddEmailCurriculo extends Migration
{
    public function up()
    {
        Schema::table('contato', function (Blueprint $table) {
            $table->string('email_curriculos')->after('email');
            $table->string('email_curriculos_restrito')->after('email_curriculos');
        });
    }

    public function down()
    {
        Schema::table('contato', function (Blueprint $table) {
            $table->dropColumn('email_curriculos');
            $table->dropColumn('email_curriculos_restrito');
        });
    }
}
