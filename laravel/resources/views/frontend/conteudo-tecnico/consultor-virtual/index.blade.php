@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="consultor-virtual">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.consultor') }}
        </h1>

        <div class="selecao">
            <a href="{{ route('conteudo-tecnico.consultor-virtual', 'problema') }}">
                <h2>{!! t('conteudo-tecnico.problema') !!}</h2>
                <img src="{{ asset('assets/img/layout/img-consultorvirtual-problema.jpg') }}" alt="">
                <div class="btn">
                    {{ t('conteudo-tecnico.encontrar-solucoes') }}
                    <span></span>
                </div>
            </a>
            <a href="{{ route('conteudo-tecnico.consultor-virtual', 'solucao') }}">
                <h2>{!! t('conteudo-tecnico.solucao') !!}</h2>
                <img src="{{ asset('assets/img/layout/img-consultorvirtual-solucao.jpg') }}" alt="">
                <div class="btn">
                    {{ t('conteudo-tecnico.encontrar-solucoes') }}
                    <span></span>
                </div>
            </a>
        </div>
    </div>

@endsection
