export default function() {
    const $warning = $('.produto-embalagens .warning');
    const newTab = url => {
        $(`<a href="${url}" target="_blank"></a>`)[0].click();
    };

    $('input[name=embalagem]').change(() => {
        $warning.hide();
    });

    $('.link-online').click(() => {
        const $checked = $('input[name=embalagem]:checked');
        const linkOnline = $checked.data('link-online');

        if (!linkOnline) {
            $warning.show();
            return;
        }

        newTab(linkOnline);
    });

    $('.link-loja').click(() => {
        const $checked = $('input[name=embalagem]:checked');
        const linkLoja = $checked.data('link-loja');

        if (linkLoja) newTab(linkLoja);
    });
}
