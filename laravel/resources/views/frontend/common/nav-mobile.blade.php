<div class="nav-mobile">
    <div class="center">
        <a href="{{ route('home') }}" class="logo">
            <img src="{{ asset('assets/img/layout/marca-soprema-menu.svg') }}" alt="Denver">
        </a>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </div>

    <nav id="nav-mobile">
        <div class="center">
            <div class="links">
                <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>
                    {{ t('nav.empresa') }}
                </a>
                <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos*')) class="active" @endif>
                    {{ t('nav.produtos') }}
                </a>
                <a href="{{ route('detalhes-tecnicos') }}">{{ t('nav.detalhes-tecnicos') }}</a>
                <a href="{{ route('obras') }}" @if(Tools::routeIs('obras')) class="active" @endif>
                    {{ t('nav.obras') }}
                </a>
                <a href="{{ route('conteudo-tecnico') }}" @if(Tools::routeIs('conteudo-tecnico*')) class="active" @endif>{{ t('nav.conteudo-tecnico') }}</a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="active" @endif>
                    {{ t('nav.contato') }}
                </a>
            </div>

            <a href="#" class="busca">
                {!! file_get_contents(resource_path('assets/img/lupa-busca.svg')) !!}
                <span>{{ t('template.busca') }}</span>
            </a>

            <div class="social">
                @foreach(['facebook', 'instagram', 'youtube', 'linkedin'] as $s)
                    @if($contato->{$s})
                        <a href="{{ Tools::parseLink($contato->{$s}) }}" target="_blank" class="{{ $s }}">
                            {!! file_get_contents(resource_path('assets/img/redessociais-'.$s.'.svg')) !!}
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </nav>
</div>
