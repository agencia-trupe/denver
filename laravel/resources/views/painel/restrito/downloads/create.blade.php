@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Downloads /</small> Adicionar Download</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.downloads.store', 'files' => true]) !!}

        @include('painel.restrito.downloads.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
