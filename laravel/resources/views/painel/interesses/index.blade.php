@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.'.$routeName.'.index') }}" title="Voltar para {{ ucfirst($routeName) }}" class="btn btn-sm btn-default">
        &larr; Voltar para {{ ucfirst($routeName) }}
    </a>

    <legend>
        <h2>
            <small>{{ ucfirst($routeName) }} /</small>
            Interesses
        </h2>
        <h3>{!! $evento->titulo_pt !!}</h3>
    </legend>

    @if(!count($interesses))
    <div class="alert alert-warning" role="alert">Nenhum interesse recebido.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($interesses as $interesse)
            <tr>
                <td>{{ $interesse->created_at }}</td>
                <td>{{ $interesse->nome }}</td>
                <td>
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $interesse->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $interesse->email }}
                </td>
                <td>{{ $interesse->telefone }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.'.$routeName.'.interesses.destroy', $evento->id, $interesse->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @endif

    {!! $interesses->links() !!}

@stop
