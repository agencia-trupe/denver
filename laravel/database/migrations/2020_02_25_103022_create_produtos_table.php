<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('subtitulo_pt');
            $table->string('subtitulo_en');
            $table->string('subtitulo_es');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->string('imagem');
            $table->text('consumo_pt');
            $table->text('consumo_en');
            $table->text('consumo_es');
            $table->text('embalagem_pt');
            $table->text('embalagem_en');
            $table->text('embalagem_es');
            $table->text('usos_pt');
            $table->text('usos_en');
            $table->text('usos_es');
            $table->string('medida');
            $table->string('unidades_por_medida');
            $table->timestamps();
        });

        Schema::create('produtos_aplicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->timestamps();
        });

        Schema::create('produto_aplicacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('produto_aplicacao_id')->unsigned();
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos');
            $table->foreign('produto_aplicacao_id')->references('id')->on('produtos_aplicacoes');
        });
    }

    public function down()
    {
        Schema::drop('produto_aplicacao');
        Schema::drop('produtos_aplicacoes');
        Schema::drop('produtos');
    }
}
