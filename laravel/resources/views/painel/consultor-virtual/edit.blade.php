@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Consultor Virtual /</small> Editar Consultoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.consultor-virtual.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.consultor-virtual.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
