<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BackgroundRequest;
use App\Http\Controllers\Controller;

use App\Models\Background;

class BackgroundController extends Controller
{
    public function index()
    {
        $registro = Background::first();

        return view('painel.background.edit', compact('registro'));
    }

    public function update(BackgroundRequest $request, Background $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Background::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.background.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
