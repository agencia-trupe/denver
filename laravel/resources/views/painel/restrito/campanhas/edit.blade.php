@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Campanhas /</small> Editar Campanha</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.campanhas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.campanhas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
