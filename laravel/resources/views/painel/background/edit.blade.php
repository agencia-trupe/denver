@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Home /</small>
            Background
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.background.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.background.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
