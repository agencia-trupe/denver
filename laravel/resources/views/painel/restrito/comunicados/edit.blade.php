@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Comunicados /</small> Editar Comunicado</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.comunicados.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.comunicados.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
