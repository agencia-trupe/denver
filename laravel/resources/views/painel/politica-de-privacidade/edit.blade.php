@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Institucional /</small>
            Política de Privacidade
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.politica-de-privacidade.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.politica-de-privacidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
