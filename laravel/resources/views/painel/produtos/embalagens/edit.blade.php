@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Editar Embalagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.embalagens.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.produtos.embalagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
