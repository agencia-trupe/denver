@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Consultor Virtual /</small> Adicionar Consultoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.consultor-virtual.store', 'files' => true]) !!}

        @include('painel.consultor-virtual.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
