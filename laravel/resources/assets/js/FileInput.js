export default function() {
    $('.file-input').each(function fileEach(index, el) {
        $(el).data(
            'title',
            $(el)
                .find('span')
                .html()
        );
    });
    $('.file-input input').on('change', function fileChange(e) {
        if (e.target.files.length) {
            const fileName = e.target.files[0].name;
            $(this)
                .next()
                .html(fileName);
        } else {
            $(this)
                .next()
                .html(
                    $(this)
                        .parent()
                        .data('title')
                );
        }
    });
}
