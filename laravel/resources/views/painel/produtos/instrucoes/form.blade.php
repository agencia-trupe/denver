@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <div style="margin-bottom:5px">
        <a href="{{ asset('arquivos/produtos/instrucoes/'.$registro->arquivo) }}">{{ $registro->arquivo }}</a>
    </div>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.instrucoes.index', $produto) }}" class="btn btn-default btn-voltar">Voltar</a>
