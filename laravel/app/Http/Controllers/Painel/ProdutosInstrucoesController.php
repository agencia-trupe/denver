<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosInstrucoesRequest;

use App\Http\Controllers\Controller;

use App\Models\ProdutoInstrucao;
use App\Models\Produto;

use App\Helpers\Tools;

class ProdutosInstrucoesController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->instrucoes;

        return view('painel.produtos.instrucoes.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.instrucoes.create', compact('produto'));
    }

    public function store(ProdutosInstrucoesRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'arquivos/produtos/instrucoes/');
            }

            $produto->instrucoes()->create($input);

            return redirect()->route('painel.produtos.instrucoes.index', $produto)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, ProdutoInstrucao $registro)
    {
        return view('painel.produtos.instrucoes.edit', compact('produto', 'registro'));
    }

    public function update(ProdutosInstrucoesRequest $request, Produto $produto, ProdutoInstrucao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'arquivos/produtos/instrucoes/');
            }

            $registro->update($input);

            return redirect()->route('painel.produtos.instrucoes.index', $produto)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, ProdutoInstrucao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.instrucoes.index', $produto)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
