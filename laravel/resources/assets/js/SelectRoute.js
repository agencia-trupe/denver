export default function() {
    $('.handle-select-route').change(function selectRoute() {
        window.location = $(this)
            .find(`option[value=${this.value}]`)
            .data('route');
    });
}
