@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Adicionar Instrução</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.instrucoes.store', $produto->id], 'files' => true]) !!}

        @include('painel.produtos.instrucoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
