<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 590,
            'height' => 590,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 885,
            'height' => 590,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 295,
            'height' => 295,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 1500,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

}
