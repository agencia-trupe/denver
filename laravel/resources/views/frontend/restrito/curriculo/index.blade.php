@extends('frontend.restrito.template')

@section('main')

    <div class="restrito-auth curriculo">
        @include('frontend.restrito._title')

        {!! Form::open([
            'route'  => 'area-restrita.curriculo.post',
            'method' => 'post',
            'files'  => true
        ]) !!}

        <div class="restrito-curriculo-wrapper">
            @if($errors->any())
            <div class="flash flash-error">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif
            @if(session('success'))
            <div class="flash flash-success">{{ session('success') }}</div>
            @endif

            <div class="box">
                <div class="row">
                    <input type="text" name="nome" placeholder="nome completo" value="{{ old('nome') ?: $cadastro->nome }}" required>
                </div>
                <div class="row">
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') ?: $cadastro->email }}" required>
                </div>
                <div class="row">
                    <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') ?: $cadastro->telefone }}">
                </div>
                <div class="row">
                    <label class="file-input">
                        <input type="file" name="curriculo" required>
                        <span>
                            {{ t('contato.anexar-curriculo') }}
                        </span>
                    </label>
                </div>
                <label class="opt-in">
                    <input type="checkbox" name="opt-in" value="1" required>
                    {{ t('contato.curriculo-opt-in') }}
                </label>
            </div>

            <input type="submit" value="ENVIAR">
        </div>

        {!! Form::close() !!}
    </div>

@endsection
