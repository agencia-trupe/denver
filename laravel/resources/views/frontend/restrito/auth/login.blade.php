@extends('frontend.common.template')

@section('content')

    <div class="restrito restrito-auth restrito-login">
        <h2>
            <span>Olá!</span>
            Acesso Restrito
        </h2>

        <div class="box box-login">
            {!! Form::open(['route' => 'restrito.auth']) !!}
                @if(session('error'))
                <div class="flash flash-error">{{ session('error') }}</div>
                @endif
                @if(session('reset'))
                <div class="flash flash-success">{{ session('reset') }}</div>
                @endif
                @if(session('cadastro'))
                <div class="flash flash-success">{{ session('cadastro') }}</div>
                @endif

                <div class="row">
                    <input type="email" name="email" placeholder="login (e-mail)" value="{{ old('email') }}" required>
                </div>
                <div class="row">
                    <input type="password" name="password" placeholder="senha" required>
                </div>
                <div class="row">
                    <input type="submit" value="ENTRAR">
                </div>

                <a href="{{ route('restrito.reset') }}" class="btn-esqueci">
                    esqueci minha senha &raquo;
                </a>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
