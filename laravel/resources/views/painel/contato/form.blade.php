@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email_curriculos', 'E-mail para recebimento de currículos') !!}
    {!! Form::email('email_curriculos', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email_curriculos_restrito', 'E-mail para recebimento de currículos (Área Restrita)') !!}
    {!! Form::email('email_curriculos_restrito', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('telefone_duvidas_tecnicas', 'Telefone Dúvidas Técnicas') !!}
    {!! Form::text('telefone_duvidas_tecnicas', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone_vendas', 'Telefone Vendas') !!}
    {!! Form::text('telefone_vendas', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone_demais_regioes', 'Telefone Demais Regiões') !!}
    {!! Form::text('telefone_demais_regioes', null, ['class' => 'form-control']) !!}
</div>

<hr>



<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone_sao_paulo', 'São Paulo - Telefone') !!}
            {!! Form::text('telefone_sao_paulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('endereco_sao_paulo', 'São Paulo - Endereço') !!}
            {!! Form::textarea('endereco_sao_paulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('google_maps_sao_paulo', 'São Paulo - Código GoogleMaps') !!}
            {!! Form::text('google_maps_sao_paulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone_feira_de_santana', 'Feira de Santana - Telefone') !!}
            {!! Form::text('telefone_feira_de_santana', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('endereco_feira_de_santana', 'Feira de Santana - Endereço') !!}
            {!! Form::textarea('endereco_feira_de_santana', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('google_maps_feira_de_santana', 'Feira de Santana - Código GoogleMaps') !!}
            {!! Form::text('google_maps_feira_de_santana', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


<hr>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('youtube', 'YouTube') !!}
    {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'LinkedIn') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
