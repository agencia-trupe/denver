<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DownloadsRequest;
use App\Http\Controllers\Controller;

use App\Models\Download;
use App\Models\DownloadCategoria;

use App\Helpers\Tools;

class DownloadsController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = DownloadCategoria::ordenados()->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (DownloadCategoria::find($filtro)) {
            $registros = Download::where('downloads_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Download::leftJoin('downloads_categorias as cat', 'cat.id', '=', 'downloads_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('downloads.*')
                ->ordenados()->get();
        }

        return view('painel.downloads.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.downloads.create', compact('categorias'));
    }

    public function store(DownloadsRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Download::upload_capa();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'arquivos/downloads/');
            }

            Download::create($input);

            return redirect()->route('painel.downloads.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Download $registro)
    {
        $categorias = $this->categorias;

        return view('painel.downloads.edit', compact('registro', 'categorias'));
    }

    public function update(DownloadsRequest $request, Download $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Download::upload_capa();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'arquivos/downloads/');
            }

            $registro->update($input);

            return redirect()->route('painel.downloads.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Download $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.downloads.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
