<?php

    Route::get('lang/{lang}', function($lang) {
        if (in_array($lang, ['pt', 'en', 'es'])) {
            Session::put('locale', $lang);
        }
        return back();
    })->name('lang');
