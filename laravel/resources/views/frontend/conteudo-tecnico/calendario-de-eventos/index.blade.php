@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="calendario-de-eventos">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.calendario') }}
        </h1>

        <div class="container">
            <div class="eventos">
                <h3>{{ t('conteudo-tecnico.eventos') }}</h3>
                @foreach($eventos as $evento)
                <a href="{{ route('conteudo-tecnico.calendario-de-eventos.evento', $evento->id) }}">
                    <img src="{{ asset('assets/img/eventos/'.$evento->capa) }}" alt="">
                    <div>
                        <h4>{{ tobj($evento, 'data') }}</h4>
                        <p>{!! tobj($evento, 'titulo') !!}</p>
                    </div>
                </a>
                @endforeach
            </div>

            <div class="treinamentos">
                <h3>{{ t('conteudo-tecnico.treinamentos') }}</h3>
                @foreach($treinamentos as $treinamento)
                <a href="{{ route('conteudo-tecnico.calendario-de-eventos.treinamento', $treinamento->id) }}">
                    <div class="data">
                        <?php list($dia, $mes, $ano) = explode('/', $treinamento->data); ?>
                        <span>{{ $dia }}</span>
                        <div>
                            <span>
                                {{ Tools::mesAbreviado($mes) }}
                            </span>
                            <span>
                                {{ $ano }}
                            </span>
                        </div>
                    </div>
                    <p>{!! tobj($treinamento, 'titulo') !!}</p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
