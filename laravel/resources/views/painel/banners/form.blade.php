@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem_pt', 'Imagem PT') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_pt', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_en', 'Imagem EN') !!}
@if($submitText == 'Alterar' && $registro->imagem_en)
    <img src="{{ url('assets/img/banners/'.$registro->imagem_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_en', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_es', 'Imagem ES') !!}
@if($submitText == 'Alterar' && $registro->imagem_es)
    <img src="{{ url('assets/img/banners/'.$registro->imagem_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_es', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
