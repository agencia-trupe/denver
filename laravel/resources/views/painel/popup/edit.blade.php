@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Popup

            {!! Form::open([
                'route'  => ['painel.popup.destroy', $registro->id],
                'method' => 'delete',
                'class'  => 'pull-right'
            ]) !!}
                <button type="submit" class="btn btn-sm btn-danger">
                    <span class="glyphicon glyphicon-trash" style="margin-right:10px"></span>
                    Limpar Popup
                </button>
            {!! Form::close() !!}
        </h2>

    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.popup.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.popup.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
