@extends('frontend.restrito.template')

@section('main')

    <div class="tabelas">
        @include('frontend.restrito._title')

        <div class="tabelas-grid">
            @foreach($tabelas as $tabela)
            <a href="{{ route('area-restrita.tabelas', $tabela->id) }}">
                <span>{{ $tabela->data }}</span>
                {{ $tabela->titulo }}
            </a>
            @endforeach
        </div>
    </div>

@endsection
