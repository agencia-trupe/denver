@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Aplicação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.aplicacoes.store']) !!}

        @include('painel.produtos.aplicacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
