export default function() {
    const data = window.DENVER_POPUP;

    if (!data) return;

    if (data.type === 'image') {
        $.fancybox({
            href: data.media,
            maxWidth: 1200,
            maxHeight: 800,
            padding: 0,
        });
    } else if (['youtube', 'vimeo'].includes(data.type)) {
        $.fancybox({
            type: 'iframe',
            href: data.media,
            width: 800,
            height: 450,
            aspectRatio: true,
            padding: 0,
        });
    }
}
