<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoEmbalagem extends Model
{
    protected $table = 'produtos_embalagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function getLinkOnlineAttribute()
    {
        return 'https://munddi.com/Denver?e=1&p='.$this->sku;
    }

    public function getLinkLojaAttribute()
    {
        return route('contato.onde-encontrar');
    }
}
