<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RevendedorEstado extends Model
{
    protected $table = 'revendedores_estados';

    protected $guarded = ['id'];

    public function cidades()
    {
        return $this
            ->hasMany('App\Models\RevendedorCidade', 'estado_id')
            ->orderBy('nome', 'asc');
    }
}
