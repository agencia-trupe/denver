<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InteresseRecebido extends Model
{
    protected $table = 'interesses_recebidos';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function evento()
    {
        return $this->morphTo();
    }
}
