@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('aplicacoes', 'Aplicações') !!}
    {!! Form::select('aplicacoes[]', $aplicacoes, isset($registro) ? $registro->aplicacoes->pluck('id')->toArray() : null, ['class' => 'form-control multi-select', 'multiple' => true]) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('subtitulo_pt', 'Subtítulo PT') !!}
            {!! Form::text('subtitulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('descricao_pt', 'Descrição PT') !!}
            {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('subtitulo_en', 'Subtítulo EN') !!}
            {!! Form::text('subtitulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição EN') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título ES') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('subtitulo_es', 'Subtítulo ES') !!}
            {!! Form::text('subtitulo_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('descricao_es', 'Descrição ES') !!}
            {!! Form::textarea('descricao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('consumo_pt', 'Consumo PT') !!}
            {!! Form::textarea('consumo_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('embalagem_pt', 'Embalagem PT') !!}
            {!! Form::textarea('embalagem_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('usos_pt', 'Usos PT') !!}
            {!! Form::textarea('usos_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'usos']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('consumo_en', 'Consumo EN') !!}
            {!! Form::textarea('consumo_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('embalagem_en', 'Embalagem EN') !!}
            {!! Form::textarea('embalagem_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('usos_en', 'Usos EN') !!}
            {!! Form::textarea('usos_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'usos']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('consumo_es', 'Consumo ES') !!}
            {!! Form::textarea('consumo_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('embalagem_es', 'Embalagem ES') !!}
            {!! Form::textarea('embalagem_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('usos_es', 'Usos ES') !!}
            {!! Form::textarea('usos_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'usos']) !!}
        </div>
    </div>
</div>

<div class="well">
    <p style="font-weight:bold;margin-bottom:1.5em;padding-bottom:1em;border-bottom:1px solid #00000015">Calculadora de consumo</p>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('medida', 'Medida (ex: m²)') !!}
                {!! Form::text('medida', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('unidades_por_medida', 'Unidades do produto por medida (ex: 1.5, usar PONTO para decimais)') !!}
                {!! Form::text('unidades_por_medida', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
