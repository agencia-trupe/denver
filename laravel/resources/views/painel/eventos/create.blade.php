@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Eventos /</small> Adicionar Evento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.eventos.store', 'files' => true]) !!}

        @include('painel.eventos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
