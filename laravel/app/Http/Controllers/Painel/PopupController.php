<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PopupRequest;
use App\Http\Controllers\Controller;

use App\Models\Popup;

class PopupController extends Controller
{
    public function index()
    {
        $registro = Popup::first();

        return view('painel.popup.edit', compact('registro'));
    }

    public function update(PopupRequest $request, Popup $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Popup::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.popup.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function destroy(Popup $registro)
    {
        try {
            $registro->update([
                'imagem'            => '',
                'video'             => '',
                'video_tipo'        => '',
                'ativo'             => false,
                'data_de_expiracao' => '',
            ]);

            return redirect()->route('painel.popup.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
