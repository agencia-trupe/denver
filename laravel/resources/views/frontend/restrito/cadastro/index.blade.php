@extends('frontend.restrito.template')

@section('main')

    <div class="restrito-auth cadastro">
        @include('frontend.restrito._title')

        {!! Form::open(['route' => 'area-restrita.cadastro.post', 'method' => 'put']) !!}
            @if($errors->any())
            <div class="flash flash-error">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif
            @if(session('success'))
            <div class="flash flash-success">{{ session('success') }}</div>
            @endif

            <div class="restrito-register-wrapper">
                <div class="box">
                    <div class="row">
                        <input type="text" name="nome" placeholder="nome completo" value="{{ old('nome') ?: $cadastro->nome }}" required>
                    </div>
                    <div class="row">
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') ?: $cadastro->email }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') ?: $cadastro->telefone }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cpf" placeholder="CPF" value="{{ old('cpf') ?: $cadastro->cpf }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="empresa_nome_fantasia" placeholder="empresa - nome fantasia" value="{{ old('empresa_nome_fantasia') ?: $cadastro->empresa_nome_fantasia }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="empresa_razao_social" placeholder="empresa - razão social" value="{{ old('empresa_razao_social') ?: $cadastro->empresa_razao_social }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cnpj" placeholder="CNPJ" value="{{ old('cnpj') ?: $cadastro->cnpj }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cep" placeholder="CEP" value="{{ old('cep') ?: $cadastro->cep }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="endereco" placeholder="endereço" value="{{ old('endereco') ?: $cadastro->endereco }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="numero" placeholder="número" value="{{ old('numero') ?: $cadastro->numero }}" required>
                        <input type="text" name="complemento" placeholder="complemento" value="{{ old('complemento') ?: $cadastro->complemento }}">
                    </div>
                    <div class="row">
                        <input type="text" name="bairro" placeholder="bairro" value="{{ old('bairro') ?: $cadastro->bairro }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cidade" placeholder="cidade" value="{{ old('cidade') ?: $cadastro->cidade }}" required>
                        <input type="text" name="uf" placeholder="UF" value="{{ old('uf') ?: $cadastro->uf }}" maxlength="2" required>
                    </div>
                </div>

                <div class="box">
                    @foreach(explode(',', $cadastro->perfil) as $perfil)
                    <label class="checkbox checkbox-disabled">
                        <input type="checkbox" name="perfil[]" value="{{ $perfil }}" checked disabled>
                        <span></span>
                        {{ Tools::restritoPerfis()[$perfil] }}
                    </label>
                    @endforeach
                </div>

                <div class="box">
                    <div class="row">
                        <input type="password" name="password" placeholder="senha">
                    </div>
                    <div class="row">
                        <input type="password" name="password_confirmation" placeholder="confirmar senha">
                    </div>
                </div>

                <input type="submit" value="ATUALIZAR CADASTRO">
            </div>
        {!! Form::close() !!}
    </div>

@endsection
