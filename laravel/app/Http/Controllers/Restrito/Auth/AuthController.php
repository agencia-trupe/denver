<?php

namespace App\Http\Controllers\Restrito\Auth;

use Auth;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use App\Http\Requests\Restrito\CadastrosRequest;

use App\Models\Restrito\Cadastro;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/area-restrita';
    protected $guard      = 'restrito';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.restrito', ['except' => 'logout']);
    }

    public function showRegisterForm(Request $request)
    {
        if ($request->token !== env('TOKEN_CADASTRO_RESTRITO')) {
            abort('404');
        }

        return view('frontend.restrito.auth.register');
    }

    public function register(CadastrosRequest $request)
    {
        if ($request->token !== env('TOKEN_CADASTRO_RESTRITO')) {
            abort('404');
        }

        $input = $request->except(['password_confirmation', 'token']);

        $input['password'] = bcrypt($input['password']);
        $input['perfil'] = implode(',', $request->get('perfil'));

        Cadastro::create($input);

        return redirect()->route('restrito.login')
            ->with('cadastro', 'Cadastro efetuado com sucesso! Aguarde a aprovação dos nossos administradores.');
    }

    public function showLoginForm()
    {
        return view('frontend.restrito.auth.login');
    }

    protected function login(Request $request)
    {
        if (Auth::guard('restrito')->attempt([
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ], true)) {
            if (!Auth::guard('restrito')->user()->aprovado) {
                Auth::guard('restrito')->logout();

                return redirect()->route('restrito.login')
                    ->with('error', 'Seu cadastro está em processo de aprovação pelos administradores.');
            }

            return redirect()->intended('area-restrita');
        } else {
            return redirect()->route('restrito.login')
                ->withInput()->with('error', 'E-mail ou senha inválidos.');
        }
    }
}
