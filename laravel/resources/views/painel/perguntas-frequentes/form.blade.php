@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('perguntas_frequentes_categoria_id', 'Categoria') !!}
    {!! Form::select('perguntas_frequentes_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pergunta_pt', 'Pergunta PT') !!}
            {!! Form::text('pergunta_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('resposta_pt', 'Resposta PT') !!}
            {!! Form::textarea('resposta_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pergunta_en', 'Pergunta EN') !!}
            {!! Form::text('pergunta_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('resposta_en', 'Resposta EN') !!}
            {!! Form::textarea('resposta_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('pergunta_es', 'Pergunta ES') !!}
            {!! Form::text('pergunta_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('resposta_es', 'Resposta ES') !!}
            {!! Form::textarea('resposta_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.perguntas-frequentes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
