<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoObra extends Model
{
    protected $table = 'produtos_obras';

    protected $guarded = ['id'];

    public function obra()
    {
        return $this->belongsTo('App\Models\Obra', 'obra_id');
    }

    public function produto()
    {
        return $this->belongsTo('App\Models\Produto', 'produto_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
