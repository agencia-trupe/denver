<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsultorVirtualRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'tipo' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
