@include('painel.common.flash')

<div class="well">
    <strong>Perfil</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(Tools::restritoPerfis() as $perfil => $label)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="perfil[]" value="{{ $perfil }}" @if(isset($registro) && str_contains($registro->perfil, $perfil) || (count(old('perfil')) && in_array($perfil, old('perfil')))) checked @endif>
            <span style="font-weight:bold">{{ $label }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome completo') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cpf', 'CPF') !!}
    {!! Form::text('cpf', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa_nome_fantasia', 'Empresa - Nome Fantasia') !!}
    {!! Form::text('empresa_nome_fantasia', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa_razao_social', 'Empresa - Razão Social') !!}
    {!! Form::text('empresa_razao_social', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('cep', 'CEP') !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('numero', 'Número') !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('complemento', 'Complemento') !!}
    {!! Form::text('complemento', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('bairro', 'Bairro') !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade', 'Cidade') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('uf', 'UF') !!}
    {!! Form::text('uf', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('password', 'Senha') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('password_confirmation', 'Confirmação de Senha') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

@if(isset($registro) && !$registro->aprovado)
<hr>

<div class="form-group" style="font-size:0">
    <a href="{{ route('painel.restrito.cadastros.aprovar', $registro->id) }}" class="btn btn-block btn-success btn-lg">
        <span class="glyphicon glyphicon-ok" style="font-size:.8em;margin-right:10px"></span>
        Aprovar Cadastro
    </a>
</div>

<hr>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.restrito.cadastros.index') }}" class="btn btn-default btn-voltar">Voltar</a>
