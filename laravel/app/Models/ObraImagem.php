<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ObraImagem extends Model
{
    protected $table = 'obras_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/obras/imagens/thumbs/'
            ],
            [
                'width'   => 920,
                'height'  => null,
                'path'    => 'assets/img/obras/imagens/'
            ]
        ]);
    }
}
