<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PoliticaDePrivacidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $registro = PoliticaDePrivacidade::first();

        return view('painel.politica-de-privacidade.edit', compact('registro'));
    }

    public function update(PoliticaDePrivacidadeRequest $request, PoliticaDePrivacidade $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.politica-de-privacidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
