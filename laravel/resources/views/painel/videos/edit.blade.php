@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Vídeos /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.videos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
