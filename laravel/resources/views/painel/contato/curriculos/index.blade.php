@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Contato /</small>
            Currículos
            @if($registros->total())
            <small>({{ $registros->total() }} registro{{ $registros->total() != 1 ? 's' : '' }})</small>
            @endif
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data de Cadastro</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Currículo</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr>
                <td>{{ $registro->created_at->format('d/m/Y H:i') }}</td>
                <td>{{ $registro->nome }}</td>
                <td>
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $registro->email }}
                </td>
                <td>
                    {{ $registro->telefone ?: '-' }}
                </td>
                <td>
                    <a href="{{ route('curriculos.public', $registro->curriculo) }}" target="_blank" class="btn btn-sm btn-info">
                        <span class="glyphicon glyphicon-file" style="margin-right:10px"></span>
                        Currículo
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.contato.curriculos.destroy', $registro],
                        'method' => 'delete'
                    ]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $registros->links() }}
    @endif

@endsection
