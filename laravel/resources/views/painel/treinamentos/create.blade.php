@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Treinamentos /</small> Adicionar Treinamento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.treinamentos.store', 'files' => true]) !!}

        @include('painel.treinamentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
