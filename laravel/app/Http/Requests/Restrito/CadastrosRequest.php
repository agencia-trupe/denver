<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

use App\Helpers\Tools;

class CadastrosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'                  => 'required',
            'email'                 => 'required|email|unique:restrito_cadastros',
            'telefone'              => 'required',
            'cpf'                   => 'required',
            'empresa_nome_fantasia' => 'required',
            'empresa_razao_social'  => 'required',
            'cnpj'                  => 'required',
            'cep'                   => 'required',
            'endereco'              => 'required',
            'numero'                => 'required',
            'complemento'           => '',
            'bairro'                => 'required',
            'cidade'                => 'required',
            'uf'                    => 'required',
            'perfil'                => 'required|array|in:'.implode(',', array_keys(Tools::restritoPerfis())),
            'password'              => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            if ($this->route('restrito_cadastros')) {
                $id = $this->route('restrito_cadastros')->id;
            } else {
                $id = auth('restrito')->user()->id;
                $rules['perfil'] = '';
            }

            $rules['email'] = 'required|email|unique:restrito_cadastros,email,'.$id;
            $rules['password'] = 'confirmed|min:6';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'password' => 'senha',
        ];
    }
}
