<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosObrasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'obra_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'obra_id' => 'obra',
        ];
    }
}
