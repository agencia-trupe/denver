<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SolucoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Solucao;

class SolucoesController extends Controller
{
    public function index()
    {
        $registros = Solucao::ordenados()->get();

        return view('painel.solucoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.solucoes.create');
    }

    public function store(SolucoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Solucao::upload_imagem();

            Solucao::create($input);

            return redirect()->route('painel.solucoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Solucao $registro)
    {
        return view('painel.solucoes.edit', compact('registro'));
    }

    public function update(SolucoesRequest $request, Solucao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Solucao::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.solucoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Solucao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.solucoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
