@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="calendario-de-eventos">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.calendario') }}
        </h1>

        <div class="evento">
            <h3>{{ t('conteudo-tecnico.eventos') }}</h3>

            <div class="wrapper">
                <img src="{{ asset('assets/img/eventos/'.$evento->capa) }}" alt="">

                <div class="evento-content">
                    <h4>{{ tobj($evento, 'data') }}</h4>
                    <h5>{!! tobj($evento, 'titulo') !!}</h5>
                    <div class="descricao">{!! tobj($evento, 'descricao') !!}</div>

                    @if($evento->telefone || $evento->site)
                    <div class="contato-info">
                        <h4>{{ t('nav.contato') }}</h4>
                        @if($evento->telefone)
                        <p class="telefone">{{ $evento->telefone }}</p>
                        @endif
                        @if($evento->site)
                        <a href="{{ Tools::parseLink($evento->site) }}" class="site" target="_blank">
                            {{ $evento->site }}
                        </a>
                        @endif
                    </div>
                    @endif

                    @if($evento->exibir_formulario)
                    <form action="{{ route('conteudo-tecnico.calendario-de-eventos.evento.interesse', $evento->id) }}" id="form-interesse">
                        <h4>{{ t('conteudo-tecnico.interesse') }}</h4>
                        <div>
                            <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required>
                            <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
                            <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}">
                            <button type="submit">
                                {{ t('contato.enviar') }}
                                {!! file_get_contents(resource_path('assets/img/ico-carta-enviar.svg')) !!}
                            </button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
