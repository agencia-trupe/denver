@include('painel.common.flash')

<div class="well">
    <strong>Perfil</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(Tools::restritoPerfis() as $perfil => $label)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="perfil[]" value="{{ $perfil }}" @if(isset($registro) && str_contains($registro->perfil, $perfil) || (count(old('perfil')) && in_array($perfil, old('perfil')))) checked @endif>
            <span style="font-weight:bold">{{ $label }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('ativo', 0) !!}
            {!! Form::checkbox('ativo') !!}
            Ativo
        </label>
    </div>
</div>

<hr>

<div class="alert alert-info" style="padding:10px 15px;">
    <small>
        <span class="glyphicon glyphicon-info-sign" style="margin-right: 10px;"></span>
        Preencha apenas um dos campos abaixo.
    </small>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    @if($registro->imagem)
    <img src="{{ url('assets/img/comunicados/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <div style="margin-bottom:5px">
        <a href="{{ route('painel.restrito.arquivo', ['comunicados', $registro->arquivo]) }}" target="_blank">
            {{ $registro->arquivo }}
        </a>
    </div>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.restrito.comunicados.index') }}" class="btn btn-default btn-voltar">Voltar</a>
