<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObrasTable extends Migration
{
    public function up()
    {
        Schema::create('obras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('capa');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->boolean('destaque_home')->default(false);
            $table->timestamps();
        });

        Schema::create('obras_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('obra_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('obra_id')->references('id')->on('obras')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('obras_imagens');
        Schema::drop('obras');
    }
}
