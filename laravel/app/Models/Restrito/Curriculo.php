<?php

namespace App\Models\Restrito;

use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
    protected $table = 'restrito_curriculos';

    protected $guarded = ['id'];
}
