<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;
use Carbon\Carbon;

class Popup extends Model
{
    protected $table = 'popup';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/popup/'
        ]);
    }

    public function isActive()
    {
        return
            $this->ativo && (
                !$this->data_de_expiracao ||
                Carbon::now()->lt(Carbon::createFromFormat('d/m/Y', $this->data_de_expiracao)->startOfDay())
            );
    }

    public function getType()
    {
        if ($this->imagem) {
            return 'image';
        }

        return $this->video_tipo ?: 'none';
    }

    public function getMedia()
    {
        switch($this->getType()) {
            case 'image':
                return asset('assets/img/popup/'.$this->imagem);
            case 'youtube':
                return "https://youtube.com/embed/$this->video";
            case 'vimeo':
                return "https://player.vimeo.com/video/$this->video";
            default:
                return 'none';
        }
    }
}
