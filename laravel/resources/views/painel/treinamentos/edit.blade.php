@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Treinamentos /</small> Editar Treinamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.treinamentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.treinamentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
