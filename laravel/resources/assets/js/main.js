import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import SelectRoute from './SelectRoute';
import Popup from './Popup';
import Banners from './Banners';
import CustomSelect from './CustomSelect';
import Newsletter from './Newsletter';
import DetalhesTecnicos from './DetalhesTecnicos';
import Downloads from './Downloads';
import Calculadora from './Calculadora';
import Contato from './Contato';
import FileInput from './FileInput';
import VideoLightbox from './VideoLightbox';
import Embalagens from './Embalagens';
import Revendedores from './Revendedores';
import Busca from './Busca';
import ScrollToId from './ScrollToId';
import InputMasks from './InputMasks';
import CEP from './CEP';

AjaxSetup();
MobileToggle();
SelectRoute();
Popup();
Banners();
CustomSelect();
Newsletter();
DetalhesTecnicos();
Downloads();
Calculadora();
Contato();
FileInput();
VideoLightbox();
Embalagens();
Revendedores();
Busca();
ScrollToId();
InputMasks();
CEP();

$(document).ready(function() {
    // AVISO DE COOKIES
    $('.aviso-cookies').show();

    $('.aceitar-cookies').click(function() {
        var url = window.location.origin + '/aceite-de-cookies';

        $.ajax({
            type: 'POST',
            url: url,
            success: function(data, textStatus, jqXHR) {
                $('.aviso-cookies').hide();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
