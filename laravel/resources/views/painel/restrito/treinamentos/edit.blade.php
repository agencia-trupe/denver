@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Treinamentos /</small> Editar Treinamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.treinamentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.treinamentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
