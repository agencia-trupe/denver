export default function() {
    $('.handle-detalhes-tecnicos').fancybox({
        type: 'ajax',
        maxWidth: 840,
        maxHeight: '70%',
        padding: 0,
    });
}
