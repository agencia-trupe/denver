export default function() {
    $('.home .select-title').click(function customSelect() {
        const $select = $(this).parent();

        $select.find('ul').scrollTop(0);
        $select.toggleClass('active');
    });
}
