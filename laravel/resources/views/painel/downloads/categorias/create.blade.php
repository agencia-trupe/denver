@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Downloads /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.downloads.categorias.store']) !!}

        @include('painel.downloads.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
