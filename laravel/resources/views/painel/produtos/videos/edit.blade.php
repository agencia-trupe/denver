@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.videos.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.produtos.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
