<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

use App\Helpers\Tools;

class CampanhasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'perfil' => 'required|array|in:'.implode(',', array_keys(Tools::restritoPerfis())),
            'titulo' => 'required',
            'capa' => 'required|image',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
