@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('data_pt', 'Data PT') !!}
            {!! Form::text('data_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::textarea('titulo_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('data_en', 'Data EN') !!}
            {!! Form::text('data_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::textarea('titulo_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('data_es', 'Data ES') !!}
            {!! Form::text('data_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título ES') !!}
            {!! Form::textarea('titulo_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/eventos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('descricao_pt', 'Descrição PT') !!}
    {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_en', 'Descrição EN') !!}
    {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_es', 'Descrição ES') !!}
    {!! Form::textarea('descricao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone (opcional)') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('site', 'Site (opcional)') !!}
    {!! Form::text('site', null, ['class' => 'form-control']) !!}
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('exibir_formulario', 0) !!}
            {!! Form::checkbox('exibir_formulario') !!}
            Exibir formulário de interesse
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.eventos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
