@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Revendedores / Estados / {{ $estado->nome }} /</small>
            Adicionar Cidade
        </h2>
    </legend>

    {!! Form::open(['route' => ['painel.revendedores.estados.cidades.store', $estado->id]]) !!}

        @include('painel.revendedores.cidades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
