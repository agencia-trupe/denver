<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoRelacionado extends Model
{
    protected $table = 'produtos_relacionados';

    protected $guarded = ['id'];

    public function relacionado()
    {
        return $this->belongsTo('App\Models\Produto', 'relacionado_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
