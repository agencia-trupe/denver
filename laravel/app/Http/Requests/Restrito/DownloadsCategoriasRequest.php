<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

class DownloadsCategoriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required'
        ];
    }
}
