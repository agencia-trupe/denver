@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Downloads /</small> Editar Download</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.downloads.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.downloads.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
