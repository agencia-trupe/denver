export default function() {
    $('.banners').cycle({
        slides: '>.banner',
        pagerTemplate: '<a href="#">{{slideNum}}</a>',
    });
}
