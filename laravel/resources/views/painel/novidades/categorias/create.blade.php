@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Novidades /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.novidades.categorias.store']) !!}

        @include('painel.novidades.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
