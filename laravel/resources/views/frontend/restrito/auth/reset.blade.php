@extends('frontend.common.template')

@section('content')

    <div class="restrito restrito-auth restrito-login">
        <h2>
            <span>Olá!</span>
            Redefinição de senha
        </h2>

        <div class="box box-login">
            {!! Form::open(['route' => 'restrito.reset.post']) !!}
                @if($errors->any())
                <div class="flash">{!! $errors->first() !!}</div>
                @endif

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}" required>

                <div class="row">
                    <input type="password" name="password" placeholder="senha" required>
                </div>
                <div class="row">
                    <input type="password" name="password_confirmation" placeholder="confirmar senha" required>
                </div>
                <div class="row">
                    <input type="submit" value="REDEFINIR SENHA">
                </div>
            {!! Form::close() !!}
        </div>

@endsection
