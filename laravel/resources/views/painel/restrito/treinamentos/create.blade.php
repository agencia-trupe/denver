@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Treinamentos /</small> Adicionar Treinamento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.treinamentos.store', 'files' => true]) !!}

        @include('painel.restrito.treinamentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
