@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="cover"></div>

        <div class="center">
            <h1 class="main-title">{{ t('contato.fale-conosco') }}</h1>

            <h2>
                <span>{{ t('contato.duvidas-tecnicas') }}</span>
                {{ $contato->telefone_duvidas_tecnicas }}
            </h2>
            <h2>
                <span>{{ t('contato.vendas') }}</span>
                {{ $contato->telefone_vendas }}
            </h2>
            <h2>
                <span>{{ t('contato.lojas-revendedores') }}</span>
                <a href="{{ route('contato.onde-encontrar') }}">
                    {{ t('contato.onde-encontrar') }} &raquo;
                </a>
            </h2>

            <div class="form-enderecos-wrapper">
                <form action="{{ route('contato.post') }}" method="POST" id="form-contato">
                    <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required>
                    <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
                    <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}">
                    <textarea name="mensagem" placeholder="{{ t('contato.mensagem') }}" required></textarea>
                    <button type="submit">
                        {{ t('contato.enviar') }}
                        {!! file_get_contents(resource_path('assets/img/ico-carta-enviar.svg')) !!}
                    </button>
                </form>

                <div class="enderecos">
                    @foreach([
                        'sao_paulo' => 'São Paulo',
                        'feira_de_santana' => 'Feira de Santana',
                    ] as $slug => $title)
                    <div class="endereco">
                        <div>
                            <h3>{{ $title }}</h3>
                            <p>
                                {!! $contato->{'endereco_'.$slug} !!}
                                <br>
                                {{ ucfirst(t('contato.telefone')) }}:
                                {{ $contato->{'telefone_'.$slug} }}
                            </p>
                        </div>
                        <div>{!! $contato->{'google_maps_'.$slug} !!}</div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="trabalhe-conosco">
                <form action="{{ route('contato.post-curriculo') }}" method="POST" id="form-curriculo">
                    <p>{{ t('contato.trabalhe-conosco') }}</p>

                    <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required>
                    <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
                    <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}">
                    <div>
                        <label class="file-input">
                            <input type="file" name="curriculo" required>
                            <span>
                                {{ t('contato.anexar-curriculo') }}
                            </span>
                        </label>
                        <button type="submit">{{ t('contato.enviar') }}</button>
                    </div>
                    <label class="opt-in">
                        <input type="checkbox" name="opt-in" value="1" required>
                        <div>
                            {!! t('contato.curriculo-opt-in') !!}
                        </div>
                    </label>
                </form>

                <img src="{{ asset('assets/img/layout/img-trabalheconosco.jpg') }}" alt="">
            </div>

            <div class="representante">
                <p>
                    {{ t('contato.representante') }}
                    <span>{{ t('contato.cadastre') }}</span>
                </p>

                <form action="{{ route('contato.post-representante') }}" method="POST" id="form-representante">
                    <div>
                        <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required>
                        <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
                        <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}">
                        <input type="text" name="uf" placeholder="{{ t('contato.uf') }}" maxlength="2">
                        <input type="text" name="cidade" placeholder="{{ t('contato.cidade') }}">
                        <label class="file-input">
                            <input type="file" name="curriculo" required>
                            <span>
                                {{ t('contato.anexar-curriculo') }}
                            </span>
                        </label>
                        <button type="submit">
                            {!! file_get_contents(resource_path('assets/img/ico-carta-enviar.svg')) !!}
                        </button>
                    </div>
                    <label class="opt-in">
                        <input type="checkbox" name="opt-in" value="1" required>
                        <div>
                            {!! t('contato.curriculo-opt-in') !!}
                        </div>
                    </label>
                </form>
            </div>
        </div>
    </div>

@endsection
