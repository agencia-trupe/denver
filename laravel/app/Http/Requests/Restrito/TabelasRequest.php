<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

use App\Helpers\Tools;

class TabelasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'perfil' => 'required|array|in:'.implode(',', array_keys(Tools::restritoPerfis())),
            'data' => 'required',
            'titulo' => 'required',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
