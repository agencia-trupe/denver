<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'aplicacoes' => 'required|array',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => '',
            'subtitulo_es' => '',
            'descricao_pt' => 'required',
            'descricao_en' => '',
            'descricao_es' => '',
            'imagem' => 'required|image',
            'consumo_pt' => 'required',
            'consumo_en' => '',
            'consumo_es' => '',
            'embalagem_pt' => 'required',
            'embalagem_en' => '',
            'embalagem_es' => '',
            'usos_pt' => 'required',
            'usos_en' => '',
            'usos_es' => '',
            'medida' => 'required',
            'unidades_por_medida' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
