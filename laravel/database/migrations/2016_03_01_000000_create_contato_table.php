<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('telefone_duvidas_tecnicas');
            $table->string('telefone_vendas');
            $table->string('telefone_demais_regioes');
            $table->text('telefone_sao_paulo');
            $table->text('endereco_sao_paulo');
            $table->text('google_maps_sao_paulo');
            $table->text('telefone_feira_de_santana');
            $table->text('endereco_feira_de_santana');
            $table->text('google_maps_feira_de_santana');
            $table->string('facebook');
            $table->string('instagram');
            $table->string('youtube');
            $table->string('linkedin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
