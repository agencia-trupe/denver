<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

use App\Helpers\Tools;

class ComunicadosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'perfil' => 'required|array|in:'.implode(',', array_keys(Tools::restritoPerfis())),
            'data' => 'required',
            'titulo' => 'required',
            'imagem' => 'image',
            'arquivo' => 'required_without:imagem'
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
