@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Downloads /</small> Adicionar Download</h2>
    </legend>

    {!! Form::open(['route' => 'painel.downloads.store', 'files' => true]) !!}

        @include('painel.downloads.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
