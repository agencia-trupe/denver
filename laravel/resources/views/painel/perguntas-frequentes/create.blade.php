@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Perguntas Frequentes /</small> Adicionar Pergunta Frequente</h2>
    </legend>

    {!! Form::open(['route' => 'painel.perguntas-frequentes.store', 'files' => true]) !!}

        @include('painel.perguntas-frequentes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
