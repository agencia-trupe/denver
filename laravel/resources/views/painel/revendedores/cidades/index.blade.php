@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.revendedores.estados') }}" title="Voltar para Estados" class="btn btn-sm btn-default">
        &larr; Voltar para Estados
    </a>

    <legend>
        <h2>
            <small>Revendedores / Estados /</small>
            {{ $estado->nome }}
            <a href="{{ route('painel.revendedores.estados.cidades.create', $estado->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Cidade</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <div class="alert alert-danger">
        A exclusão de cidades afetará possíveis representantes que estejam associados a mesma.
    </div>

    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Cidade</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.revendedores.estados.cidades.destroy', $estado->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.revendedores.estados.cidades.edit', [$estado->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
