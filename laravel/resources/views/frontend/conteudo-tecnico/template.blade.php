@extends('frontend.common.template')

@section('content')

    <div class="conteudo-tecnico">
        <nav @if(Tools::routeIs('conteudo-tecnico.consultor-virtual') && isset($consultoria)) class="mobile-after" @endif>
            <a href="{{ route('conteudo-tecnico.novidades') }}" @if(Tools::routeIs('conteudo-tecnico.novidades*')) class="active" @endif>
                {!! file_get_contents(resource_path('assets/img/ico-aprenda-novidades.svg')) !!}
                <span>{{ t('nav.novidades') }}</span>
            </a>
            <a href="{{ route('conteudo-tecnico.calendario-de-eventos') }}" @if(Tools::routeIs('conteudo-tecnico.calendario-de-eventos*')) class="active" @endif>
                {!! file_get_contents(resource_path('assets/img/ico-aprenda-calendario.svg')) !!}
                <span>{{ t('nav.calendario') }}</span>
            </a>
            <a href="{{ route('conteudo-tecnico.videos') }}" @if(Tools::routeIs('conteudo-tecnico.videos*')) class="active" @endif>
                {!! file_get_contents(resource_path('assets/img/ico-aprenda-videos.svg')) !!}
                <span>{{ t('nav.videos') }}</span>
            </a>
            <a href="{{ route('conteudo-tecnico.downloads') }}" @if(Tools::routeIs('conteudo-tecnico.downloads*')) class="active" @endif>
                {!! file_get_contents(resource_path('assets/img/ico-aprenda-downloads.svg')) !!}
                <span>{{ t('nav.downloads') }}</span>
            </a>
            <a href="{{ route('conteudo-tecnico.perguntas-frequentes') }}" @if(Tools::routeIs('conteudo-tecnico.perguntas-frequentes*')) class="active" @endif>
                {!! file_get_contents(resource_path('assets/img/ico-aprenda-faq.svg')) !!}
                <span>{{ t('nav.faq') }}</span>
            </a>
            <a href="{{ route('conteudo-tecnico.consultor-virtual') }}" @if(Tools::routeIs('conteudo-tecnico.consultor-virtual*')) class="active" @endif>
                {!! file_get_contents(resource_path('assets/img/ico-aprenda-consultor.svg')) !!}
                <span>{{ t('nav.consultor') }}</span>
            </a>
        </nav>

        <main>
            @yield('main')
        </main>
    </div>

@endsection
