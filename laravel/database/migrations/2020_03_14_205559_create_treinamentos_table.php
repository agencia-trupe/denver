<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTreinamentosTable extends Migration
{
    public function up()
    {
        Schema::create('treinamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->text('descricao_pt');
            $table->text('descricao_en');
            $table->text('descricao_es');
            $table->string('telefone');
            $table->string('site');
            $table->boolean('exibir_formulario')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('treinamentos');
    }
}
