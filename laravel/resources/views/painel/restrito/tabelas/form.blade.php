@include('painel.common.flash')

<div class="well">
    <strong>Perfil</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(Tools::restritoPerfis() as $perfil => $label)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="perfil[]" value="{{ $perfil }}" @if(isset($registro) && str_contains($registro->perfil, $perfil) || (count(old('perfil')) && in_array($perfil, old('perfil')))) checked @endif>
            <span style="font-weight:bold">{{ $label }}</span>
        </label>
    </div>
    @endforeach
</div>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <div style="margin-bottom:5px">
        <a href="{{ route('painel.restrito.arquivo', ['tabelas', $registro->arquivo]) }}" target="_blank">
            {{ $registro->arquivo }}
        </a>
    </div>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.restrito.tabelas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
