@extends('frontend.restrito.template')

@section('main')

    <div class="campanhas">
        @include('frontend.restrito._title')

        <div class="campanhas-grid">
            @foreach($campanhas as $campanha)
                <a href="{{ route('area-restrita.campanhas', $campanha->id) }}">
                    <img src="{{ asset('assets/img/campanhas/'.$campanha->capa) }}" alt="">
                    <span>{{ $campanha->titulo }}</span>
                </a>
            @endforeach
        </div>
    </div>

@endsection
