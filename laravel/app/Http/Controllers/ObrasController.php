<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Obra;

class ObrasController extends Controller
{
    public function index(Obra $obra)
    {
        if ($obra->exists) {
            $obras = Obra::ordenados()
                ->where('id', '!=', $obra->id)->limit(8)->get();

            return view('frontend.obras.show', compact('obra', 'obras'));
        }

        return view('frontend.obras.index', [
            'obras' => Obra::ordenados()->get()
        ]);
    }
}
