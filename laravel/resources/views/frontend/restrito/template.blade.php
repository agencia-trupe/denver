@extends('frontend.common.template')

@section('content')

    <div class="restrito restrito-template">
        <nav>
            <a href="{{ route('area-restrita.comunicados') }}" @if(Tools::routeIs('area-restrita.comunicados*')) class="active" @endif>
                Comunicados
            </a>
            <a href="{{ route('area-restrita.campanhas') }}" @if(Tools::routeIs('area-restrita.campanhas*')) class="active" @endif>
                Campanhas
            </a>
            <a href="{{ route('area-restrita.downloads') }}" @if(Tools::routeIs('area-restrita.downloads*')) class="active" @endif>
                Downloads
            </a>
            <a href="{{ route('area-restrita.videos') }}" @if(Tools::routeIs('area-restrita.videos*')) class="active" @endif>
                Vídeos
            </a>
            <a href="{{ route('area-restrita.treinamentos') }}" @if(Tools::routeIs('area-restrita.treinamentos*')) class="active" @endif>
                Treinamentos
            </a>
            <a href="{{ route('area-restrita.tabelas') }}" @if(Tools::routeIs('area-restrita.tabelas*')) class="active" @endif>
                Tabelas de preço
            </a>
            <a href="{{ route('area-restrita.cadastro') }}" @if(Tools::routeIs('area-restrita.cadastro')) class="active" @endif>
                Meu cadastro
            </a>
            <a href="{{ route('area-restrita.curriculo') }}" @if(Tools::routeIs('area-restrita.curriculo')) class="active" @endif>
                Envie seu currículo
            </a>
        </nav>

        <main>
            @yield('main')
        </main>
    </div>

@endsection
