@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.consultor-virtual.index') }}" title="Voltar para Consultor Virtual" class="btn btn-sm btn-default">
        &larr; Voltar para Consultor Virtual    </a>

    <legend>
        <h2>
            <small>Conteúdo Técnico / Consultor Virtual / {{ $consultoria->titulo_pt }} /</small>
            Indicações
            <a href="{{ route('painel.consultor-virtual.indicacoes.create', $consultoria->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Indicação</a>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="consultor_virtual_indicacoes">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título PT</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->titulo_pt }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.consultor-virtual.indicacoes.destroy', $consultoria->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.consultor-virtual.indicacoes.edit', [$consultoria->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
