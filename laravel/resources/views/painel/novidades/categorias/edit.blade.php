@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Novidades /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.novidades.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.novidades.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
