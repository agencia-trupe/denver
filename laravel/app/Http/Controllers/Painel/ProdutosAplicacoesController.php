<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosAplicacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoAplicacao;

class ProdutosAplicacoesController extends Controller
{
    public function index()
    {
        $aplicacoes = ProdutoAplicacao::orderBy('titulo_pt', 'ASC')->get();

        return view('painel.produtos.aplicacoes.index', compact('aplicacoes'));
    }

    public function create()
    {
        return view('painel.produtos.aplicacoes.create');
    }

    public function store(ProdutosAplicacoesRequest $request)
    {
        try {

            $input = $request->all();

            ProdutoAplicacao::create($input);
            return redirect()->route('painel.produtos.aplicacoes.index')->with('success', 'Aplicação adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar aplicação: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoAplicacao $aplicacao)
    {
        return view('painel.produtos.aplicacoes.edit', compact('aplicacao'));
    }

    public function update(ProdutosAplicacoesRequest $request, ProdutoAplicacao $aplicacao)
    {
        try {

            $input = $request->all();

            $aplicacao->update($input);
            return redirect()->route('painel.produtos.aplicacoes.index')->with('success', 'Aplicação alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar aplicação: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoAplicacao $aplicacao)
    {
        try {

            $aplicacao->delete();
            return redirect()->route('painel.produtos.aplicacoes.index')->with('success', 'Aplicação excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir aplicação: '.$e->getMessage()]);

        }
    }
}
