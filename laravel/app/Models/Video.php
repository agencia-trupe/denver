<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Video extends Model
{
    protected $table = 'videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'titulo_'.$locale
        ];

        return $query->where(function($q) use ($fields, $value) {
            foreach($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('titulo_'.$locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = $this->video_url;
        $obj->label = tobj($this, 'titulo');

        return $obj;
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 570,
            'height' => 320,
            'path'   => 'assets/img/videos/'
        ]);
    }

    public function getVideoUrlAttribute()
    {
        switch($this->video_tipo) {
            case 'youtube':
                return "https://youtube.com/embed/$this->video";
            case 'vimeo':
                return "https://player.vimeo.com/video/$this->video";
            default:
                return null;
        }
    }
}
