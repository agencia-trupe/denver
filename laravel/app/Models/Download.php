<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Helpers\CropImage;

class Download extends Model
{
    use SoftDeletes;

    protected $table = 'downloads';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('downloads_categoria_id', $categoria_id);
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'titulo_'.$locale
        ];

        return $query->where(function($q) use ($fields, $value) {
            foreach($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('titulo_'.$locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = asset('arquivos/downloads/'.$this->arquivo);
        $obj->label = tobj($this, 'titulo');

        return $obj;
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\DownloadCategoria', 'downloads_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'       => 340,
            'height'      => 360,
            'transparent' => true,
            'path'        => 'assets/img/downloads/'
        ]);
    }
}
