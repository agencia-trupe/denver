<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosVideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa' => 'required|image',
            'video' => 'required',
            'video_tipo' => 'required|in:youtube,vimeo',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
