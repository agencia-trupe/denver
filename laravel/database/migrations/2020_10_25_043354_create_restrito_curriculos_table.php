<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestritoCurriculosTable extends Migration
{
    public function up()
    {
        Schema::create('restrito_curriculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('curriculo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('restrito_curriculos');
    }
}
