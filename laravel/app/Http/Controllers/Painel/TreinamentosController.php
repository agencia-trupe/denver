<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TreinamentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Treinamento;

class TreinamentosController extends Controller
{
    public function index()
    {
        $registros = Treinamento::ordenados()->get();

        return view('painel.treinamentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.treinamentos.create');
    }

    public function store(TreinamentosRequest $request)
    {
        try {

            $input = $request->all();

            Treinamento::create($input);

            return redirect()->route('painel.treinamentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Treinamento $registro)
    {
        return view('painel.treinamentos.edit', compact('registro'));
    }

    public function update(TreinamentosRequest $request, Treinamento $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.treinamentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Treinamento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.treinamentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
