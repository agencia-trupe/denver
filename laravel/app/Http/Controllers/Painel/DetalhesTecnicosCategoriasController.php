<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DetalhesTecnicosCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\DetalheTecnicoCategoria;

class DetalhesTecnicosCategoriasController extends Controller
{
    public function index()
    {
        $categorias = DetalheTecnicoCategoria::ordenados()->get();

        return view('painel.detalhes-tecnicos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.detalhes-tecnicos.categorias.create');
    }

    public function store(DetalhesTecnicosCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = DetalheTecnicoCategoria::upload_capa();

            DetalheTecnicoCategoria::create($input);
            return redirect()->route('painel.detalhes-tecnicos.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(DetalheTecnicoCategoria $categoria)
    {
        return view('painel.detalhes-tecnicos.categorias.edit', compact('categoria'));
    }

    public function update(DetalhesTecnicosCategoriasRequest $request, DetalheTecnicoCategoria $categoria)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = DetalheTecnicoCategoria::upload_capa();

            $categoria->update($input);
            return redirect()->route('painel.detalhes-tecnicos.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(DetalheTecnicoCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.detalhes-tecnicos.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
