export default function RevendedoresCidades() {
    const $selectCidade = $('#revendedores-cidade');
    const $selectEstado = $('#revendedores-estado');
    const $listCidades = $('#revendedores-cidades-list');

    const cidadesArr = [];

    $selectEstado.change(function changeEstado() {
        const estado = $(this).val();

        if (!estado) {
            return $selectCidade
                .attr('disabled', true)
                .find('option:not(:first-child)')
                .remove();
        }

        $selectCidade.attr('disabled', true);

        return $.post($(this).data('route'), { estado })
            .done(cidades => {
                $selectCidade
                    .find('option:not(:first-child)')
                    .remove()
                    .append.apply(
                        $selectCidade,
                        cidades.map(({ nome, id }) =>
                            $(`<option value="${id}">${nome}</option>`)
                        )
                    );
            })
            .fail(() => alert('Ocorreu um erro.'))
            .always(() => $selectCidade.attr('disabled', false));
    });

    function renderCidades() {
        $listCidades.html('').append(
            ...cidadesArr
                .sort((a, b) => {
                    if (a.estado === b.estado) {
                        return a.nome > b.nome ? 1 : -1;
                    }

                    return a.estado > b.estado ? 1 : -1;
                })
                .map(
                    ({ cidade, uf, nome }) => `
                    <li class="list-group-item">
                        <input type="hidden" name="cidades[]" value="${cidade}">
                        <span>${uf} - ${nome}</span>
                        <a href="#" class="btn btn-sm btn-danger btn-remove-cidade" data-cidade-id="${cidade}">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </li>
                `
                )
        );
    }

    $('.btn-add-cidade').click(function add(event) {
        event.preventDefault();

        const estado = $selectEstado.val();
        const cidade = $selectCidade.val();

        if (estado && cidade) {
            const exists = cidadesArr.find(c => c.cidade === Number(cidade));

            if (!exists) {
                cidadesArr.push({
                    cidade: Number(cidade),
                    estado: Number(estado),
                    uf: $selectEstado.find('option:selected').data('uf'),
                    nome: $selectCidade.find('option:selected').text(),
                });

                renderCidades();
            }
        }
    });

    $('body').on('click', '.btn-remove-cidade', function remove(event) {
        event.preventDefault();

        const $this = $(this);
        const cidadeId = $this.data('cidade-id');

        const indexToRemove = cidadesArr.findIndex(
            cidade => cidade.cidade === cidadeId
        );

        cidadesArr.splice(indexToRemove, 1);
        $this.parent().remove();
    });

    if (window.DENVER_REVENDEDORES_CIDADES) {
        cidadesArr.push(...window.DENVER_REVENDEDORES_CIDADES);
        renderCidades();
    }
}
