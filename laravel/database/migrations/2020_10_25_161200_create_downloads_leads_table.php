<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadsLeadsTable extends Migration
{
    public function up()
    {
        Schema::create('downloads_leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('profissao');
            $table->string('email');
            $table->string('telefone');
            $table->string('segmento');
            $table->integer('download_id')->unsigned()->nullable();
            $table->foreign('download_id')->references('id')->on('downloads')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('downloads_leads');
    }
}
