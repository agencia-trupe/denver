<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ObrasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'capa' => 'required|image',
            'descricao_pt' => 'required',
            'descricao_en' => '',
            'descricao_es' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
