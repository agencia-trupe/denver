<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosRelacionadosRequest;

use App\Http\Controllers\Controller;

use App\Models\ProdutoRelacionado;
use App\Models\Produto;

class ProdutosRelacionadosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->relacionados;

        return view('painel.produtos.relacionados.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        $produtos = Produto::orderBy('titulo_pt', 'ASC')->lists('titulo_pt', 'id');

        return view('painel.produtos.relacionados.create', compact('produto', 'produtos'));
    }

    public function store(ProdutosRelacionadosRequest $request, Produto $produto)
    {
        try {

            if ($produto->id === (int) $request->produto_id) {
                throw new \Exception('Não é possível adicionar o próprio produto como relacionado.');
            }
            if ($produto->relacionados->where('relacionado_id', (int)$request->produto_id)->count()) {
                throw new \Exception('O relacionamento selecionado já existe.');
            }

            $produto->relacionados()->create(['relacionado_id' => $request->produto_id]);

            return redirect()->route('painel.produtos.relacionados.index', $produto)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, ProdutoRelacionado $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.relacionados.index', $produto)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
