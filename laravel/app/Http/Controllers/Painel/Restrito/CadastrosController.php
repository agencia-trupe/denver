<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests\Restrito\CadastrosRequest;
use App\Http\Controllers\Controller;

use App\Models\Restrito\Cadastro;

class CadastrosController extends Controller
{
    public function index()
    {
        $filtro = request('filtro');

        if ($filtro == 'aprovados') {
            $registros = Cadastro::where('aprovado', 1)->orderBy('id', 'desc')->get();
        } else {
            $registros = Cadastro::where('aprovado', 0)->orderBy('id', 'desc')->get();
        }

        $countAprovacao = Cadastro::where('aprovado', 0)->count();

        return view('painel.restrito.cadastros.index', compact('registros', 'countAprovacao'));
    }

    public function create()
    {
        return view('painel.restrito.cadastros.create');
    }

    public function store(CadastrosRequest $request)
    {
        try {

            $input = $request->except('password_confirmation');

            $input['perfil'] = implode(',', $request->get('perfil'));
            $input['password'] = bcrypt($input['password']);
            $input['aprovado'] = 1;

            Cadastro::create($input);

            return redirect()->route('painel.restrito.cadastros.index', ['filtro' => 'aprovados'])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cadastro $registro)
    {
        return view('painel.restrito.cadastros.edit', compact('registro'));
    }

    public function update(CadastrosRequest $request, Cadastro $registro)
    {
        try {

            $input = $request->except('password_confirmation');

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['password']) && $input['password'] != '') {
                $input['password'] = bcrypt($input['password']);
            } else {
                unset($input['password']);
            }

            $registro->update($input);

            $filtro = $registro->aprovado ? 'aprovados' : null;

            return redirect()->route('painel.restrito.cadastros.index', ['filtro' => $filtro])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cadastro $registro)
    {
        try {

            $registro->delete();

            return back()->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function aprovar(Cadastro $cadastro)
    {
        $cadastro->update(['aprovado' => true]);

        \Mail::send('emails.restrito.aprovado', $cadastro->toArray(), function ($message) use ($cadastro) {
            $message->to($cadastro->email, $cadastro->nome)
                    ->subject('Denver · Cadastro Aprovado');
        });

        return redirect()->route('painel.restrito.cadastros.index', ['filtro' => 'aprovados'])->with('success', 'Cadastro aprovado com sucesso.');
    }
}
