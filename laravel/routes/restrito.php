<?php

    Route::group([
        'prefix' => 'area-restrita',
        'namespace' => 'Restrito\Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')
            ->name('restrito.login');
        Route::post('login', 'AuthController@login')
            ->name('restrito.auth');
        Route::get('cadastro', 'AuthController@showRegisterForm')
            ->name('restrito.register');
        Route::post('cadastro', 'AuthController@register')
            ->name('restrito.register.post');
        Route::get('logout', 'AuthController@logout')
            ->name('restrito.logout');
        Route::get('esqueci-minha-senha', 'PasswordController@forgotPassword')
            ->name('restrito.reset');
        Route::post('redefinicao-de-senha', 'PasswordController@sendResetLinkEmail')
            ->name('restrito.reset.email');
        Route::get('redefinicao-de-senha/{token}', 'PasswordController@showResetForm')
            ->name('restrito.reset.token');
        Route::post('redefinir-senha', 'PasswordController@reset')
            ->name('restrito.reset.post');
    });

    Route::group([
        'prefix' => 'area-restrita',
        'namespace' => 'Restrito',
        'middleware' => ['auth.restrito']
    ], function() {
        Route::get('/', function() {
            return redirect()->route('area-restrita.comunicados');
        })->name('area-restrita');

        Route::get('comunicados/{id?}', 'ComunicadosController@index')
            ->name('area-restrita.comunicados');
        Route::get('campanhas/{id?}', 'CampanhasController@index')
            ->name('area-restrita.campanhas');
        Route::get('downloads/{download_restrito_categoria_slug?}', 'DownloadsController@index')
            ->name('area-restrita.downloads');
        Route::get('downloads/{id}/download', 'DownloadsController@download')
            ->name('area-restrita.downloads.download');
        Route::get('videos/{id?}', 'VideosController@index')
            ->name('area-restrita.videos');
        Route::get('videos/{id}/download', 'VideosController@download')
            ->name('area-restrita.videos.download');
        Route::get('treinamentos/{id?}', 'TreinamentosController@index')
            ->name('area-restrita.treinamentos');
        Route::get('treinamentos/{id}/download', 'TreinamentosController@download')
            ->name('area-restrita.treinamentos.download');
        Route::get('tabelas-de-preco/{id?}', 'TabelasController@index')
            ->name('area-restrita.tabelas');
        Route::get('meu-cadastro', 'CadastroController@index')
            ->name('area-restrita.cadastro');
        Route::put('meu-cadastro', 'CadastroController@post')
            ->name('area-restrita.cadastro.post');
        Route::get('curriculo', 'CurriculoController@index')
            ->name('area-restrita.curriculo');
        Route::post('curriculo', 'CurriculoController@post')
            ->name('area-restrita.curriculo.post');
    });
