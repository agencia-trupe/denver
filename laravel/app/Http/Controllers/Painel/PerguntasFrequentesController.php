<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerguntasFrequentesRequest;
use App\Http\Controllers\Controller;

use App\Models\PerguntaFrequente;
use App\Models\PerguntaFrequenteCategoria;

class PerguntasFrequentesController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = PerguntaFrequenteCategoria::ordenados()->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (PerguntaFrequenteCategoria::find($filtro)) {
            $registros = PerguntaFrequente::where('perguntas_frequentes_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = PerguntaFrequente::leftJoin('perguntas_frequentes_categorias as cat', 'cat.id', '=', 'perguntas_frequentes_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('perguntas_frequentes.*')
                ->ordenados()->get();
        }

        return view('painel.perguntas-frequentes.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.perguntas-frequentes.create', compact('categorias'));
    }

    public function store(PerguntasFrequentesRequest $request)
    {
        try {

            $input = $request->all();

            PerguntaFrequente::create($input);

            return redirect()->route('painel.perguntas-frequentes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(PerguntaFrequente $registro)
    {
        $categorias = $this->categorias;

        return view('painel.perguntas-frequentes.edit', compact('registro', 'categorias'));
    }

    public function update(PerguntasFrequentesRequest $request, PerguntaFrequente $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.perguntas-frequentes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(PerguntaFrequente $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.perguntas-frequentes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
