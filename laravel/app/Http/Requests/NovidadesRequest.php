<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NovidadesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'novidades_categoria_id' => 'required',
            'data' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'capa' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => '',
            'texto_es' => '',
            'publicado' => '',
            'destaque_home' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
