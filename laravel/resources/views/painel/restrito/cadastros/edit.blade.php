@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Cadastros /</small> Editar Cadastro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.cadastros.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.cadastros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
