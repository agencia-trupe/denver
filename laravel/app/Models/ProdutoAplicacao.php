<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class ProdutoAplicacao extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos_aplicacoes';

    protected $guarded = ['id'];

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function produtos()
    {
        return $this
            ->belongsToMany('App\Models\Produto', 'produto_aplicacao', 'produto_aplicacao_id', 'produto_id')
            ->ordenados();
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('titulo_'.app()->getLocale());
    }
}
