@extends('frontend.common.template')

@section('content')

    <div class="detalhes-tecnicos">
        <div class="center">
            <h1 class="main-title">{{ t('nav.detalhes-tecnicos') }}</h1>

            <div class="grid">
                @foreach($categorias as $categoria)
                <a href="{{ route('detalhes-tecnicos', $categoria->slug) }}" class="handle-detalhes-tecnicos">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/detalhes-tecnicos/'.$categoria->capa) }}" alt="">
                        <div class="overlay">
                            <div class="pdf"></div>
                        </div>
                    </div>
                    <span>{{ tobj($categoria, 'titulo') }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
