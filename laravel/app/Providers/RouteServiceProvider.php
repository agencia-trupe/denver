<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('restrito_comunicados', 'App\Models\Restrito\Comunicado');
        $router->model('restrito_campanhas', 'App\Models\Restrito\Campanha');
        $router->model('restrito_downloads', 'App\Models\Restrito\Download');
        $router->model('restrito_categorias_downloads', 'App\Models\Restrito\DownloadCategoria');
        $router->model('restrito_videos', 'App\Models\Restrito\Video');
        $router->model('restrito_treinamentos', 'App\Models\Restrito\Treinamento');
        $router->model('restrito_tabelas', 'App\Models\Restrito\Tabela');
        $router->model('restrito_curriculos', 'App\Models\Restrito\Curriculo');
        $router->model('restrito_cadastros', 'App\Models\Restrito\Cadastro');

		$router->model('consultor-virtual', 'App\Models\Consultoria');
		$router->model('indicacoes', 'App\Models\ConsultoriaIndicacao');
		$router->model('treinamentos', 'App\Models\Treinamento');
		$router->model('eventos', 'App\Models\Evento');
		$router->model('interesses', 'App\Models\InteresseRecebido');
		$router->model('novidades', 'App\Models\Novidade');
		$router->model('categorias_novidades', 'App\Models\NovidadeCategoria');
		$router->model('perguntas-frequentes', 'App\Models\PerguntaFrequente');
		$router->model('categorias_perguntas-frequentes', 'App\Models\PerguntaFrequenteCategoria');
		$router->model('downloads', 'App\Models\Download');
		$router->model('leads', 'App\Models\DownloadLead');
		$router->model('categorias_downloads', 'App\Models\DownloadCategoria');
		$router->model('videos', 'App\Models\Video');
		$router->model('revendedores', 'App\Models\Revendedor');
		$router->model('estados', 'App\Models\RevendedorEstado');
		$router->model('cidades', 'App\Models\RevendedorCidade');
		$router->model('produtos', 'App\Models\Produto');
		$router->model('produtos_aplicacoes', 'App\Models\ProdutoAplicacao');
		$router->model('produtos_obras', 'App\Models\ProdutoObra');
		$router->model('produtos_relacionados', 'App\Models\ProdutoRelacionado');
		$router->model('produtos_instrucoes', 'App\Models\ProdutoInstrucao');
		$router->model('produtos_videos', 'App\Models\ProdutoVideo');
        $router->model('produtos_embalagens', 'App\Models\ProdutoEmbalagem');

		$router->model('popup', 'App\Models\Popup');
		$router->model('detalhes-tecnicos', 'App\Models\DetalheTecnico');
		$router->model('categorias_detalhes-tecnicos', 'App\Models\DetalheTecnicoCategoria');
		$router->model('termos-de-uso', 'App\Models\TermosDeUso');
		$router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('obras', 'App\Models\Obra');
		$router->model('imagens_obras', 'App\Models\ObraImagem');
		$router->model('solucoes', 'App\Models\Solucao');
		$router->model('banners', 'App\Models\Banner');
		$router->model('background', 'App\Models\Background');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('curriculos', 'App\Models\Curriculo');
        $router->model('representantes_recebidos', 'App\Models\RepresentanteRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('produto_slug', function($value) {
            return \App\Models\Produto::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('aplicacao_slug', function($value) {
            return \App\Models\ProdutoAplicacao::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('detalhes_tecnicos_slug', function($value) {
            return
                \App\Models\DetalheTecnicoCategoria::whereSlug($value)->first()
                ?: abort('404');
        });
        $router->bind('obra_slug', function($value) {
            return \App\Models\Obra::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('faq_categoria_slug', function($value) {
            return \App\Models\PerguntaFrequenteCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('download_categoria_slug', function($value) {
            return \App\Models\DownloadCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('novidade_categoria_slug', function($value) {
            return \App\Models\NovidadeCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('novidade_slug', function($value) {
            return \App\Models\Novidade::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('download_restrito_categoria_slug', function($value) {
            return \App\Models\Restrito\DownloadCategoria::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group([
            'namespace'  => $this->namespace,
            'middleware' => ['web'],
        ], function() {
            require base_path('routes/institucional.php');
            require base_path('routes/restrito.php');
            require base_path('routes/localization.php');
            require base_path('routes/painel.php');
        });
    }
}
