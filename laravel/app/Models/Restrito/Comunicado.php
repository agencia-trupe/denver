<?php

namespace App\Models\Restrito;

use Carbon\Carbon;
use App\Helpers\CropImage;

class Comunicado extends Model
{
    protected $table = 'restrito_comunicados';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeAtivos($query)
    {
        return $query->where('ativo', '=', 1);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 140,
                'height' => 180,
                'path'   => 'assets/img/comunicados/thumbs/'
            ],
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/comunicados/'
            ]
        ]);
    }

    public function getFilePathAttribute()
    {
        return storage_path('app/restrito/comunicados/'.$this->arquivo);
    }
}
