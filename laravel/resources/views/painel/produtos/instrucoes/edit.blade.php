@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Editar Instrução</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.instrucoes.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.produtos.instrucoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
