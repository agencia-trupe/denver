@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.produtos.index') }}" title="Voltar para Produtos" class="btn btn-sm btn-default">
        &larr; Voltar para Produtos    </a>

    <legend>
        <h2>
            <small>Produtos /</small> Aplicações

            <a href="{{ route('painel.produtos.aplicacoes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Aplicação</a>
        </h2>
    </legend>

    @if(!count($aplicacoes))
    <div class="alert alert-warning" role="alert">Nenhuma aplicação cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos_aplicacoes">
        <thead>
            <tr>
                <th>Título PT</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($aplicacoes as $aplicacao)
            <tr class="tr-row" id="{{ $aplicacao->id }}">
                <td>{{ $aplicacao->titulo_pt }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.produtos.aplicacoes.destroy', $aplicacao->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.aplicacoes.edit', $aplicacao->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
