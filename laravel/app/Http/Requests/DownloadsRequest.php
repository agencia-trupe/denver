<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DownloadsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'downloads_categoria_id' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'capa' => 'required|image',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
