<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TreinamentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'descricao_pt' => 'required',
            'descricao_en' => '',
            'descricao_es' => '',
            'telefone' => '',
            'site' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
