@extends('frontend.common.template')

@section('content')

    <div class="politica-termos">
        <div class="center">
            <h1 class="main-title">{{ t('nav.termos') }}</h1>

            {!! tobj($termosDeUso, 'texto') !!}
        </div>
    </div>

@endsection
