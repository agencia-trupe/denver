@extends('frontend.restrito.template')

@section('main')

    <div class="videos">
        @include('frontend.restrito._title')

        <div class="video-show">
            <div class="video">
                <iframe src="{{ $treinamento->video_url }}" picture-in-picture allowfullscreen></iframe>
            </div>

            <h1>{{ $treinamento->titulo }}</h1>

            <a href="{{ route('area-restrita.treinamentos') }}" class="voltar">Voltar</a>
        </div>
    </div>

@endsection
