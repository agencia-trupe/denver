<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ObrasImagensRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'required|image'
        ];
    }
}
