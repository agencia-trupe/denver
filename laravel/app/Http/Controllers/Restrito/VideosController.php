<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Models\Restrito\Video;

class VideosController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            $video = Video::findOrFail($id);

            return $video->hasPerfil
                ? view('frontend.restrito.videos.show', compact('video'))
                : abort('404');
        }

        $videos = Video::perfil()->ordenados()->get();

        return view('frontend.restrito.videos.index', compact('videos'));
    }

    public function download($id)
    {
        $video = Video::findOrFail($id);

        return $video->hasPerfil
            ? response()->download($video->filePath)
            : abort('404');
    }
}
