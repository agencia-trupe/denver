<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class PerguntaFrequente extends Model
{
    protected $table = 'perguntas_frequentes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('perguntas_frequentes_categoria_id', $categoria_id);
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'pergunta_'.$locale,
            'resposta_'.$locale
        ];

        return $query->where(function($q) use ($fields, $value) {
            foreach($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('pergunta_'.$locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = route('conteudo-tecnico.perguntas-frequentes', [
            $this->categoria->slug,
            'scroll-id' => $this->id
        ]);
        $obj->label = tobj($this, 'pergunta');

        return $obj;
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\PerguntaFrequenteCategoria', 'perguntas_frequentes_categoria_id');
    }
}
