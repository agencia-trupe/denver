@extends('frontend.common.template')

@section('content')

    <div class="produtos">
        <div class="center">
            <div class="produtos-breadcrumb">
                <span>{{ t('produtos.produtos') }}</span>
                <div class="arrow"></div>
                <select name="aplicacao" class="handle-select-route">
                    <option value="0" data-route="{{ route('produtos') }}">
                        {{ t('produtos.todos') }}
                    </option>
                    @foreach($aplicacoes as $a)
                    <option
                        value="{{ $a->id }}"
                        data-route="{{ route('produtos', $a->slug) }}"
                        @if($aplicacao && $a->id === $aplicacao->id) selected @endif
                    >
                        {{ tobj($a, 'titulo') }}
                    </option>
                    @endforeach
                </select>
                <div class="arrow"></div>
                <select name="produto" class="handle-select-route">
                    @foreach($produtos as $p)
                    <option
                        value="{{ $p->id }}"
                        data-route="{{ route('produtos.show', $p->slug) }}"
                        @if($produto && $p->id === $produto->id) selected @endif
                    >
                        {{ tobj($p, 'titulo') }}
                    </option>
                    @endforeach
                </select>
            </div>

            <h1 class="main-title">{{ tobj($produto, 'titulo') }}</h1>

            <div class="produtos-show">
                <div class="left">
                    <h2>{{ tobj($produto, 'subtitulo') }}</h2>
                    <p>{{ tobj($produto, 'descricao') }}</p>
                    <img src="{{ asset('assets/img/produtos/ampliacao/'.$produto->imagem) }}" alt="">
                </div>

                <div class="right">
                    <div class="produto-info">
                        <h3>
                            <img src="{{ asset('assets/img/layout/ico-produtos-consumo.svg') }}" alt="">
                            <span>{{ t('produtos.consumo') }}</span>
                        </h3>
                        <p>{!! tobj($produto, 'consumo') !!}</p>
                    </div>

                    <div class="produto-info">
                        <h3>
                            <img src="{{ asset('assets/img/layout/ico-produtos-embalagem.svg') }}" alt="">
                            <span>{{ t('produtos.embalagem') }}</span>
                        </h3>
                        <p>{!! tobj($produto, 'embalagem') !!}</p>
                    </div>

                    @if(count($produto->instrucoes))
                    <div class="produto-info">
                        <h3>
                            <img src="{{ asset('assets/img/layout/ico-produtos-instrucoes.svg') }}" alt="">
                            <span>{{ t('produtos.instrucoes') }}</span>
                        </h3>
                        @foreach($produto->instrucoes as $instrucao)
                        <a href="{{ asset('arquivos/produtos/instrucoes/'.$instrucao->arquivo) }}" target="_blank" class="produto-pdf">
                            {{ tobj($instrucao, 'titulo') }}
                        </a>
                        @endforeach
                    </div>
                    @endif

                    <div class="produto-usos">
                        <h3>{{ t('produtos.usos') }}</h3>
                        <div>{!! tobj($produto, 'usos') !!}</div>
                    </div>

                    @if(count($produto->videos))
                    <div class="produto-info">
                        <h3>
                            <img src="{{ asset('assets/img/layout/ico-produtos-saibamais.svg') }}" alt="">
                            <span>{{ t('produtos.saiba-mais') }}</span>
                        </h3>
                        <div class="produto-videos">
                            @foreach($produto->videos as $video)
                            <a href="{{ $video->video_url }}" class="handle-video">
                                <img src="{{ asset('assets/img/produtos/videos/'.$video->capa) }}" alt="">
                            </a>
                            @endforeach
                        </div>
                    </div>
                    @endif

                    {{-- <div class="produto-calculadora">
                        <a href="#" class="calculadora-button">
                            <div class="button">
                                <img src="{{ asset('assets/img/layout/submenu-produtos-calculadora.svg') }}" alt="">
                                <span>{{ t('nav.produtos-calculadora') }}</span>
                            </div>
                            <p>{{ t('produtos.calculadora.denver-ajuda') }}</p>
                        </a>

                        <form
                            class="calculadora-form"
                            data-ratio="{{ $produto->unidades_por_medida }}"
                            data-result="{{ t('produtos.calculadora.resultado') }}"
                            data-unit="{{ $produto->medida }}"
                            data-product="{{ tobj($produto, 'titulo') }}"
                        >
                            <div>
                                <img src="{{ asset('assets/img/layout/submenu-produtos-calculadora.svg') }}" alt="">
                                <span>{{ t('nav.produtos-calculadora') }}</span>
                            </div>
                            <div>
                                <span>{{ t('produtos.calculadora.quantidade', ['medida' => $produto->medida]) }}</span>
                                <input type="number" step="0.1" min="0.1" max="999999" name="quantidade" required>
                            </div>
                            <button type="submit">{{ t('produtos.calculadora.calcular') }}</button>
                        </form>

                        <div class="calculadora-resultado-wrapper">
                            <div>
                                <img src="{{ asset('assets/img/layout/submenu-produtos-calculadora.svg') }}" alt="">
                                <span>{{ t('nav.produtos-calculadora') }}</span>
                            </div>
                            <div class="calculadora-resultado"></div>
                            <button>OK</button>
                        </div>
                    </div> --}}

                    @if(count($produto->embalagens))
                    <div class="produto-embalagens">
                        <div class="radios">
                            <div class="warning" style="display:none">
                                {{ t('produtos.embalagem-aviso') }}
                            </div>
                            @foreach($produto->embalagens as $embalagem)
                            <label>
                                <input
                                    type="radio"
                                    name="embalagem"
                                    value="{{ $embalagem->id }}"
                                    data-link-online="{{ $embalagem->link_online }}"
                                    data-link-loja="{{ $embalagem->link_loja }}"
                                >
                                <span>{{ t('produtos.embalagem') }}:</span>
                                {{ tobj($embalagem, 'titulo') }}
                            </label>
                            @endforeach
                        </div>
                        <div class="buttons">
                            <button class="link-online">
                                <img src="{{ asset('assets/img/layout/ico-botao-compreonline.svg') }}" alt="">
                                {{ t('produtos.compre-online') }}
                            </button>
                            <a href="{{ route('contato.onde-encontrar') }}" class="link-loja">
                                <img src="{{ asset('assets/img/layout/ico-botao-encontreumaloja.svg') }}" alt="">
                                {{ t('produtos.encontre-loja') }}
                            </a>
                        </div>
                    </div>
                    @endif
                </div>

                @if(count($produto->obras))
                <div class="produto-obras">
                    <h3>{{ t('produtos.obras') }}</h3>
                    <div class="grid">
                        @foreach($produto->obras as $relation)
                        <a href="{{ route('obras', $relation->obra->slug) }}">
                            <img src="{{ asset('assets/img/obras/'.$relation->obra->capa) }}" alt="">
                        </a>
                        @endforeach
                    </div>
                    <a href="{{ route('obras') }}">
                        {{ t('produtos.outras-aplicacoes') }} &raquo;
                    </a>
                </div>
                @endif

                @if(count($produto->relacionados))
                <div class="produto-relacionados">
                    <h3>{{ t('produtos.relacionados') }}</h3>
                    <div class="grid">
                        @foreach($produto->relacionados as $relation)
                        <a href="{{ route('produtos.show', $relation->relacionado->slug) }}">
                            <img src="{{ asset('assets/img/produtos/'.$relation->relacionado->imagem) }}" alt="">
                            <span>{{ tobj($relation->relacionado, 'titulo') }}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
