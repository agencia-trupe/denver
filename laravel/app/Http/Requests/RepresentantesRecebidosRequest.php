<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RepresentantesRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'email'     => 'required|email',
            'curriculo' => 'required|max:4096|mimes:doc,docx,pdf',
            'opt-in'    => 'required|in:1'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'      => t('validation.nome-required'),
            'email.required'     => t('validation.email-required'),
            'email.email'        => t('validation.email-email'),
            'email.unique'       => t('validation.email-unique'),
            'curriculo.required' => t('validation.arquivo-required'),
            'curriculo.mimes'    => t('validation.arquivo-mimes'),
            'curriculo.max'      => t('validation.arquivo-max'),
            'opt-in.required'    => t('validation.opt-in'),
            'opt-in.in'          => t('validation.opt-in'),
        ];
    }
}
