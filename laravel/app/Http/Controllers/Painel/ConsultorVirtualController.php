<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConsultorVirtualRequest;
use App\Http\Controllers\Controller;

use App\Models\Consultoria;

class ConsultorVirtualController extends Controller
{
    public function index(Request $request)
    {
        $tipos     = Consultoria::$tipos;
        $filtro    = $request->query('filtro');

        if ($filtro && array_key_exists($filtro, $tipos)) {
            $registros = Consultoria::whereTipo($filtro)->ordenados()->get();
        } else {
            $registros = Consultoria::orderBy('tipo', 'asc')->ordenados()->get();
        }

        return view('painel.consultor-virtual.index', compact('registros', 'tipos', 'filtro'));
    }

    public function create()
    {
        $tipos = Consultoria::$tipos;

        return view('painel.consultor-virtual.create', compact('tipos'));
    }

    public function store(ConsultorVirtualRequest $request)
    {
        try {

            $input = $request->all();

            Consultoria::create($input);

            return redirect()->route('painel.consultor-virtual.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Consultoria $registro)
    {
        $tipos = Consultoria::$tipos;

        return view('painel.consultor-virtual.edit', compact('registro', 'tipos'));
    }

    public function update(ConsultorVirtualRequest $request, Consultoria $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.consultor-virtual.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Consultoria $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.consultor-virtual.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
