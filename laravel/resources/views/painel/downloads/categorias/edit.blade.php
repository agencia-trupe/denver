@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Downloads /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.downloads.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.downloads.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
