<?php

namespace App\Models\Restrito;

use App\Helpers\CropImage;

class Video extends Model
{
    protected $table = 'restrito_videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 380,
            'height' => 180,
            'path'   => 'assets/img/videos-restritos/'
        ]);
    }

    public function getVideoUrlAttribute()
    {
        switch($this->video_tipo) {
            case 'youtube':
                return "https://youtube.com/embed/$this->video";
            case 'vimeo':
                return "https://player.vimeo.com/video/$this->video";
            default:
                return null;
        }
    }

    public function getFilePathAttribute()
    {
        return storage_path('app/restrito/videos/'.$this->arquivo);
    }
}
