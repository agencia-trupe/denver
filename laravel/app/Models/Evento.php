<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Evento extends Model
{
    protected $table = 'eventos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'titulo_'.$locale,
            'descricao_'.$locale,
        ];

        return $query->where(function($q) use ($fields, $value) {
            foreach($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('titulo_'.$locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = route('conteudo-tecnico.calendario-de-eventos.evento', $this->id);
        $obj->label = tobj($this, 'titulo');

        return $obj;
    }

    public function interesses()
    {
        return $this->morphMany(InteresseRecebido::class, 'evento');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 300,
            'height' => 190,
            'path'   => 'assets/img/eventos/'
        ]);
    }
}
