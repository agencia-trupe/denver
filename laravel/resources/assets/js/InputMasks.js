export default function InputMasks() {
    $('input[name=cpf]').inputmask('999.999.999-99', { jitMasking: true });
    $('input[name=cnpj]').inputmask('99.999.999/9999-99', { jitMasking: true });
    $('input[name=cep]').inputmask('99999-999', { jitMasking: true });
    $('input[name=telefone]').inputmask({
        mask: ['(99) 9999-9999', '(99) 99999-9999'],
        jitMasking: true,
    });
}
