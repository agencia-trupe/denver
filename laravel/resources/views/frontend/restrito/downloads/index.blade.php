@extends('frontend.restrito.template')

@section('main')

    <div class="downloads">
        @include('frontend.restrito._title')

        <div class="grid">
            <div class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('area-restrita.downloads', $cat->slug) }}" @if($cat->id === $categoria->id) class="active" @endif>
                    {{ $cat->titulo }}
                </a>
                @endforeach
            </div>

            @foreach($downloads as $download)
            <a href="{{ route('area-restrita.downloads.download', $download->id) }}">
                <img src="{{ asset('assets/img/downloads-restritos/'.$download->capa) }}" alt="">
                <span>Fazer download do arquivo</span>
                <p>{{ $download->titulo }}</p>
            </a>
            @endforeach
        </div>

        {!! $downloads->render() !!}
    </div>

@endsection
