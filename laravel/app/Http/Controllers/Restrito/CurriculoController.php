<?php

namespace App\Http\Controllers\Restrito;

use App\Helpers\Tools;
use App\Http\Controllers\Controller;
use App\Http\Requests\Restrito\CurriculosRequest;
use Illuminate\Support\Facades\Mail;

use App\Models\Contato;
use App\Models\Restrito\Curriculo;

class CurriculoController extends Controller
{
    public function index()
    {
        $cadastro = auth('restrito')->user();

        return view('frontend.restrito.curriculo.index', compact('cadastro'));
    }

    public function post(CurriculosRequest $request, Curriculo $curriculo)
    {
        try {

            $data = $request->except('opt-in');

            $data['curriculo'] = Tools::fileStorageUpload('curriculo', 'curriculos/');

            $curriculo->create($data);
            $this->sendMail('curriculo', '[CURRÍCULO]', $data);

            return redirect()->route('area-restrita.curriculo')
                ->with('success', 'Currículo enviado com sucesso!');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar currículo: '.$e->getMessage()]);

        }
    }

    public function sendMail($view, $title, $data)
    {
        if (! $email = Contato::first()->email_curriculos_restrito) {
           return false;
        }

        Mail::send('emails.'.$view, $data, function($m) use ($email, $title, $data)
        {
            $m->to($email, config('app.name'))
               ->subject($title.' '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
