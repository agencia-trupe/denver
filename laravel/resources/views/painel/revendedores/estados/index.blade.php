@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.revendedores.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Revendedores
    </a>

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Revendedores /</small> Estados
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Estado</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }} ({{ $registro->uf }})</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.revendedores.estados.cidades.index', $registro->id) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-map-marker" style="margin-right:10px;"></span>Editar Cidades
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
