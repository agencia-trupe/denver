<?php

namespace App\Http\Controllers\Restrito\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

use Auth;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $subject = 'Denver · Redefinição de senha';

    protected $guard  = 'restrito';
    protected $broker = 'restrito';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.restrito');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ], [
            'email.required' => 'Preencha seu e-mail.',
            'email.email'    => 'Insira um e-mail válido.',
        ]);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (\Illuminate\Mail\Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()
                    ->with('sent', 'Um e-mail foi enviado com instruções para a redefinição de senha.');
            case Password::INVALID_USER:
            default:
                return redirect()->back()
                    ->with('error', 'E-mail não encontrado.');
        }
    }

    public function forgotPassword()
    {
        return view('frontend.restrito.auth.forgot-password');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->input('email');

        return view('frontend.restrito.auth.reset', compact('email', 'token'));
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules(), [
            'password.required'  => 'Insira uma senha.',
            'password.min'       => 'Sua senha deve ter no mínimo 6 caracteres.',
            'password.confirmed' => 'A confirmação de senha não confere.',
        ]);

        $credentials = $request->only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);

            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
    }

    protected function getResetSuccessResponse($response)
    {
        return redirect()->route('restrito.login')
            ->with('reset', 'Senha redefinida com sucesso!');
    }
}
