<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosObrasRequest;

use App\Http\Controllers\Controller;
use App\Models\ProdutoObra;
use App\Models\Produto;
use App\Models\Obra;

class ProdutosObrasController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->obras;

        return view('painel.produtos.obras.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        $obras = Obra::ordenados()->lists('titulo_pt', 'id');

        return view('painel.produtos.obras.create', compact('produto', 'obras'));
    }

    public function store(ProdutosObrasRequest $request, Produto $produto)
    {
        try {

            if ($produto->obras->where('obra_id', (int)$request->obra_id)->count()) {
                throw new \Exception('O relacionamento selecionado já existe.');
            }

            $produto->obras()->create(['obra_id' => $request->obra_id]);

            return redirect()->route('painel.produtos.obras.index', $produto)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, ProdutoObra $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.obras.index', $produto)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
