<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SolucoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => '',
            'subtitulo_es' => '',
            'imagem' => 'required|image',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
