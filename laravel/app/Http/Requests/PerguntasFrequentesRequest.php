<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerguntasFrequentesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'perguntas_frequentes_categoria_id' => 'required',
            'pergunta_pt' => 'required',
            'pergunta_en' => '',
            'pergunta_es' => '',
            'resposta_pt' => 'required',
            'resposta_en' => '',
            'resposta_es' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
