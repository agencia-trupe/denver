@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Cadastros /</small> Adicionar Cadastro</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.cadastros.store', 'files' => true]) !!}

        @include('painel.restrito.cadastros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
