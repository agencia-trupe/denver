<?php

namespace App\Http\Controllers;

use App\Models\Novidade;
use App\Models\NovidadeCategoria;

class NovidadesController extends Controller
{
    public function index(NovidadeCategoria $categoria)
    {
        $categorias = NovidadeCategoria::ordenados()->get();

        if ($categoria->exists) {
            $novidades = $categoria->novidades()->publicados()->paginate(14);
        } else {
            $categoria = null;
            $novidades = Novidade::ordenados()->publicados()->paginate(14);
        }

        return view('frontend.conteudo-tecnico.novidades.index', compact(
            'categorias',
            'categoria',
            'novidades'
        ));
    }

    public function show(NovidadeCategoria $categoria, Novidade $novidade)
    {
        if (!$novidade->publicado) abort('404');

        $categorias = NovidadeCategoria::ordenados()->get();

        $relacionados = $novidade->categoria
            ->novidades()
            ->publicados()
            ->where('id', '!=', $novidade->id)
            ->ordenados()
            ->limit(3)
            ->get();

        return view('frontend.conteudo-tecnico.novidades.show', compact(
            'categorias',
            'categoria',
            'novidade',
            'relacionados'
        ));
    }
}
