@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.downloads.index') }}" title="Voltar para Downloads" class="btn btn-sm btn-default">
        &larr; Voltar para Downloads    </a>

    <legend>
        <h2>
            <small>Downloads /</small>
            Leads
            @if($registros->total())
            <small>({{ $registros->total() }} registro{{ $registros->total() != 1 ? 's' : '' }})</small>
            @endif

            <a href="{{ route('painel.downloads.leads.exportar', $_GET) }}" class="btn btn-sm btn-info pull-right @if(!$registros->total()) disabled @endif">
                <span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>
                Exportar XLS
            </a>
        </h2>
    </legend>

    <form action="{{ route('painel.downloads.leads.index') }}" method="GET">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Data de início</label>
                    <input type="text" name="data_inicio" value="{{ request('data_inicio') }}" class="datepicker form-control" data-no-default="true">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Data final</label>
                    <input type="text" name="data_final" value="{{ request('data_final') }}" class="datepicker form-control" data-no-default="true">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Segmento</label>
                    <select name="segmento" class="form-control">
                        <option value="">Segmento</option>
                        @foreach(\App\Models\DownloadLead::segmentos() as $s)
                        <option value="{{ $s }}" @if(request('segmento') == $s) selected @endif>{{ $s }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Material</label>
                    <input type="text" name="material" value="{{ request('material') }}" placeholder="Material" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-block btn-success" style="margin-top:26px">
                    <span class="glyphicon glyphicon-search" style="margin-right:10px;"></span>
                    Filtrar
                </button>
            </div>
            @if(count(request()->all()))
            <div class="col-md-2">
                <a href="{{ route('painel.downloads.leads.index') }}" class="btn btn-block btn-warning" style="margin-top:26px">
                    Limpar
                </a>
            </div>
            @endif
        </div>
    </form>

    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data de Cadastro</th>
                <th>
                    Nome<br>
                    Profissão<br>
                    Telefone<br>
                    E-mail<br>
                </th>
                <th>Segmentos</th>
                <th>Material</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr>
                <td>{{ $registro->created_at->format('d/m/Y H:i') }}</td>
                <td>
                    {{ $registro->nome }}<br>
                    {{ $registro->profissao }}<br>
                    {{ $registro->telefone }}<br>
                    {{-- <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $registro->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button> --}}
                    {{ $registro->email }}
                </td>
                <td><small>
                    {!! implode('<br>', array_map('trim', explode(',', $registro->segmento))) !!}
                </small></td>
                <td>{{ $registro->download->titulo_pt }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.downloads.leads.destroy', $registro],
                        'method' => 'delete'
                    ]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $registros->appends($_GET)->links() }}
    @endif

@endsection
