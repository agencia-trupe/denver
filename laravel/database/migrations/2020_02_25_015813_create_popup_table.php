<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupTable extends Migration
{
    public function up()
    {
        Schema::create('popup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('video');
            $table->string('video_tipo');
            $table->boolean('ativo')->default(false);
            $table->string('data_de_expiracao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('popup');
    }
}
