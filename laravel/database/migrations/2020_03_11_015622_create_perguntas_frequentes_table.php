<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerguntasFrequentesTable extends Migration
{
    public function up()
    {
        Schema::create('perguntas_frequentes_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('perguntas_frequentes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('perguntas_frequentes_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->text('pergunta_pt');
            $table->text('pergunta_en');
            $table->text('pergunta_es');
            $table->text('resposta_pt');
            $table->text('resposta_en');
            $table->text('resposta_es');
            $table->foreign('perguntas_frequentes_categoria_id')->references('id')->on('perguntas_frequentes_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('perguntas_frequentes');
        Schema::drop('perguntas_frequentes_categorias');
    }
}
