    <header>
        <p>
            São Paulo: <strong>{{ $contato->telefone_sao_paulo }}</strong>
            <span>&middot;</span>
            {{ t('template.demais-regioes') }}: <strong>{{ $contato->telefone_demais_regioes }}</strong>
        </p>

        <a href="{{ route('area-restrita') }}" @if(Tools::routeIs([
            'restrito.*',
            'area-restrita*'
        ])) class="active" @endif>
            {{ t('nav.area-restrita') }}
        </a>

        <div class="lang-dropdown">
            <select name="lang" class="handle-select-route">
                @foreach(['pt', 'en', 'es'] as $lang)
                <option
                    value="{{ $lang }}"
                    @if(app()->getLocale() === $lang) selected @endif
                    data-route="{{ route('lang', $lang) }}"
                >
                    {{ strtoupper($lang) }}
                </option>
                @endforeach
            </select>
        </div>
    </header>
