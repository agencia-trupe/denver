<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RevendedoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Revendedor;
use App\Models\RevendedorCidade;
use App\Models\RevendedorEstado;

class RevendedoresController extends Controller
{
    public function index()
    {
        $registros = Revendedor::orderBy('titulo', 'asc')->get();

        return view('painel.revendedores.index', compact('registros'));
    }

    public function create()
    {
        $estados = RevendedorEstado::all();

        return view('painel.revendedores.create', compact('estados'));
    }

    public function store(RevendedoresRequest $request)
    {
        try {

            $input   = $request->except(['estado', 'cidade', 'cidades']);
            $cidades = ($request->get('cidades') ?: []);

            $input['tipo'] = implode(',', $request->get('tipo'));

            $registro = Revendedor::create($input);
            $registro->cidades()->sync($cidades);

            return redirect()->route('painel.revendedores.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Revendedor $registro)
    {
        $estados = RevendedorEstado::all();

        return view('painel.revendedores.edit', compact('registro', 'estados'));
    }

    public function update(RevendedoresRequest $request, Revendedor $registro)
    {
        try {

            $input   = $request->except(['estado', 'cidade', 'cidades']);
            $cidades = ($request->get('cidades') ?: []);

            $input['tipo'] = implode(',', $request->get('tipo'));

            $registro->update($input);
            $registro->cidades()->sync($cidades);

            return redirect()->route('painel.revendedores.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Revendedor $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.revendedores.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function buscaCidades(Request $request)
    {
        $this->validate($request, ['estado' => 'required']);

        return RevendedorCidade::orderBy('nome', 'ASC')
            ->where('estado_id', $request->estado)
            ->select(['nome', 'id'])
            ->get();
    }
}
