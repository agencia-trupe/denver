@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well">
    <strong>Tipo</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    @foreach(['lojas', 'construtoras'] as $tipo)
    <div class="checkbox" style="margin:.5em 0 0">
        <label>
            <input type="checkbox" name="tipo[]" value="{{ $tipo }}" @if(isset($registro) && str_contains($registro->tipo, $tipo) || (count(old('tipo')) && in_array($tipo, old('tipo')))) checked @endif>
            <span style="font-weight:bold">{{ ucfirst($tipo) }}</span>
        </label>
    </div>
    @endforeach
</div>

<hr>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('e_mail', 'E-mail (opcional, separados por vírgula)') !!}
    {!! Form::text('e_mail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('site', 'Site (opcional)') !!}
    {!! Form::text('site', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well">
    <strong>Estados/Cidades</strong>
    <hr style="border-color:#ddd;margin:.5em 0 1em">

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <select name="estado" id="revendedores-estado" class="form-control" data-route="{{ route('painel.revendedores.cidades') }}">
                    <option value="">Estado</option>
                    @foreach($estados as $estado)
                    <option value="{{ $estado->id }}" data-uf="{{ $estado->uf }}">
                        {{ $estado->uf }} - {{ $estado->nome }}
                    </option>
                    @endforeach
                </select>
                <select name="cidade" id="revendedores-cidade" class="form-control" style="margin-top:5px" disabled>
                    <option value="">Cidade</option>
                </select>
            </div>
            <a href="#" class="btn btn-sm btn-success btn-add-cidade">
                <span class="glyphicon glyphicon-plus" style="margin-right:10px"></span>
                Adicionar
            </a>
        </div>
        <div class="col-md-7">
            <ul class="list-group" id="revendedores-cidades-list" style="margin:0">
            </ul>
        </div>
    </div>

</div>

@if(isset($registro) && count($registro->cidades))
<script>
    window.DENVER_REVENDEDORES_CIDADES = [
        @foreach($registro->cidades as $cidade)
        {
            estado: {{ $cidade->estado->id }},
            cidade: {{ $cidade->id }},
            uf: '{{ $cidade->estado->uf }}',
            nome: '{{ $cidade->nome }}',
        },
        @endforeach
    ];
</script>
@endif

@if(old('cidades'))
<?php $oldCidades = \App\Models\RevendedorCidade::with('estado')->findMany(old('cidades')); ?>
<script>
    window.DENVER_REVENDEDORES_CIDADES = [
        @foreach($oldCidades as $cidade)
        {
            estado: {{ $cidade->estado->id }},
            cidade: {{ $cidade->id }},
            uf: '{{ $cidade->estado->uf }}',
            nome: '{{ $cidade->nome }}',
        },
        @endforeach
    ];
</script>
@endif

<style>
.list-group-item {
    display: flex;
    align-items: center;
    justify-content: space-between;
}
</style>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.revendedores.index') }}" class="btn btn-default btn-voltar">Voltar</a>
