<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data_pt' => 'required',
            'data_en' => '',
            'data_es' => '',
            'capa' => 'required|image',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'descricao_pt' => 'required',
            'descricao_en' => '',
            'descricao_es' => '',
            'telefone' => '',
            'site' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
