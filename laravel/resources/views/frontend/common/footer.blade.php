    <div class="social-bar">
        <div class="center">
            <div class="absolut_soprema">
                <p>{{ t('template.acompanhe') }}</p> 
                <p class="pers">#DENVERIMPERAGORAÉSOPREMA</p>
            </div>
            {{-- @foreach(['facebook', 'instagram', 'youtube', 'linkedin'] as $s)
                @if($contato->{$s})
                    <a href="{{ Tools::parseLink($contato->{$s}) }}" target="_blank" class="{{ $s }}">
                        {!! file_get_contents(resource_path('assets/img/redessociais-'.$s.'.svg')) !!}
                    </a>
                @endif
            @endforeach --}}
            <div class="social_footer">   
                @if($contato->facebook)
                    <a href="{{$contato->facebook}}">
                        <div class="img_socials"></div>
                    </a>
                @endif  
                @if($contato->instagram)
                    <a href="{{$contato->instagram}}">
                        <div class="img_socials2"></div>
                    </a>
                @endif  
                @if($contato->youtube)
                <a href="{{$contato->youtube}}">
                    <div class="img_socials3"></div>
                </a>
                @endif   
                @if($contato->linkedin)
                <a href="{{$contato->linkedin}}">
                    <div class="img_socials4"></div>
                </a>
                @endif
            </div>
        </div>
    </div>
    <div class="divisor_hr">
        <div class="divisor1_hr"></div>
        <div class="divisor2_hr"></div>
    </div>

    <footer>
        <div class="center">
            <div class="left">
                <img src="{{ asset('assets/img/layout/marca-soprema.svg') }}" alt="Soprema">
                <img style="width:60%" src="{{ asset('assets/img/layout/marca-denver.svg') }}" alt="Denver">

                <div class="endereco">
                    <h4>SÃO PAULO</h4>
                    <h3>{{ $contato->telefone_sao_paulo }}</h3>
                    <p>{!! $contato->endereco_sao_paulo !!}</p>
                </div>

                <div class="endereco">
                    <h4>FEIRA DE SANTANA</h4>
                    <h3>{{ $contato->telefone_feira_de_santana }}</h3>
                    <p>{!! $contato->endereco_feira_de_santana !!}</p>
                </div>

                <a href="{{ route('area-restrita') }}">{{ t('nav.area-restrita') }}</a>
            </div>

            <div class="right">
                <div class="links">
                    <div class="col">
                        <a href="{{ route('home') }}">Home</a>
                        <a href="{{ route('empresa') }}">{{ t('nav.empresa') }}</a>
                        <a href="{{ route('contato') }}">{{ t('nav.contato') }}</a>
                    </div>
                    <div class="col">
                        <a href="{{ route('produtos') }}">{{ t('nav.produtos') }}</a>
                        <a href="{{ route('produtos') }}" class="sub">&rsaquo; {{ t('nav.produtos-todos') }}</a>
                        {{-- <a href="{{ route('produtos.calculadora') }}" class="sub">&rsaquo; {{ t('nav.produtos-calculadora') }}</a> --}}
                    </div>
                    <div class="col">
                        @foreach($aplicacoes as $aplicacao)
                            <a href="{{ route('produtos', $aplicacao->slug) }}" class="sub">
                                &rsaquo; {{ tobj($aplicacao, 'titulo') }}
                            </a>
                        @endforeach
                    </div>
                    <div class="col">
                        <a href="{{ route('detalhes-tecnicos') }}">{{ t('nav.detalhes-tecnicos') }}</a>
                        <a href="{{ route('obras') }}">{{ t('nav.obras') }}</a>
                        <a href="{{ route('conteudo-tecnico') }}">{{ t('nav.conteudo-tecnico') }}</a>
                        <a href="{{ route('conteudo-tecnico.novidades') }}" class="sub">&rsaquo; {{ t('nav.novidades') }}</a>
                        <a href="{{ route('conteudo-tecnico.calendario-de-eventos') }}" class="sub">&rsaquo; {{ t('nav.calendario') }}</a>
                        <a href="{{ route('conteudo-tecnico.videos') }}" class="sub">&rsaquo; {{ t('nav.videos') }}</a>
                        <a href="{{ route('conteudo-tecnico.downloads') }}" class="sub">&rsaquo; {{ t('nav.downloads') }}</a>
                        <a href="{{ route('conteudo-tecnico.perguntas-frequentes') }}" class="sub">&rsaquo; {{ t('nav.faq') }}</a>
                        <a href="{{ route('conteudo-tecnico.consultor-virtual') }}" class="sub">&rsaquo; {{ t('nav.consultor') }}</a>
                    </div>
                </div>

                <div class="selos">
                    <div class="politica-termos">
                        <a href="{{ route('politica-de-privacidade') }}">
                            &middot; {{ t('nav.politica') }}
                        </a>
                        <a href="{{ route('termos-de-uso') }}">
                            &middot; {{ t('nav.termos') }}
                        </a>
                    </div>

                    <div class="selo">
                        <span>ISO 9001</span>
                        <img src="{{ asset('assets/img/layout/selo-rodape-iso9001.png') }}" alt="ISO 9001">
                    </div>
                    <div class="selo">
                        <span>Empresa associada</span>
                        <img src="{{ asset('assets/img/layout/selo-rodape-IBI.png') }}" alt="Empresa associada">
                    </div>
                    <div class="selo">
                        <span>Certificado</span>
                        <img src="{{ asset('assets/img/layout/selo-rodape-abrinq.png') }}" alt="Certificado">
                    </div>
                </div>

                <div class="copyright">
                    <p>
                        © {{ date('Y') }} {{ config('app.name') }} - {{ t('template.direitos') }}
                        <span>|</span>
                        <a href="https://www.trupe.net" target="_blank">{{ t('template.criacao-sites') }}</a>:
                        <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
