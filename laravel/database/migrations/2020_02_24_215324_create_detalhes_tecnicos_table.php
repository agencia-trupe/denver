<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalhesTecnicosTable extends Migration
{
    public function up()
    {
        Schema::create('detalhes_tecnicos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('capa');
            $table->timestamps();
        });

        Schema::create('detalhes_tecnicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detalhes_tecnicos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('arquivo');
            $table->foreign('detalhes_tecnicos_categoria_id')->references('id')->on('detalhes_tecnicos_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('detalhes_tecnicos');
        Schema::drop('detalhes_tecnicos_categorias');
    }
}
