@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Detalhes Técnicos /</small> Editar Detalhe Técnico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.detalhes-tecnicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.detalhes-tecnicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
