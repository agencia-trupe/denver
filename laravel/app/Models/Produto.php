<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function aplicacoes()
    {
        return $this
            ->belongsToMany('App\Models\ProdutoAplicacao', 'produto_aplicacao', 'produto_id', 'produto_aplicacao_id')
            ->ordenados();
    }

    public function obras()
    {
        return $this->hasMany('App\Models\ProdutoObra', 'produto_id')->ordenados();
    }

    public function relacionados()
    {
        return $this->hasMany('App\Models\ProdutoRelacionado', 'produto_id')->ordenados();
    }

    public function instrucoes()
    {
        return $this->hasMany('App\Models\ProdutoInstrucao', 'produto_id')->ordenados();
    }

    public function videos()
    {
        return $this->hasMany('App\Models\ProdutoVideo', 'produto_id')->ordenados();
    }

    public function embalagens()
    {
        return $this->hasMany('App\Models\ProdutoEmbalagem', 'produto_id')->ordenados();
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'titulo_' . $locale,
            'subtitulo_' . $locale,
            'descricao_' . $locale,
        ];

        return $query->where(function ($q) use ($fields, $value) {
            foreach ($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('titulo_' . $locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = route('produtos.show', $this->slug);
        $obj->label = tobj($this, 'titulo');

        return $obj;
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'       => 300,
                'height'      => 300,
                'path'        => 'assets/img/produtos/',
                'transparent' => true,
            ],
            [
                'width'       => 650,
                'height'      => 650,
                'path'        => 'assets/img/produtos/ampliacao/',
                'transparent' => true,
            ]
        ]);
    }
}
