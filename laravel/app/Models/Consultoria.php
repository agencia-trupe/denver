<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Consultoria extends Model
{
    protected $table = 'consultor_virtual';

    protected $guarded = ['id'];

    public static $tipos = [
        'problema' => 'Local do Problema',
        'solucao'  => 'Solução',
    ];

    public function indicacoes()
    {
        return $this->hasMany('App\Models\ConsultoriaIndicacao', 'consultoria_id')->ordenados();
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
