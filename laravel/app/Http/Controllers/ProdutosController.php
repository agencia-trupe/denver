<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Produto;
use App\Models\ProdutoAplicacao;

class ProdutosController extends Controller
{
    public function index(ProdutoAplicacao $aplicacao, Request $request)
    {
        $aplicacoes = ProdutoAplicacao::ordenados()->get();

        if ($aplicacao->exists) {
            return view('frontend.produtos.index', [
                'aplicacoes' => $aplicacoes,
                'aplicacao'  => $aplicacao,
                'produtos'   => $aplicacao->produtos,
            ]);
        }

        $produtos = Produto::ordenados();

        if ($request->get('busca')) {
            $produtos = $produtos->busca($request->get('busca'));
        }

        return view('frontend.produtos.index', [
            'aplicacoes' => $aplicacoes,
            'aplicacao'  => null,
            'produtos'   => $produtos->get()
        ]);
    }

    public function show(Produto $produto)
    {
        if (request('aplicacao')) {
            $aplicacao = ProdutoAplicacao::whereSlug(request('aplicacao'))->firstOrFail();
        } else {
            $aplicacao = $produto->aplicacoes()->firstOrFail();
        }

        return view('frontend.produtos.show', [
            'aplicacoes' => ProdutoAplicacao::ordenados()->get(),
            'aplicacao'  => $aplicacao,
            'produtos'   => $aplicacao->produtos,
            'produto'    => $produto
        ]);
    }

    public function calculadora(Produto $produto)
    {
        return view('frontend.produtos.calculadora', [
            'produtos' => Produto::ordenados()->get(),
            'produto'  => $produto->exists ? $produto: null
        ]);
    }
}
