@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Perguntas Frequentes /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.perguntas-frequentes.categorias.store']) !!}

        @include('painel.perguntas-frequentes.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
