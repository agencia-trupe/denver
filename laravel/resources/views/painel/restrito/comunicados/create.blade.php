@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Comunicados /</small> Adicionar Comunicado</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.comunicados.store', 'files' => true]) !!}

        @include('painel.restrito.comunicados.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
