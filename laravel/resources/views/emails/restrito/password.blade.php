<!DOCTYPE html>
<html>
<head>
    <title>Denver · Redefinição de senha</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-size:16px;line-height:1.5;font-family:Verdana, sans-serif;'>
        <a href="{{ url('area-restrita/redefinicao-de-senha', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">Clique aqui para redefinir sua senha.</a>
    </span>
</body>
</html>
