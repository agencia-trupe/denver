<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovidadesTable extends Migration
{
    public function up()
    {
        Schema::create('novidades_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('novidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('novidades_categoria_id')->unsigned()->nullable();
            $table->string('data');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->string('capa');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->boolean('publicado')->default(false);
            $table->boolean('destaque_home')->default(false);
            $table->foreign('novidades_categoria_id')->references('id')->on('novidades_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('novidades');
        Schema::drop('novidades_categorias');
    }
}
