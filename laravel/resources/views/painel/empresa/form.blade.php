@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1') !!}
    @if($registro->imagem_1)
    <img src="{{ url('assets/img/empresa/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2') !!}
    @if($registro->imagem_2)
    <img src="{{ url('assets/img/empresa/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_3', 'Imagem 3') !!}
    @if($registro->imagem_3)
    <img src="{{ url('assets/img/empresa/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_4', 'Imagem 4') !!}
    @if($registro->imagem_4)
    <img src="{{ url('assets/img/empresa/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_5', 'Imagem 5') !!}
    @if($registro->imagem_5)
    <img src="{{ url('assets/img/empresa/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_1_pt', 'Texto 1 PT') !!}
            {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_pt', 'Texto 3 PT') !!}
            {!! Form::textarea('texto_3_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_1_en', 'Texto 1 EN') !!}
            {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_en', 'Texto 3 EN') !!}
            {!! Form::textarea('texto_3_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_1_es', 'Texto 1 ES') !!}
            {!! Form::textarea('texto_1_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_es', 'Texto 2 ES') !!}
            {!! Form::textarea('texto_2_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_es', 'Texto 3 ES') !!}
            {!! Form::textarea('texto_3_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_pt', 'Missão PT') !!}
            {!! Form::textarea('missao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('visao_pt', 'Visão PT') !!}
            {!! Form::textarea('visao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('valores_pt', 'Valores PT') !!}
            {!! Form::textarea('valores_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_en', 'Missão EN') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('visao_en', 'Visão EN') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('valores_en', 'Valores EN') !!}
            {!! Form::textarea('valores_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_es', 'Missão ES') !!}
            {!! Form::textarea('missao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('visao_es', 'Visão ES') !!}
            {!! Form::textarea('visao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('valores_es', 'Valores ES') !!}
            {!! Form::textarea('valores_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('box_titulo_pt', 'Box Título PT') !!}
            {!! Form::text('box_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('box_texto_pt', 'Box Texto PT') !!}
            {!! Form::textarea('box_texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('box_titulo_en', 'Box Título EN') !!}
            {!! Form::text('box_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('box_texto_en', 'Box Texto EN') !!}
            {!! Form::textarea('box_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('box_titulo_es', 'Box Título ES') !!}
            {!! Form::text('box_titulo_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('box_texto_es', 'Box Texto ES') !!}
            {!! Form::textarea('box_texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
