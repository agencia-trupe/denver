<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Newsletter;

class NewsletterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'  => 'required|email|unique:newsletter,email',
            'perfil' => 'required|in:'.join(',', Newsletter::perfis())
        ];
    }

    public function messages()
    {
        return [
            'email.required'  => t('validation.email-required'),
            'email.email'     => t('validation.email-email'),
            'email.unique'    => t('validation.email-unique'),
            'perfil.required' => t('validation.perfil-required'),
            'perfil.in'       => t('validation.perfil-in'),
        ];
    }
}
