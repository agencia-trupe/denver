@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Downloads /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.downloads.categorias.store']) !!}

        @include('painel.restrito.downloads.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
