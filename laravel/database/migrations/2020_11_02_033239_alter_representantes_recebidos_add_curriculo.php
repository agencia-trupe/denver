<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRepresentantesRecebidosAddCurriculo extends Migration
{
    public function up()
    {
        Schema::table('representantes_recebidos', function (Blueprint $table) {
            $table->string('curriculo')->after('email');
        });
    }

    public function down()
    {
        Schema::table('representantes_recebidos', function (Blueprint $table) {
            $table->dropColumn('curriculo');
        });
    }
}
