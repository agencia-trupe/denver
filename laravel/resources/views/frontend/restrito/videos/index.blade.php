@extends('frontend.restrito.template')

@section('main')

    <div class="videos">
        @include('frontend.restrito._title')

        <div class="videos-grid">
            @foreach($videos as $video)
            <div>
                <a href="{{ route('area-restrita.videos', $video->id) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/videos-restritos/'.$video->capa) }}" alt="">
                    </div>
                    <span>{{ $video->titulo }}</span>
                </a>
                <a href="{{ route('area-restrita.videos.download', $video->id) }}" class="download-link">
                    Fazer download do vídeo
                </a>
            </div>
            @endforeach
        </div>
    </div>

@endsection
