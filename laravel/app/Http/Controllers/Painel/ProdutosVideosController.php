<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosVideosRequest;

use App\Http\Controllers\Controller;

use App\Models\ProdutoVideo;
use App\Models\Produto;

class ProdutosVideosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->videos;

        return view('painel.produtos.videos.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.videos.create', compact('produto'));
    }

    public function store(ProdutosVideosRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ProdutoVideo::upload_capa();

            $produto->videos()->create($input);

            return redirect()->route('painel.produtos.videos.index', $produto)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, ProdutoVideo $registro)
    {
        return view('painel.produtos.videos.edit', compact('produto', 'registro'));
    }

    public function update(ProdutosVideosRequest $request, Produto $produto, ProdutoVideo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = ProdutoVideo::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.produtos.videos.index', $produto)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, ProdutoVideo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.videos.index', $produto)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
