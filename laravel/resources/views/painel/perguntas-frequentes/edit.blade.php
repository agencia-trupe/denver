@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Perguntas Frequentes /</small> Editar Pergunta Frequente</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.perguntas-frequentes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.perguntas-frequentes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
