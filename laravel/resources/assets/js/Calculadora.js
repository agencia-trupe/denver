export default function() {
    const $resultEl = $('.calculadora-resultado');
    $resultEl.hide();
    $(`
        .produto-calculadora .calculadora-form,
        .produto-calculadora .calculadora-resultado-wrapper
    `).hide();

    $('.calculadora-button').click(function click(event) {
        event.preventDefault();
        $(this).hide();
        $('.calculadora-form').show();
    });

    $('.calculadora-form').submit(function calculadora(event) {
        event.preventDefault();

        const $formEl = $(this);

        const amount = $formEl.find('input[name=quantidade]').val();
        const { ratio, result: resultString, product, unit } = $formEl.data();

        if (amount > 0 && ratio && resultString && product && unit) {
            const replacements = {
                '%m': `${amount} <span>${unit}</span>`,
                '%u': +`${Math.round(`${ratio * amount}e+2`)}e-2`,
                '%p': product,
            };

            $resultEl
                .html(
                    `${resultString.replace(
                        /%\w/g,
                        replace => `<strong>${replacements[replace]}</strong>`
                    )}`
                )
                .show();

            if ($formEl.parent().hasClass('produto-calculadora')) {
                $formEl.hide();
                $formEl[0].reset();
                $('.calculadora-resultado-wrapper').show();
            }
        }
    });

    $('.calculadora-resultado-wrapper button').click(function click() {
        $(this)
            .parent()
            .hide();
        $('.calculadora-button').show();
    });
}
