<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests\Restrito\ComunicadosRequest;
use App\Http\Controllers\Controller;

use App\Models\Restrito\Comunicado;

use App\Helpers\Tools;

class ComunicadosController extends Controller
{
    public function index()
    {
        $registros = Comunicado::ordenados()->get();

        return view('painel.restrito.comunicados.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.restrito.comunicados.create');
    }

    public function store(ComunicadosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo']) && isset($input['imagem'])) {
                throw new \Exception('Preencha apenas um dos campos (imagem/arquivo).');
            }

            $input['perfil'] = implode(',', $request->get('perfil'));

            if (isset($input['imagem'])) {
                $input['arquivo'] = '';
                $input['imagem'] = Comunicado::upload_imagem();
            } elseif (isset($input['arquivo'])) {
                $input['imagem'] = '';
                $input['arquivo'] = Tools::fileStorageUpload('arquivo', 'restrito/comunicados/');
            }

            Comunicado::create($input);

            return redirect()->route('painel.restrito.comunicados.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function edit(Comunicado $registro)
    {
        return view('painel.restrito.comunicados.edit', compact('registro'));
    }

    public function update(ComunicadosRequest $request, Comunicado $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo']) && isset($input['imagem'])) {
                throw new \Exception('Preencha apenas um dos campos (imagem/arquivo).');
            }

            $input['perfil'] = implode(',', $request->get('perfil'));

            if (isset($input['imagem'])) {
                $input['arquivo'] = '';
                $input['imagem'] = Comunicado::upload_imagem();
            } elseif (isset($input['arquivo'])) {
                $input['imagem'] = '';
                $input['arquivo'] = Tools::fileStorageUpload('arquivo', 'restrito/comunicados/');
            }

            $registro->update($input);

            return redirect()->route('painel.restrito.comunicados.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function destroy(Comunicado $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.restrito.comunicados.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
