@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('downloads_categoria_id', 'Categoria') !!}
    {!! Form::select('downloads_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/downloads/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <div style="margin-bottom:5px">
        <a href="{{ asset('arquivos/downloads/'.$registro->arquivo) }}">{{ $registro->arquivo }}</a>
    </div>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_email', 'Texto do e-mail (opcional, utilizado caso o arquivo esteja em uma categoria que exige cadastro)') !!}
    {!! Form::textarea('texto_email', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.downloads.index') }}" class="btn btn-default btn-voltar">Voltar</a>
