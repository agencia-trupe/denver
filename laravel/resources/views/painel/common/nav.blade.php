<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs([
        'painel.background*',
        'painel.banners*',
        'painel.solucoes*',
        'painel.popup*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.background*')) class="active" @endif>
                <a href="{{ route('painel.background.index') }}">Background</a>
            </li>
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.solucoes*')) class="active" @endif>
                <a href="{{ route('painel.solucoes.index') }}">Soluções</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.popup*')) class="active" @endif>
                <a href="{{ route('painel.popup.index') }}">Popup</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs([
        'painel.empresa*',
        'painel.obras*',
        'painel.politica-de-privacidade*',
        'painel.termos-de-uso*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Institucional
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.empresa*')) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>
            <li @if(Tools::routeIs('painel.obras*')) class="active" @endif>
                <a href="{{ route('painel.obras.index') }}">Obras</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.politica-de-privacidade*')) class="active" @endif>
                <a href="{{ route('painel.politica-de-privacidade.index') }}">Política de Privacidade</a>
            </li>
            <li @if(Tools::routeIs('painel.termos-de-uso*')) class="active" @endif>
                <a href="{{ route('painel.termos-de-uso.index') }}">Termos de Uso</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.produtos*')) class="active" @endif>
		<a href="{{ route('painel.produtos.index') }}">Produtos</a>
	</li>

    <li class="dropdown @if(Tools::routeIs([
        'painel.detalhes-tecnicos*',
        'painel.novidades*',
        'painel.eventos*',
        'painel.treinamentos*',
        'painel.videos*',
        'painel.downloads*',
        'painel.perguntas-frequentes*',
        'painel.consultor-virtual*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Conteúdo Técnico
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.detalhes-tecnicos*')) class="active" @endif>
                <a href="{{ route('painel.detalhes-tecnicos.index') }}">Detalhes Técnicos</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.novidades*')) class="active" @endif>
                <a href="{{ route('painel.novidades.index') }}">Novidades</a>
            </li>
            <li @if(Tools::routeIs('painel.eventos*')) class="active" @endif>
                <a href="{{ route('painel.eventos.index') }}">Eventos</a>
            </li>
            <li @if(Tools::routeIs('painel.treinamentos*')) class="active" @endif>
                <a href="{{ route('painel.treinamentos.index') }}">Treinamentos</a>
            </li>
            <li @if(Tools::routeIs('painel.videos*')) class="active" @endif>
                <a href="{{ route('painel.videos.index') }}">Vídeos</a>
            </li>
            <li @if(Tools::routeIs('painel.downloads*')) class="active" @endif>
                <a href="{{ route('painel.downloads.index') }}">Downloads</a>
            </li>
            <li @if(Tools::routeIs('painel.perguntas-frequentes*')) class="active" @endif>
                <a href="{{ route('painel.perguntas-frequentes.index') }}">Perguntas Frequentes</a>
            </li>
            <li @if(Tools::routeIs('painel.consultor-virtual*')) class="active" @endif>
                <a href="{{ route('painel.consultor-virtual.index') }}">Consultor Virtual</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs([
        'painel.contato.index',
        'painel.revendedores*',
        'painel.contato.recebidos*',
        'painel.contato.curriculos*',
        'painel.contato.representantes*',
        'painel.newsletter*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.revendedores*')) class="active" @endif>
                <a href="{{ route('painel.revendedores.index') }}">Revendedores</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li @if(Tools::routeIs('painel.contato.curriculos*')) class="active" @endif>
                <a href="{{ route('painel.contato.curriculos.index') }}">
                    Currículos
                </a>
            </li>
            <li @if(Tools::routeIs('painel.contato.representantes*')) class="active" @endif>
                <a href="{{ route('painel.contato.representantes.index') }}">
                    Representantes
                </a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs([
        'painel.restrito.comunicados*',
        'painel.restrito.campanhas*',
        'painel.restrito.downloads*',
        'painel.restrito.videos*',
        'painel.restrito.treinamentos*',
        'painel.restrito.tabelas*',
        'painel.restrito.curriculos*',
        'painel.restrito.cadastros*',
    ])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Área Restrita
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.restrito.comunicados*')) class="active" @endif>
                <a href="{{ route('painel.restrito.comunicados.index') }}">Comunicados</a>
            </li>
            <li @if(Tools::routeIs('painel.restrito.campanhas*')) class="active" @endif>
                <a href="{{ route('painel.restrito.campanhas.index') }}">Campanhas</a>
            </li>
            <li @if(Tools::routeIs('painel.restrito.downloads*')) class="active" @endif>
                <a href="{{ route('painel.restrito.downloads.index') }}">Downloads</a>
            </li>
            <li @if(Tools::routeIs('painel.restrito.videos*')) class="active" @endif>
                <a href="{{ route('painel.restrito.videos.index') }}">Vídeos</a>
            </li>
            <li @if(Tools::routeIs('painel.restrito.treinamentos*')) class="active" @endif>
                <a href="{{ route('painel.restrito.treinamentos.index') }}">Treinamentos</a>
            </li>
            <li @if(Tools::routeIs('painel.restrito.tabelas*')) class="active" @endif>
                <a href="{{ route('painel.restrito.tabelas.index') }}">Tabelas de Preço</a>
            </li>
            <li @if(Tools::routeIs('painel.restrito.curriculos*')) class="active" @endif>
                <a href="{{ route('painel.restrito.curriculos.index') }}">Currículos</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.restrito.cadastros*')) class="active" @endif>
                <a href="{{ route('painel.restrito.cadastros.index') }}">Cadastros</a>
            </li>
        </ul>
    </li>
</ul>
