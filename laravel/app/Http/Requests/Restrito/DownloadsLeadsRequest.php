<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

class DownloadsLeadsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'email'     => 'required|email',
            'opt-in'    => 'required|in:1'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'      => t('validation.nome-required'),
            'email.required'     => t('validation.email-required'),
            'email.email'        => t('validation.email-email'),
            'email.unique'       => t('validation.email-unique'),
            'opt-in.required'    => t('validation.opt-in'),
            'opt-in.in'          => t('validation.opt-in'),
        ];
    }
}
