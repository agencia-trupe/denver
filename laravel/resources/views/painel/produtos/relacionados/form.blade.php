@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produto_id', 'Produto') !!}
    {!! Form::select('produto_id', $produtos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.relacionados.index', $produto) }}" class="btn btn-default btn-voltar">Voltar</a>
