<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revendedor extends Model
{
    protected $table = 'revendedores';

    protected $guarded = ['id'];

    public function cidades()
    {
        return $this
            ->belongsToMany('App\Models\RevendedorCidade', 'revendedor_cidade', 'revendedor_id', 'cidade_id')
            ->orderBy('estado_id', 'asc')
            ->orderBy('nome', 'asc');
    }
}
