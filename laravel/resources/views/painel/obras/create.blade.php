@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Institucional / Obras /</small> Adicionar Obra</h2>
    </legend>

    {!! Form::open(['route' => 'painel.obras.store', 'files' => true]) !!}

        @include('painel.obras.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
