<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Models\Restrito\Campanha;

class CampanhasController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            $campanha = Campanha::findOrFail($id);

            return $campanha->hasPerfil
                ? response()->download($campanha->filePath)
                : abort('404');
        }

        $campanhas = Campanha::perfil()->ordenados()->get();

        return view('frontend.restrito.campanhas.index', compact('campanhas'));
    }
}
