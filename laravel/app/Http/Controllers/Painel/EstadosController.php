<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use App\Models\RevendedorEstado;

class EstadosController extends Controller
{
    public function index()
    {
        $registros = RevendedorEstado::orderBy('nome', 'ASC')->get();

        return view('painel.revendedores.estados.index', compact('registros'));
    }
}
