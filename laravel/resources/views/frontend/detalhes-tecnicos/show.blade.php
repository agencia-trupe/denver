<div class="detalhes-tecnicos-lightbox">
    <h2>{{ tobj($categoria, 'titulo') }}</h2>
    <div class="arquivos">
        @foreach($categoria->detalhes_tecnicos as $arquivo)
        <a href="{{ asset('arquivos/detalhes-tecnicos/'.$arquivo->arquivo) }}" target="_blank">
            {{ tobj($arquivo, 'titulo') }}
        </a>
        @endforeach
    </div>
</div>
