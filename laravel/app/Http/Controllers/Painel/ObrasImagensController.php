<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ObrasImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Obra;
use App\Models\ObraImagem;

use App\Helpers\CropImage;

class ObrasImagensController extends Controller
{
    public function index(Obra $registro)
    {
        $imagens = ObraImagem::obra($registro->id)->ordenados()->get();

        return view('painel.obras.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Obra $registro, ObraImagem $imagem)
    {
        return $imagem;
    }

    public function store(Obra $registro, ObrasImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ObraImagem::uploadImagem();
            $input['obra_id'] = $registro->id;

            $imagem = ObraImagem::create($input);

            $view = view('painel.obras.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Obra $registro, ObraImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.obras.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Obra $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.obras.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
