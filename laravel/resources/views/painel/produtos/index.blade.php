@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Produtos
        <div class="btn-group pull-right">
            <a href="{{ route('painel.produtos.aplicacoes.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-tag" style="margin-right:10px;"></span>Editar Aplicações</a>
            <a href="{{ route('painel.produtos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
        </div>
    </h2>
</legend>

<div class="well">
    {!! Form::open([
    'route' => 'painel.produtos.index',
    'style' => 'display:flex;max-width:450px',
    'method' => 'GET',
    ]) !!}
    <input type="text" name="busca" value="{{ request('busca') }}" class="form-control" placeholder="Buscar por título" style="margin-right:10px">
    {!! Form::select('aplicacao', $aplicacoes, request('aplicacao'), ['class' => 'form-control', 'placeholder' => 'Todas aplicações']) !!}
    <button type="submit" class="btn btn-info" style="margin-left:10px">
        <span class="glyphicon glyphicon-search"></span>
    </button>
    {!! Form::close() !!}
</div>

@if(!count($registros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable"  data-table="produtos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Título PT</th>
            <th>Imagem</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $registro->titulo_pt }}</td>
            <td><img src="{{ asset('assets/img/produtos/'.$registro->imagem) }}" style="width: 100%; max-width:65px;" alt=""></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.produtos.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-th-list" style="margin-right:10px"></span>
                            Informações
                            <b class="caret"></b>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('painel.produtos.instrucoes.index', $registro->id) }}">
                                    Instruções
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('painel.produtos.videos.index', $registro->id) }}">
                                    Vídeos
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('painel.produtos.obras.index', $registro->id) }}">
                                    Obras
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('painel.produtos.embalagens.index', $registro->id) }}">
                                    Embalagens
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('painel.produtos.relacionados.index', $registro->id) }}">
                                    Relacionados
                                </a>
                            </li>
                        </ul>
                    </div>

                    <a href="{{ route('painel.produtos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endif

@endsection