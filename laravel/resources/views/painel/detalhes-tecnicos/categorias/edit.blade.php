@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Detalhes Técnicos /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.detalhes-tecnicos.categorias.update', $categoria->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.detalhes-tecnicos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
