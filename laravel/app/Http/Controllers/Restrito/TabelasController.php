<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Models\Restrito\Tabela;

class TabelasController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            $tabela = Tabela::findOrFail($id);

            return $tabela->hasPerfil
                ? response()->download($tabela->filePath)
                : abort('404');
        }

        $tabelas = Tabela::perfil()->ordenados()->get();

        return view('frontend.restrito.tabelas.index', compact('tabelas'));
    }
}
