<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsultorVirtualIndicacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos' => 'required|array',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'produto_id' => 'produto',
        ];
    }
}
