export default function() {
    $('.handle-video').fancybox({
        type: 'iframe',
        width: 800,
        height: 450,
        aspectRatio: true,
        padding: 0,
    });
}
