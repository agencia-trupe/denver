@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Adicionar Embalagem</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.embalagens.store', $produto->id], 'files' => true]) !!}

        @include('painel.produtos.embalagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
