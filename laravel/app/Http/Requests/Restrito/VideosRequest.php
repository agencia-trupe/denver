<?php

namespace App\Http\Requests\Restrito;

use App\Http\Requests\Request;

use App\Helpers\Tools;

class VideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'perfil' => 'required|array|in:'.implode(',', array_keys(Tools::restritoPerfis())),
            'titulo' => 'required',
            'capa' => 'required|image',
            'video_tipo' => 'required',
            'video' => 'required',
            'arquivo' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['arquivo'] = '';
        }

        return $rules;
    }
}
