<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoVideo extends Model
{
    protected $table = 'produtos_videos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 190,
            'height' => 110,
            'path'   => 'assets/img/produtos/videos/'
        ]);
    }

    public function getVideoUrlAttribute()
    {
        switch($this->video_tipo) {
            case 'youtube':
                return "https://youtube.com/embed/$this->video";
            case 'vimeo':
                return "https://player.vimeo.com/video/$this->video";
            default:
                return null;
        }
    }
}
