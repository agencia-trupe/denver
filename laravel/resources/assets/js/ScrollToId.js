export default function ScrollToId() {
    const params = new URLSearchParams(window.location.search);

    if (params.has('scroll-id')) {
        const id = params.get('scroll-id');

        $('html, body').animate(
            {
                scrollTop: $(`div[data-id=${id}]`).offset().top,
            },
            300
        );
    }
}
