<?php

namespace App\Http\Controllers;

use App\Helpers\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Requests\CurriculosRecebidosRequest;
use App\Http\Requests\RepresentantesRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Curriculo;
use App\Models\RepresentanteRecebido;
use App\Models\Revendedor;
use App\Models\RevendedorCidade;
use App\Models\RevendedorEstado;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail('contato', '[CONTATO]', $data);

        return response()->json([
            'message' => t('contato.sucesso')
        ]);
    }

    public function postCurriculo(CurriculosRecebidosRequest $request, Curriculo $contatoRecebido)
    {
        $data = $request->except('opt-in');

        $data['curriculo'] = Tools::fileStorageUpload('curriculo', 'curriculos/');

        $contatoRecebido->create($data);
        $this->sendMail('curriculo', '[CURRÍCULO]', $data, 'email_curriculos');

        return response()->json([
            'message' => t('contato.sucesso-curriculo')
        ]);
    }

    public function postRepresentante(RepresentantesRecebidosRequest $request, RepresentanteRecebido $contatoRecebido)
    {
        $data = $request->except('opt-in');

        $data['curriculo'] = Tools::fileStorageUpload('curriculo', 'curriculos/');

        $contatoRecebido->create($data);
        $this->sendMail('representante', '[REPRESENTANTE]', $data, 'email_curriculos');

        return response()->json([
            'message' => t('contato.sucesso-representante')
        ]);
    }

    private function sendMail($view, $title, $data, $emailRecebimento = 'email')
    {
        if (! $email = Contato::first()->{$emailRecebimento}) {
           return false;
        }

        Mail::send('emails.'.$view, $data, function($m) use ($email, $title, $data)
        {
            $m->to($email, config('app.name'))
               ->subject($title.' '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }

    public function ondeEncontrar(Request $request)
    {
        $estados = RevendedorEstado::whereHas('cidades', function($q) {
            $q->has('revendedores');
        })->lists('nome', 'id');

        $cidades = [];
        if ($estado = $request->estado) {
            $cidades = RevendedorCidade::where('estado_id', $estado)
                ->orderBy('nome', 'ASC')
                ->has('revendedores')
                ->lists('nome', 'id');
        }

        $revendedores = null;

        if (($request->cidade || $request->estado) && !in_array($request->tipo, ['lojas', 'construtoras'])) {
            return redirect()->route('contato.onde-encontrar');
        }

        if ($request->cidade || $request->estado) {
            session()->flash('scrollToRevendedores', true);
        }

        if ($request->estado && $request->cidade) {
            $cidade = RevendedorCidade::findOrFail($request->cidade);
            $revendedores = $cidade->revendedores()
                ->where('tipo', 'LIKE', "%$request->tipo%")
                ->orderBy('titulo')
                ->get();
        }

        return view('frontend.onde-encontrar', compact(
            'estados',
            'cidades',
            'revendedores'
        ));
    }

    public function buscaCidades(Request $request)
    {
        $this->validate($request, [
            'estado' => 'required',
            'tipo'   => 'required'
        ]);

        return RevendedorCidade::where('estado_id', $request->estado)
            ->orderBy('nome', 'ASC')
            ->whereHas('revendedores', function($q) use ($request) {
                $q->where('tipo', 'LIKE', "%$request->tipo%");
            })
            ->select(['nome', 'id'])->get();
    }
}
