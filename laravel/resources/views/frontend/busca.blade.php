@extends('frontend.common.template')

@section('content')

    <div class="busca-resultados">
        <div class="center">
            <h1 class="main-title">{{ t('template.busca-resultado') }}</h1>

            @if($total >= 1)
                @foreach($resultados as $area => $items)
                    @if(count($items))
                    <div class="area">
                        <h2>{{ t('nav.'.$area) }}</h2>
                        @foreach($items as $item)
                        <a
                            href="{{ $item->busca->route }}"
                            @if($area == 'videos') class="handle-video" @endif
                            @if(in_array($area, ['downloads', 'detalhes-tecnicos'])) target="_blank" @endif
                        >
                            {{ $item->busca->label }}
                        </a>
                        @endforeach
                    </div>
                    @endif
                @endforeach
            @else
                <div class="nenhum">{{ t('template.busca-nenhum') }}</div>
            @endif
        </div>
    </div>

@endsection
