<?php

    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

        /* GENERATED ROUTES */
		Route::resource('consultor-virtual', 'ConsultorVirtualController');
		Route::resource('consultor-virtual.indicacoes', 'ConsultorVirtualIndicacoesController');
		Route::resource('treinamentos', 'TreinamentosController');
		Route::resource('treinamentos.interesses', 'InteressesRecebidosController', [
            'only' => ['index', 'destroy']
        ]);
		Route::resource('eventos', 'EventosController');
		Route::resource('eventos.interesses', 'InteressesRecebidosController', [
            'only' => ['index', 'destroy']
        ]);
		Route::resource('novidades/categorias', 'NovidadesCategoriasController', ['parameters' => ['categorias' => 'categorias_novidades']]);
		Route::resource('novidades', 'NovidadesController');
		Route::resource('perguntas-frequentes/categorias', 'PerguntasFrequentesCategoriasController', ['parameters' => ['categorias' => 'categorias_perguntas-frequentes']]);
        Route::resource('perguntas-frequentes', 'PerguntasFrequentesController');

        Route::resource('downloads/categorias', 'DownloadsCategoriasController', ['parameters' => ['categorias' => 'categorias_downloads']]);
        Route::resource('downloads/leads', 'DownloadsLeadsController', ['only' => ['index', 'destroy']]);
        Route::get('downloads/leads/exportar', 'DownloadsLeadsController@exportar')
            ->name('painel.downloads.leads.exportar');
        Route::resource('downloads', 'DownloadsController');

        Route::resource('videos', 'VideosController');

        Route::get('revendedores/estados', 'EstadosController@index')->name('painel.revendedores.estados');
        Route::resource('revendedores/estados.cidades', 'CidadesController');
        Route::post('revendedores/cidades', 'RevendedoresController@buscaCidades')
        ->name('painel.revendedores.cidades');
        Route::resource('revendedores', 'RevendedoresController');

		Route::resource('produtos/aplicacoes', 'ProdutosAplicacoesController', [
            'parameters' => ['aplicacoes' => 'produtos_aplicacoes']
        ]);
        Route::resource('produtos', 'ProdutosController');
        Route::resource('produtos.obras', 'ProdutosObrasController', [
            'except' => ['edit', 'update'],
            'parameters' => ['obras' => 'produtos_obras']
        ]);
        Route::resource('produtos.relacionados', 'ProdutosRelacionadosController', [
            'except' => ['edit', 'update'],
            'parameters' => ['relacionados' => 'produtos_relacionados']
        ]);
        Route::resource('produtos.instrucoes', 'ProdutosInstrucoesController', [
            'parameters' => ['instrucoes' => 'produtos_instrucoes']
        ]);
        Route::resource('produtos.videos', 'ProdutosVideosController', [
            'parameters' => ['videos' => 'produtos_videos']
        ]);
        Route::resource('produtos.embalagens', 'ProdutosEmbalagensController', [
            'parameters' => ['embalagens' => 'produtos_embalagens']
        ]);

		Route::resource('popup', 'PopupController', ['only' => ['index', 'update', 'destroy']]);
		Route::resource('detalhes-tecnicos/categorias', 'DetalhesTecnicosCategoriasController', [
            'parameters' => ['categorias' => 'categorias_detalhes-tecnicos']
        ]);
		Route::resource('detalhes-tecnicos', 'DetalhesTecnicosController');
		Route::resource('termos-de-uso', 'TermosDeUsoController', ['only' => ['index', 'update']]);
		Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('obras', 'ObrasController');
		Route::get('obras/{obras}/imagens/clear', [
			'as'   => 'painel.obras.imagens.clear',
			'uses' => 'ObrasImagensController@clear'
		]);
		Route::resource('obras.imagens', 'ObrasImagensController', ['parameters' => ['imagens' => 'imagens_obras']]);
		Route::resource('solucoes', 'SolucoesController');
		Route::resource('banners', 'BannersController');
		Route::resource('background', 'BackgroundController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', [
            'as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle'
        ]);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');

        Route::get('contato/curriculos/arquivo/{arquivo}', 'CurriculosRecebidosController@show')->name('painel.contato.curriculos.arquivo');
        Route::resource('contato/curriculos', 'CurriculosRecebidosController', ['only' => ['index', 'destroy']]);

        Route::resource('contato/representantes', 'RepresentantesRecebidosController', ['parameters' => ['representantes' => 'representantes_recebidos'], 'only' => ['index', 'destroy']]);

        Route::resource('contato', 'ContatoController');

        Route::resource('newsletter', 'NewsletterController', ['only' => ['index', 'destroy']]);
        Route::get('newsletter/exportar', 'NewsletterController@exportar')->name('painel.newsletter.exportar');

        Route::resource('usuarios', 'UsuariosController');

		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');

        Route::group([
            'prefix' => 'restrito',
            'namespace' => 'Restrito'
        ], function() {
            Route::resource('comunicados', 'ComunicadosController', [
                'parameters' => ['comunicados' => 'restrito_comunicados']
            ]);
            Route::resource('campanhas', 'CampanhasController', [
                'parameters' => ['campanhas' => 'restrito_campanhas']
            ]);
            Route::resource('downloads/categorias', 'DownloadsCategoriasController', [
                'parameters' => ['categorias' => 'restrito_categorias_downloads']
            ]);
		    Route::resource('downloads', 'DownloadsController', [
                'parameters' => ['downloads' => 'restrito_downloads']
            ]);
            Route::resource('videos', 'VideosController', [
                'parameters' => ['videos' => 'restrito_videos']
            ]);
            Route::resource('treinamentos', 'TreinamentosController', [
                'parameters' => ['treinamentos' => 'restrito_treinamentos']
            ]);
            Route::resource('tabelas', 'TabelasController', [
                'parameters' => ['tabelas' => 'restrito_tabelas']
            ]);
            Route::resource('curriculos', 'CurriculosController', [
                'parameters' => ['curriculos' => 'restrito_curriculos'],
                'only' => ['index', 'destroy']
            ]);
            Route::resource('cadastros', 'CadastrosController', [
                'parameters' => ['cadastros' => 'restrito_cadastros']
            ]);
            Route::get('cadastros/{restrito_cadastros}/aprovar', 'CadastrosController@aprovar')
                ->name('painel.restrito.cadastros.aprovar');

            Route::get('arquivo/{dir}/{file}', 'ArquivoController@show')->name('painel.restrito.arquivo');
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
