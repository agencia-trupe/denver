@extends('frontend.restrito.template')

@section('main')

    <div class="videos">
        @include('frontend.restrito._title')

        <div class="video-show">
            <div class="video">
                <iframe src="{{ $video->video_url }}" picture-in-picture allowfullscreen></iframe>
            </div>

            <h1>{{ $video->titulo }}</h1>

            <a href="{{ route('area-restrita.videos.download', $video->id) }}" class="download-link">
                Fazer download do vídeo
            </a>

            <a href="{{ route('area-restrita.videos') }}" class="voltar">Voltar</a>
        </div>
    </div>

@endsection
