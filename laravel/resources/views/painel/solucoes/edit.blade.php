@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Soluções /</small> Editar Solução</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.solucoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.solucoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
