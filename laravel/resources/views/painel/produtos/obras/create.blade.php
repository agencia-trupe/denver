@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo_pt }} /</small> Adicionar Obra</h2>
    </legend>

    {!! Form::open(['route' => ['painel.produtos.obras.store', $produto->id], 'files' => true]) !!}

        @include('painel.produtos.obras.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
