<!DOCTYPE html>
<html>
<head>
    <title>Arquivo para download</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-size:16px;line-height:1.5;font-family:Verdana, sans-serif;'>
        Olá {{ $nome }},
        @if($download['texto_email'])
        <br><br>{!! $download['texto_email'] !!}
        @endif
        <br><br>
        <a href="{{ asset('arquivos/downloads/'.$download['arquivo']) }}">{{ $download['titulo_pt'] }}</a>
        <br><br>
        <strong>DENVER · <a href="http://www.denverimper.com.br">www.denverimper.com.br</a></strong>
    </span>
</body>
</html>
