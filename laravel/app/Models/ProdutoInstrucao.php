<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoInstrucao extends Model
{
    protected $table = 'produtos_instrucoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
