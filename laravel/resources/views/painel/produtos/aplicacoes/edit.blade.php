@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Aplicação</h2>
    </legend>

    {!! Form::model($aplicacao, [
        'route'  => ['painel.produtos.aplicacoes.update', $aplicacao->id],
        'method' => 'patch'])
    !!}

    @include('painel.produtos.aplicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
