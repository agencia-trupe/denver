@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Consultor Virtual / {{ $consultoria->titulo_pt }} /</small> Adicionar Indicação</h2>
    </legend>

    {!! Form::open(['route' => ['painel.consultor-virtual.indicacoes.store', $consultoria->id], 'files' => true]) !!}

        @include('painel.consultor-virtual.indicacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
