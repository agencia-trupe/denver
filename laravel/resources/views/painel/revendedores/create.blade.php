@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Revendedores /</small> Adicionar Revendedor</h2>
    </legend>

    {!! Form::open(['route' => 'painel.revendedores.store', 'files' => true]) !!}

        @include('painel.revendedores.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
