<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadsTable extends Migration
{
    public function up()
    {
        Schema::create('downloads_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('downloads_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('capa');
            $table->string('arquivo');
            $table->foreign('downloads_categoria_id')->references('id')->on('downloads_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('downloads');
        Schema::drop('downloads_categorias');
    }
}
