<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('frontend.*', function ($view) {
            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('aplicacoes', \App\Models\ProdutoAplicacao::ordenados()->get());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
