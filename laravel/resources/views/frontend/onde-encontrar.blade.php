@extends('frontend.common.template')

@section('content')

    <div class="onde-encontrar">
        <div class="center">
            <h1 class="main-title">{{ t('contato.onde-encontrar') }}</h1>

            <h2>{{ t('contato.nossos-produtos') }}</h2>
            <iframe src="https://munddi.com/Denver?e=1&logo=1" width="100%" height="500px" allow="geolocation *;" style="border:0;"></iframe>

            <h2>{{ t('contato.nossos-revendedores') }}</h2>
            <div class="revendedores">
                <div class="left">
                    <div class="radios">
                        <label>
                            <input type="radio" name="tipo" value="lojas" @if(!request('tipo') || request('tipo') != 'construtoras') checked @endif>
                            <span>{{ t('contato.lojas') }}</span>
                        </label>
                        <label>
                            <input type="radio" name="tipo" value="construtoras" @if(request('tipo') == 'construtoras') checked @endif>
                            <span>{{ t('contato.construtoras') }}</span>
                        </label>
                    </div>

                    <div class="regiao">
                        <p>{{ t('contato.localize-regiao') }}</p>
                        <select name="estado" data-route="{{ route('contato.onde-encontrar.cidades') }}">
                            <option value="">{{ t('contato.estado') }}</option>
                            @foreach($estados as $uf => $nome)
                            <option value="{{ $uf }}" @if(request('estado') == $uf) selected @endif>
                                {{ $nome }}
                            </option>
                            @endforeach
                        </select>
                        <select name="cidade" @if(!request('estado')) disabled @endif data-route="{{ route('contato.onde-encontrar') }}">
                            <option value="">{{ t('contato.cidade') }}</option>
                            @foreach($cidades as $id => $c)
                            <option value="{{ $id }}" @if(request('cidade') == $id) selected @endif>
                                {{ $c }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="right">
                    @if($revendedores)
                    <div class="revendedores-results">
                        @if(count($revendedores))
                            @foreach($revendedores as $r)
                            <div class="revendedor">
                                <div class="info">
                                    <h4>{{ $r->titulo }}</h4>
                                    <p>
                                        {!! $r->descricao !!}
                                        @if($r->e_mail)
                                            @foreach(explode(',', $r->e_mail) as $e)
                                            <br>
                                            <a href="mailto:{{ trim($e) }}">{{ trim($e) }}</a>
                                            @endforeach
                                        @endif
                                        @if($r->site)
                                        <br>
                                        <a href="{{ Tools::parseLink($r->site) }}" target="_blank">{{ $r->site }}</a>
                                        @endif
                                    </p>
                                </div>
                                <div class="buttons">
                                    @if($r->e_mail)
                                        <a href="mailto:{{ str_replace([' ', ','], ['', ';'], $r->e_mail) }}" class="email">{{ $r->e_mail }}</a>
                                    @endif
                                    @if($r->site)
                                        <a href="{{ Tools::parseLink($r->site) }}" target="_blank" class="site">{{ $r->site }}</a>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        @else
                        <div class="nenhum">{{ t('contato.nenhum-revendedor') }}</div>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if(session('scrollToRevendedores'))
    <script>var REVENDEDORES_SCROLL = true;</script>
    @endif

@endsection
