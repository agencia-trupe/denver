<?php

namespace App\Models\Restrito;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    public function scopePerfil($query)
    {
        return $query->where(function($q) {
            foreach(explode(',', auth('restrito')->user()->perfil) as $perfil) {
                $q->orWhere('perfil', 'LIKE', "%$perfil%");
            }
        });
    }

    public function getHasPerfilAttribute()
    {
        return str_contains(
            $this->perfil,
            explode(',', auth('restrito')->user()->perfil)
        );
    }
}
