@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Revendedores / Estados / {{ $estado->nome }} /</small>
            Editar Cidade
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.revendedores.estados.cidades.update', $estado->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.revendedores.cidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
