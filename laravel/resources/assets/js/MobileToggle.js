export default function MobileToggle() {
    const $handle = $('#mobile-toggle');
    const $nav = $('#nav-mobile');

    $handle.on('click touchstart', event => {
        event.preventDefault();
        $nav.slideToggle();
        $handle.toggleClass('close');
    });
}
