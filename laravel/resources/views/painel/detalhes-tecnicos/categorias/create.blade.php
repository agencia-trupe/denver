@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Detalhes Técnicos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.detalhes-tecnicos.categorias.store', 'files' => true]) !!}

        @include('painel.detalhes-tecnicos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
