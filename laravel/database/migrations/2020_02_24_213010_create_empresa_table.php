<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->text('texto_1_pt');
            $table->text('texto_1_en');
            $table->text('texto_1_es');
            $table->text('texto_2_pt');
            $table->text('texto_2_en');
            $table->text('texto_2_es');
            $table->text('texto_3_pt');
            $table->text('texto_3_en');
            $table->text('texto_3_es');
            $table->text('missao_pt');
            $table->text('missao_en');
            $table->text('missao_es');
            $table->text('visao_pt');
            $table->text('visao_en');
            $table->text('visao_es');
            $table->text('valores_pt');
            $table->text('valores_en');
            $table->text('valores_es');
            $table->string('box_titulo_pt');
            $table->string('box_titulo_en');
            $table->string('box_titulo_es');
            $table->text('box_texto_pt');
            $table->text('box_texto_en');
            $table->text('box_texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
