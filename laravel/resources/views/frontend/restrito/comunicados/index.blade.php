@extends('frontend.restrito.template')

@section('main')

    <div class="comunicados">
        @include('frontend.restrito._title')

        <div class="comunicados-grid">
            @foreach($comunicados as $comunicado)
                @if($comunicado->imagem)
                <a href="{{ url('assets/img/comunicados/'.$comunicado->imagem) }}" target="_blank">
                    <img src="{{ asset('assets/img/comunicados/thumbs/'.$comunicado->imagem) }}" alt="">
                @else
                <a href="{{ route('area-restrita.comunicados', $comunicado->id) }}">
                    <img src="{{ asset('assets/img/layout/comunicados-placeholder.png') }}" alt="">
                @endif
                    <div>
                        <span class="data">{{ $comunicado->data }}</span>
                        <span>{{ $comunicado->titulo }}</span>
                    </div>
                </a>
            @endforeach
        </div>
    </div>

@endsection
