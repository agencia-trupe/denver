<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ObrasRequest;
use App\Http\Controllers\Controller;

use App\Models\Obra;

class ObrasController extends Controller
{
    public function index()
    {
        $registros = Obra::ordenados()->get();

        return view('painel.obras.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.obras.create');
    }

    public function store(ObrasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Obra::upload_capa();

            Obra::create($input);

            return redirect()->route('painel.obras.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Obra $registro)
    {
        return view('painel.obras.edit', compact('registro'));
    }

    public function update(ObrasRequest $request, Obra $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Obra::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.obras.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Obra $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.obras.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
