@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Detalhes Técnicos /</small> Adicionar Detalhe Técnico</h2>
    </legend>

    {!! Form::open(['route' => 'painel.detalhes-tecnicos.store', 'files' => true]) !!}

        @include('painel.detalhes-tecnicos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
