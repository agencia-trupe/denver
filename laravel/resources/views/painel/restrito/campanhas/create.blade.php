@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Campanhas /</small> Adicionar Campanha</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.campanhas.store', 'files' => true]) !!}

        @include('painel.restrito.campanhas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
