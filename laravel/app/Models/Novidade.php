<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;
use Carbon\Carbon;

class Novidade extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'novidades';

    protected $guarded = ['id'];

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('novidades_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\NovidadeCategoria', 'novidades_categoria_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopePublicados($query)
    {
        return $query->where('publicado', '=', 1);
    }

    public function scopeBusca($query, $value)
    {
        $locale = app()->getLocale();
        $fields = [
            'titulo_'.$locale,
            'texto_'.$locale,
        ];

        return $query->publicados()->where(function($q) use ($fields, $value) {
            foreach($fields as $field) {
                $q->orWhere($field, 'LIKE', "%$value%");
            }
        })->orderBy('titulo_'.$locale);
    }

    public function getBuscaAttribute()
    {
        $obj = new \stdClass();

        $obj->route = route('conteudo-tecnico.novidades.show', [
            $this->categoria->slug, $this->slug
        ]);
        $obj->label = tobj($this, 'titulo');

        return $obj;
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 600,
                'height' => 340,
                'path'   => 'assets/img/novidades/thumbs/'
            ],
            [
                'width'  => 800,
                'height' => 455,
                'path'   => 'assets/img/novidades/'
            ],
        ]);
    }
}
