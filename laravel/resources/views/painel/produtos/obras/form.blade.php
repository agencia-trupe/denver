@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('obra_id', 'Obra') !!}
    {!! Form::select('obra_id', $obras, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.obras.index', $produto) }}" class="btn btn-default btn-voltar">Voltar</a>
