@extends('frontend.common.template')

@section('content')

    <div class="obras">
        <div class="center">
            <h1 class="main-title">SOPREMA EM GRANDES OBRAS</h1>

            <div class="grid-index">
                @foreach($obras as $obra)
                <a href="{{ route('obras', $obra->slug) }}">
                    <img src="{{ asset('assets/img/obras/'.$obra->capa) }}" alt="">
                    <span>{{ tobj($obra, 'titulo') }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
