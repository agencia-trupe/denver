<?php

namespace App\Models\Restrito;

use App\Helpers\CropImage;

class Download extends Model
{
    protected $table = 'restrito_downloads';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('downloads_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo(DownloadCategoria::class, 'downloads_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'       => 340,
            'height'      => 360,
            'transparent' => true,
            'path'        => 'assets/img/downloads-restritos/'
        ]);
    }

    public function getFilePathAttribute()
    {
        return storage_path('app/restrito/downloads/'.$this->arquivo);
    }
}
