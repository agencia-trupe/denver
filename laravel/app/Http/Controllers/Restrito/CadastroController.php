<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Http\Requests\Restrito\CadastrosRequest;

class CadastroController extends Controller
{
    public function index()
    {
        $cadastro = auth('restrito')->user();

        return view('frontend.restrito.cadastro.index', compact('cadastro'));
    }

    public function post(CadastrosRequest $request)
    {
        try {

            $input = $request->except(['perfil', 'password_confirmation']);

            if (isset($input['password']) && $input['password'] != '') {
                $input['password'] = bcrypt($input['password']);
            } else {
                unset($input['password']);
            }

            auth('restrito')->user()->update($input);

            return redirect()->route('area-restrita.cadastro')
                ->with('success', 'Cadastro atualizado com sucesso!');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }
}
