<?php

namespace App\Models\Restrito;

use App\Helpers\CropImage;

class Treinamento extends Model
{
    protected $table = 'restrito_treinamentos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 380,
            'height' => 180,
            'path'   => 'assets/img/treinamentos-restritos/'
        ]);
    }

    public function getVideoUrlAttribute()
    {
        switch($this->video_tipo) {
            case 'youtube':
                return "https://youtube.com/embed/$this->video";
            case 'vimeo':
                return "https://player.vimeo.com/video/$this->video";
            default:
                return null;
        }
    }
}
