@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="videos">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.videos') }}
        </h1>

        <div class="thumbs">
            @foreach($videos as $video)
            <a href="{{ $video->video_url }}" class="handle-video">
                <div class="imagem">
                    <img src="{{ asset('assets/img/videos/'.$video->capa) }}" alt="">
                </div>
                <span>{{ tobj($video, 'titulo') }}</span>
            </a>
            @endforeach
        </div>

        {!! $videos->render() !!}
    </div>

@endsection
