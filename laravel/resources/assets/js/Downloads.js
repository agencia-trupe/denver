export default function() {
    $('.handle-downloads-lead').fancybox({
        type: 'ajax',
        maxWidth: 580,
        maxHeight: '70%',
        padding: 0,
        afterShow() {
            $('input[name=telefone]').inputmask({
                mask: ['(99) 9999-9999', '(99) 99999-9999'],
                jitMasking: true,
            });
        },
    });
}
