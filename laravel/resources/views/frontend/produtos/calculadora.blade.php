@extends('frontend.common.template')

@section('content')

    <div class="calculadora-pagina">
        <div class="center">
            <h1 class="main-title">
                <img src="{{ asset('assets/img/layout/submenu-produtos-calculadora.svg') }}" alt="">
                {{ t('nav.produtos-calculadora') }}
            </h1>

            <div class="produtos-breadcrumb">
                <span>{{ t('produtos.produto') }}</span>
                <div class="arrow"></div>
                <select name="produto" class="handle-select-route">
                    <option value="" data-route="{{ route('produtos.calculadora') }}">
                        {{ t('produtos.selecionar-produto') }}
                    </option>
                    @foreach($produtos as $p)
                    <option
                        value="{{ $p->id }}"
                        data-route="{{ route('produtos.calculadora', $p->slug) }}"
                        @if($produto && $p->id === $produto->id) selected @endif
                    >
                        {{ tobj($p, 'titulo') }}
                    </option>
                    @endforeach
                </select>
            </div>

            <p class="chamada">{{ t('produtos.calculadora.denver-ajuda') }}</p>

            @if($produto)
            <form
                class="calculadora-form"
                data-ratio="{{ $produto->unidades_por_medida }}"
                data-result="{{ t('produtos.calculadora.resultado') }}"
                data-unit="{{ $produto->medida }}"
                data-product="{{ tobj($produto, 'titulo') }}"
            >
                <span>{{ t('produtos.calculadora.quantidade', ['medida' => $produto->medida]) }}</span>
                <input type="number" step="0.1" min="0.1" max="999999" name="quantidade" required>
                <button type="submit">{{ t('produtos.calculadora.calcular') }}</button>
            </form>
            <div class="calculadora-resultado"></div>
            @endif
        </div>
    </div>

@endsection
