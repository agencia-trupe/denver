@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home / Soluções /</small> Adicionar Solução</h2>
    </legend>

    {!! Form::open(['route' => 'painel.solucoes.store', 'files' => true]) !!}

        @include('painel.solucoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
