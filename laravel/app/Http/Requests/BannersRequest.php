<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem_pt' => 'required|image',
            'imagem_en' => 'image',
            'imagem_es' => 'image',
            'link' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_pt'] = 'image';
            $rules['imagem_en'] = 'image';
            $rules['imagem_es'] = 'image';
        }

        return $rules;
    }
}
