<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Models\Restrito\Download;
use App\Models\Restrito\DownloadCategoria;

class DownloadsController extends Controller
{
    public function index(DownloadCategoria $categoria)
    {
        if (!$categoria->exists) {
            $categoria = DownloadCategoria::ordenados()->firstOrFail();
        }

        $categorias = DownloadCategoria::ordenados()->get();
        $downloads  = $categoria->downloads()->perfil()->paginate(10);

        return view('frontend.restrito.downloads.index', compact(
            'categorias',
            'categoria',
            'downloads'
        ));
    }

    public function download($id)
    {
        $download = Download::findOrFail($id);

        return $download->hasPerfil
            ? response()->download($download->filePath)
            : abort('404');
    }
}
