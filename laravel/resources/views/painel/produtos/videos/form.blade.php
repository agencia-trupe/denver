@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/videos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video_tipo', 'Vídeo (Tipo)') !!}
    {!! Form::select('video_tipo', [
        'youtube' => 'YouTube',
        'vimeo'   => 'Vimeo',
    ], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('video', 'Vídeo (código)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.videos.index', $produto) }}" class="btn btn-default btn-voltar">Voltar</a>
