<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests\Restrito\CampanhasRequest;
use App\Http\Controllers\Controller;

use App\Models\Restrito\Campanha;

use App\Helpers\Tools;

class CampanhasController extends Controller
{
    public function index()
    {
        $registros = Campanha::ordenados()->get();

        return view('painel.restrito.campanhas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.restrito.campanhas.create');
    }

    public function store(CampanhasRequest $request)
    {
        try {

            $input = $request->all();

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['capa'])) $input['capa'] = Campanha::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileStorageUpload('arquivo', 'restrito/campanhas/');
            }

            Campanha::create($input);

            return redirect()->route('painel.restrito.campanhas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Campanha $registro)
    {
        return view('painel.restrito.campanhas.edit', compact('registro'));
    }

    public function update(CampanhasRequest $request, Campanha $registro)
    {
        try {

            $input = $request->all();

            $input['perfil'] = implode(',', $request->get('perfil'));
            if (isset($input['capa'])) $input['capa'] = Campanha::upload_capa();
            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileStorageUpload('arquivo', 'restrito/campanhas/');
            }

            $registro->update($input);

            return redirect()->route('painel.restrito.campanhas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Campanha $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.restrito.campanhas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
