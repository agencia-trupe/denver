<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PopupRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'video_tipo' => 'required_with:video|in:youtube,vimeo',
            'video' => 'required_with:video_tipo',
            'ativo' => '',
            'data_de_expiracao' => '',
        ];
    }
}
