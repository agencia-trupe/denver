<?php

use Illuminate\Database\Seeder;

class PopupSeeder extends Seeder
{
    public function run()
    {
        DB::table('popup')->insert([
            'imagem' => '',
            'video' => '',
            'video_tipo' => '',
            'ativo' => '',
            'data_de_expiracao' => '',
        ]);
    }
}
