@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Downloads /</small> Editar Download</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.downloads.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.downloads.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
