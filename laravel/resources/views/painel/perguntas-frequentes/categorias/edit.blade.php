@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Perguntas Frequentes /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.perguntas-frequentes.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.perguntas-frequentes.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
