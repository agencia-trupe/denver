@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Institucional / Obras /</small> Editar Obra</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.obras.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.obras.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
