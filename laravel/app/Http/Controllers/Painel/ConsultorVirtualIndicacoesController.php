<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConsultorVirtualIndicacoesRequest;

use App\Models\Consultoria;
use App\Models\ConsultoriaIndicacao;
use App\Models\Produto;

class ConsultorVirtualIndicacoesController extends Controller
{
    public function index(Consultoria $consultoria)
    {
        $registros = $consultoria->indicacoes;

        return view('painel.consultor-virtual.indicacoes.index', compact('consultoria', 'registros'));
    }

    public function create(Consultoria $consultoria)
    {
        $produtos = Produto::orderBy('titulo_pt', 'ASC')->lists('titulo_pt', 'id');

        return view('painel.consultor-virtual.indicacoes.create', compact('consultoria', 'produtos'));
    }

    public function store(ConsultorVirtualIndicacoesRequest $request, Consultoria $consultoria)
    {
        try {

            $input    = $request->except('produtos');
            $produtos = ($request->get('produtos') ?: []);

            $registro = $consultoria->indicacoes()->create($input);
            $registro->produtos()->sync($produtos);

            return redirect()->route('painel.consultor-virtual.indicacoes.index', $consultoria->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Consultoria $consultoria, ConsultoriaIndicacao $registro)
    {
        $produtos = Produto::orderBy('titulo_pt', 'ASC')->lists('titulo_pt', 'id');

        return view('painel.consultor-virtual.indicacoes.edit', compact('consultoria', 'registro', 'produtos'));
    }

    public function update(ConsultorVirtualIndicacoesRequest $request, Consultoria $consultoria, ConsultoriaIndicacao $registro)
    {
        try {

            $input = $request->except('produtos');

            $registro->update($input);

            $produtos = ($request->get('produtos') ?: []);
            $registro->produtos()->sync($produtos);

            return redirect()->route('painel.consultor-virtual.indicacoes.index', $consultoria->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Consultoria $consultoria, ConsultoriaIndicacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.consultor-virtual.indicacoes.index', $consultoria->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
