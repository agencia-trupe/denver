    <nav>
        <a href="{{ route('home') }}" class="logo">
            <img src="{{ asset('assets/img/layout/marca-soprema-menu.svg') }}" alt="Denver" class="img-logo">
        </a>

        <div class="links">
            <a href="{{ route('empresa') }}" @if(Tools::routeIs('empresa')) class="active" @endif>
                {{ t('nav.empresa') }}
            </a>
            <div class="nav-produtos-wrapper">
                <span @if(Tools::routeIs('produtos*')) class="active" @endif>{{ t('nav.produtos') }}</span>
                <div class="nav-produtos-sub-1">
                    <a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos') && !request()->aplicacao_slug) class="active" @endif>
                        {!! file_get_contents(resource_path('assets/img/submenu-produtos-todos.svg')) !!}
                        <span>{{ t('nav.produtos-todos') }}</span>
                    </a>
                    <div class="nav-produtos-aplicacoes @if(Tools::routeIs('produtos') && request()->aplicacao_slug) active @endif">
                        {!! file_get_contents(resource_path('assets/img/submenu-produtos-aplicacoes.svg')) !!}
                        <span>{{ t('nav.produtos-aplicacoes') }}</span>
                        <div class="nav-produtos-sub-2">
                            @foreach($aplicacoes as $aplicacao)
                                <a href="{{ route('produtos', $aplicacao->slug) }}" class="sub @if(request()->aplicacao_slug && request()->aplicacao_slug->slug === $aplicacao->slug) active @endif">
                                    {{ tobj($aplicacao, 'titulo') }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                    {{-- <a href="{{ route('produtos.calculadora') }}" @if(Tools::routeIs('produtos.calculadora')) class="active" @endif>
                        {!! file_get_contents(resource_path('assets/img/submenu-produtos-calculadora.svg')) !!}
                        <span>{{ t('nav.produtos-calculadora') }}</span>
                    </a> --}}
                </div>
            </div>
            <a href="{{ route('detalhes-tecnicos') }}" @if(Tools::routeIs('detalhes-tecnicos')) class="active" @endif>
                {{ t('nav.detalhes-tecnicos') }}
            </a>
            <a href="{{ route('obras') }}" @if(Tools::routeIs('obras')) class="active" @endif>
                {{ t('nav.obras') }}
            </a>
            <a href="{{ route('conteudo-tecnico') }}" @if(Tools::routeIs('conteudo-tecnico*')) class="active" @endif>{{ t('nav.conteudo-tecnico') }}</a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="active" @endif>
                {{ t('nav.contato') }}
            </a>
        </div>

        <a href="#" class="busca">
            {!! file_get_contents(resource_path('assets/img/lupa-busca.svg')) !!}
            <span>{{ t('template.busca') }}</span>
        </a>

        {{-- <div class="social">
            @foreach(['facebook', 'instagram', 'youtube', 'linkedin'] as $s)
                @if($contato->{$s})
                    <a href="{{ Tools::parseLink($contato->{$s}) }}" target="_blank" class="{{ $s }}">
                        {!! file_get_contents(resource_path('assets/img/redessociais-'.$s.'.svg')) !!}
                    </a>
                @endif
            @endforeach
        </div> --}}

        <div class="social">   
            @if($contato->facebook)
                <a href="{{$contato->facebook}}">
                    <div class="img_socials"></div>
                </a>
            @endif  
            @if($contato->instagram)
                <a href="{{$contato->instagram}}">
                    <div class="img_socials2"></div>
                </a>
            @endif  
            @if($contato->youtube)
            <a href="{{$contato->youtube}}">
                <div class="img_socials3"></div>
            </a>
            @endif   
            @if($contato->linkedin)
            <a href="{{$contato->linkedin}}">
                <div class="img_socials4"></div>
            </a>
            @endif
        </div>

        <div class="resizer"><img src="{{ asset('assets/img/layout/marca-denver-menu.svg') }}" alt="Denver" class="denver_novo_logo"></div>
    </nav>
