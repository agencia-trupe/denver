@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Tabelas de Preço /</small> Adicionar Tabela</h2>
    </legend>

    {!! Form::open(['route' => 'painel.restrito.tabelas.store', 'files' => true]) !!}

        @include('painel.restrito.tabelas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
