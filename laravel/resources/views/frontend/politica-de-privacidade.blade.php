@extends('frontend.common.template')

@section('content')

    <div class="politica-termos">
        <div class="center">
            <h1 class="main-title">{{ t('nav.politica') }}</h1>

            {!! tobj($politicaDePrivacidade, 'texto') !!}
        </div>
    </div>

@endsection
