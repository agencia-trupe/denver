<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosEmbalagensRequest;

use App\Http\Controllers\Controller;
use App\Models\ProdutoEmbalagem;
use App\Models\Produto;

class ProdutosEmbalagensController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->embalagens;

        return view('painel.produtos.embalagens.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto)
    {
        return view('painel.produtos.embalagens.create', compact('produto'));
    }

    public function store(ProdutosEmbalagensRequest $request, Produto $produto)
    {
        try {

            $input = $request->all();

            $produto->embalagens()->create($input);

            return redirect()->route('painel.produtos.embalagens.index', $produto)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $produto, ProdutoEmbalagem $registro)
    {
        return view('painel.produtos.embalagens.edit', compact('produto', 'registro'));
    }

    public function update(ProdutosEmbalagensRequest $request, Produto $produto, ProdutoEmbalagem $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.produtos.embalagens.index', $produto)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $produto, ProdutoEmbalagem $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.embalagens.index', $produto)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
