<?php

namespace App\Http\Controllers;

use App\Models\DetalheTecnicoCategoria;

class DetalhesTecnicosController extends Controller
{
    public function index(DetalheTecnicoCategoria $categoria)
    {
        if ($categoria->exists) {
            return view('frontend.detalhes-tecnicos.show', compact('categoria'));
        }

        return view('frontend.detalhes-tecnicos.index', [
            'categorias' => DetalheTecnicoCategoria::ordenados()->get()
        ]);
    }
}
