@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Downloads /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.restrito.downloads.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.restrito.downloads.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
