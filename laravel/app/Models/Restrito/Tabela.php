<?php

namespace App\Models\Restrito;

use Carbon\Carbon;

class Tabela extends Model
{
    protected $table = 'restrito_tabelas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getFilePathAttribute()
    {
        return storage_path('app/restrito/tabelas/'.$this->arquivo);
    }
}
