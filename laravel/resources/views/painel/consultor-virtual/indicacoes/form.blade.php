@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('produtos', 'Produtos') !!}
    {!! Form::select('produtos[]', $produtos, isset($registro) ? $registro->produtos->pluck('id')->toArray() : null, ['class' => 'form-control multi-select', 'multiple' => true]) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.consultor-virtual.indicacoes.index', $consultoria) }}" class="btn btn-default btn-voltar">Voltar</a>
