<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevendedoresTable extends Migration
{
    public function up()
    {
        Schema::create('revendedores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('tipo');
            $table->text('descricao');
            $table->string('e_mail');
            $table->string('site');
            $table->timestamps();
        });

        Schema::create('revendedores_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('uf');
            $table->timestamps();
        });

        Schema::create('revendedores_cidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('revendedores_estados')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('revendedor_cidade', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('revendedor_id')->unsigned();
            $table->foreign('revendedor_id')->references('id')->on('revendedores')->onDelete('cascade');
            $table->integer('cidade_id')->unsigned();
            $table->foreign('cidade_id')->references('id')->on('revendedores_cidades')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('revendedor_cidade');
        Schema::drop('revendedores_cidades');
        Schema::drop('revendedores_estados');
        Schema::drop('revendedores');
    }
}
