<?php

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
            'texto_1_pt' => '',
            'texto_1_en' => '',
            'texto_1_es' => '',
            'texto_2_pt' => '',
            'texto_2_en' => '',
            'texto_2_es' => '',
            'texto_3_pt' => '',
            'texto_3_en' => '',
            'texto_3_es' => '',
            'missao_pt' => '',
            'missao_en' => '',
            'missao_es' => '',
            'visao_pt' => '',
            'visao_en' => '',
            'visao_es' => '',
            'valores_pt' => '',
            'valores_en' => '',
            'valores_es' => '',
            'box_titulo_pt' => '',
            'box_titulo_en' => '',
            'box_titulo_es' => '',
            'box_texto_pt' => '',
            'box_texto_en' => '',
            'box_texto_es' => '',
        ]);
    }
}
