@extends('frontend.common.template')

@section('content')

    <div class="produtos">
        <div class="center">
            <div class="produtos-breadcrumb">
                <span>{{ t('produtos.produtos') }}</span>
                <div class="arrow"></div>
                <select name="produto" class="handle-select-route">
                    @if(request('busca') && !$aplicacao)
                    <option value="busca" data-route="{{ route('produtos') }}">
                        {{ t('produtos.buscando', ['termo' => request('busca')]) }}
                    </option>
                    @endif
                    <option value="0" data-route="{{ route('produtos') }}">
                        {{ t('produtos.todos') }}
                    </option>
                    @foreach($aplicacoes as $a)
                    <option
                        value="{{ $a->id }}"
                        data-route="{{ route('produtos', $a->slug) }}"
                        @if($aplicacao && $a->id === $aplicacao->id) selected @endif
                    >
                        {{ tobj($a, 'titulo') }}
                    </option>
                    @endforeach
                </select>
            </div>

            @if(count($produtos))
                <div class="grid">
                    @foreach($produtos as $produto)
                    <a href="{{ route('produtos.show', array_merge(
                        [$produto->slug],
                        $aplicacao ? ['aplicacao' => $aplicacao->slug] : []
                    )) }}">
                        <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                        <span>{{ tobj($produto, 'titulo') }}</span>
                    </a>
                    @endforeach
                </div>
            @else
                <div class="nenhum">{{ t('produtos.nenhum') }}</div>
            @endif
        </div>
    </div>

@endsection
