<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DetalhesTecnicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'detalhes_tecnicos_categoria_id' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => '',
            'titulo_es' => '',
            'arquivo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = '';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'detalhes_tecnicos_categoria_id' => 'categoria',
        ];
    }
}
