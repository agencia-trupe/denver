<?php

namespace App\Http\Controllers\Painel\Restrito;

use App\Http\Controllers\Controller;

use App\Models\Restrito\Curriculo;

class CurriculosController extends Controller
{
    public function index()
    {
        $registros = Curriculo::orderBy('created_at', 'DESC')->paginate(20);

        return view('painel.restrito.curriculos.index', compact('registros'));
    }

    public function show($arquivo)
    {
        $filePath = storage_path('app/curriculos/'.$arquivo);

        if (file_exists($filePath)) {
            return response()->file($filePath);
        }

        return response('Arquivo não encontrado.', 404);
    }

    public function destroy(Curriculo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.restrito.curriculos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
