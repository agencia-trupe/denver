@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Área Restrita /</small>
            Cadastros
            <a href="{{ route('painel.restrito.cadastros.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Cadastro</a>
        </h2>
    </legend>

    <p class="alert alert-info">
        <span class="glyphicon glyphicon-link" style="margin-right:10px;"></span>
        Link público para cadastro:
        <a href="{{ route('restrito.register', ['token' => env('TOKEN_CADASTRO_RESTRITO')]) }}" class="alert-link" target="_blank">
            {{ route('restrito.register', ['token' => env('TOKEN_CADASTRO_RESTRITO')]) }}
        </a>
    </p>

    <div class="btn-group">
        <a href="{{ route('painel.restrito.cadastros.index', ['filtro' => null]) }}" class="btn btn-warning @if(request('filtro') == '' || !request('filtro')) active @endif">
            Aguardando aprovação
            @if($countAprovacao >= 1)
            <div class="label label-warning" style="margin-left:4px">{{ $countAprovacao }}</div>
            @endif
        </a>
        <a href="{{ route('painel.restrito.cadastros.index', ['filtro' => 'aprovados']) }}" class="btn btn-success @if(request('filtro') == 'aprovados') active @endif">Aprovados</a>
    </div>

    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Perfil</th>
                <th>Nome</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    @foreach(explode(',', $registro->perfil) as $perfil)
                    <small style="display:block">{{ Tools::restritoPerfis()[$perfil] }}</small>
                    @endforeach
                </td>
                <td>{{ $registro->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.restrito.cadastros.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.restrito.cadastros.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
