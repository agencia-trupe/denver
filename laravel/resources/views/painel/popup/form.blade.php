@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <p class="alert alert-info small" style="margin-bottom: 15px; padding: 12px 15px; display:flex; align-items: center;">
            <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
            Preencha um dos campos abaixo.<br>Se os dois estiverem preenchidos a imagem será exibida.
        </p>

        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            @if($registro->imagem)
            <img src="{{ url('assets/img/popup/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('video_tipo', 'Vídeo (Tipo)') !!}
            {!! Form::select('video_tipo', [
                'youtube' => 'YouTube',
                'vimeo'   => 'Vimeo',
            ], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('video', 'Vídeo') !!}
            {!! Form::text('video', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well">
            <div class="checkbox" style="margin:0">
                <label style="font-weight:bold">
                    {!! Form::hidden('ativo', 0) !!}
                    {!! Form::checkbox('ativo') !!}
                    Ativo
                </label>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('data_de_expiracao', 'Data de Expiração') !!}
            {!! Form::text('data_de_expiracao', null, ['class' => 'form-control datepicker', 'data-no-default' => true]) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
