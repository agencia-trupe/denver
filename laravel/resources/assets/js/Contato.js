export default function() {
    function contatoSubmit(event, callback) {
        event.preventDefault();

        const $form = $(this);

        if ($form.hasClass('sending')) return false;

        $form.addClass('sending');

        return $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: new FormData(this),
            processData: false,
            contentType: false,
        })
            .done(data => {
                alert(data.message);

                $form[0].reset();
                $form.find('.file-input input').trigger('change');
                if (callback) callback();
            })
            .fail(data => {
                const { responseJSON: res } = data;

                let txt = '';

                Object.keys(res).map(key =>
                    res[key].forEach(msg => {
                        txt += `${msg}\n`;
                    })
                );

                alert(txt);
            })
            .always(() => $form.removeClass('sending'));
    }

    $('#form-contato').submit(contatoSubmit);
    $('#form-curriculo').submit(contatoSubmit);
    $('#form-representante').submit(contatoSubmit);
    $('#form-interesse').submit(contatoSubmit);
    $(document).on('submit', '#form-download-lead', function submit(event) {
        contatoSubmit.call(this, event, () => $.fancybox.close());
    });
}
