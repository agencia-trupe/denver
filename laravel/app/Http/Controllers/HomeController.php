<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewsletterRequest;
use App\Models\AceiteDeCookies;
use App\Models\Background;
use App\Models\Banner;
use App\Models\Solucao;
use App\Models\Obra;
use App\Models\Popup;
use App\Models\Newsletter;
use App\Models\Consultoria;
use App\Models\DetalheTecnico;
use App\Models\Download;
use App\Models\Evento;
use App\Models\Novidade;
use App\Models\PerguntaFrequente;
use App\Models\Produto;
use App\Models\ProdutoAplicacao;
use App\Models\Treinamento;
use App\Models\Video;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home', [
            'bg'         => Background::first(),
            'banners'    => Banner::ordenados()->get(),
            'problemas'  => Consultoria::whereTipo('problema')->ordenados()->get(),
            'aplicacoes' => ProdutoAplicacao::ordenados()->get(),
            'solucoes'   => Solucao::ordenados()->get(),
            'obras'      => Obra::where('destaque_home', true)->ordenados()->get(),
            'novidades'  => Novidade::publicados()->where('destaque_home', true)->ordenados()->limit(2)->get(),
            'popup'      => Popup::first(),
        ]);
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        return response()->json([
            'message' => t('newsletter.sucesso')
        ]);
    }

    public function busca(Request $request)
    {
        $termo = $request->q;

        if (!$termo) {
            return redirect()->route('home');
        }

        $resultados = [
            'produtos'          => Produto::busca($termo)->get(),
            'obras'             => Obra::busca($termo)->get(),
            'novidades'         => Novidade::busca($termo)->get(),
            'eventos'           => Evento::busca($termo)->get(),
            'treinamentos'      => Treinamento::busca($termo)->get(),
            'videos'            => Video::busca($termo)->get(),
            'downloads'         => Download::busca($termo)->get(),
            'detalhes-tecnicos' => DetalheTecnico::busca($termo)->get(),
            'faq'               => PerguntaFrequente::busca($termo)->get(),
        ];

        $total = array_sum(array_map('count', $resultados));

        return view('frontend.busca', compact('resultados', 'total'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
