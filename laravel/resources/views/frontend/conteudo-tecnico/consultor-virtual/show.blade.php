@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="consultor-virtual">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.consultor') }}
        </h1>

        <div class="consultoria">
            <img src="{{ asset('assets/img/layout/img-consultorvirtual-'.$tipo.'.jpg') }}" alt="">
            <div>
                <h2>{!! t('conteudo-tecnico.'.$tipo) !!}</h2>

                <select class="handle-select-route">
                    <option value="null" data-route="{{ route('conteudo-tecnico.consultor-virtual', $tipo) }}">{{ t('home.'.($tipo === 'problema' ? 'local' : 'tipo')) }}</option>
                    @foreach($consultorias as $c)
                        <option value="{{ $c->id }}" data-route="{{ route('conteudo-tecnico.consultor-virtual', [$tipo, $c->id]) }}" @if($consultoria && $consultoria->id == $c->id) selected @endif>{{ tobj($c, 'titulo') }}</option>
                    @endforeach
                </select>

                @if($consultoria && count($consultoria->indicacoes))
                <div class="options">
                    @if(count($consultoria->indicacoes) === 1)
                        @foreach($consultoria->indicacoes as $i)
                        <span>{{ tobj($i, 'titulo') }}</span>
                        @endforeach
                    @else
                        @foreach($consultoria->indicacoes as $i)
                        <a href="{{ route('conteudo-tecnico.consultor-virtual', [$tipo, $consultoria->id, $i->id]) }}" @if($indicacao && $indicacao->id == $i->id) class="active" @endif>
                            {{ tobj($i, 'titulo') }}
                        </a>
                        @endforeach
                    @endif
                </div>
                @endif

                @if($indicacao)
                <div class="indicado">
                    <span>{{ t('conteudo-tecnico.indicado') }} <i></i></span>

                    @foreach($indicacao->produtos as $i)
                    <a href="{{ route('produtos.show', $i->slug) }}">
                        <p>{{ tobj($i, 'titulo') }}</p>
                        <img src="{{ asset('assets/img/produtos/'.$i->imagem) }}" alt="">
                        <span>
                            {{ t('conteudo-tecnico.ver-mais') }}
                            <i></i>
                        </span>
                    </a>
                    @endforeach
                </div>
                @endif
            </div>

        </div>
    </div>

@endsection
