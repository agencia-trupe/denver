<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadLead extends Model
{
    protected $table = 'downloads_leads';

    protected $guarded = ['id'];

    public function download()
    {
        return $this->belongsTo(Download::class, 'download_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('created_at', 'DESC')->orderBy('id', 'DESC');
    }

    public static function segmentos()
    {
        return [
            'Construtora',
            'Aplicador',
            'Home center',
            'Lojista',
            'Cliente final'
        ];
    }
}
