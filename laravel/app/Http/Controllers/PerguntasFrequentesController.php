<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\PerguntaFrequenteCategoria;

class PerguntasFrequentesController extends Controller
{
    public function index(PerguntaFrequenteCategoria $categoria)
    {
        if (!$categoria->exists) {
            $categoria = PerguntaFrequenteCategoria::ordenados()->firstOrFail();
        }

        $categorias = PerguntaFrequenteCategoria::ordenados()->get();
        $perguntas  = $categoria->perguntas_frequentes;

        return view('frontend.conteudo-tecnico.perguntas-frequentes.index', compact(
            'categorias',
            'categoria',
            'perguntas'
        ));
    }
}
