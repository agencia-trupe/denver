<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests\Restrito\DownloadsCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\Restrito\DownloadCategoria;

class DownloadsCategoriasController extends Controller
{
    public function index()
    {
        $categorias = DownloadCategoria::ordenados()->get();

        return view('painel.restrito.downloads.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.restrito.downloads.categorias.create');
    }

    public function store(DownloadsCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            DownloadCategoria::create($input);
            return redirect()->route('painel.restrito.downloads.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(DownloadCategoria $categoria)
    {
        return view('painel.restrito.downloads.categorias.edit', compact('categoria'));
    }

    public function update(DownloadsCategoriasRequest $request, DownloadCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.restrito.downloads.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(DownloadCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.restrito.downloads.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
