<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use App\Models\RepresentanteRecebido;

class RepresentantesRecebidosController extends Controller
{
    public function index()
    {
        $registros = RepresentanteRecebido::orderBy('created_at', 'DESC')->paginate(20);

        return view('painel.contato.representantes.index', compact('registros'));
    }

    public function destroy(RepresentanteRecebido $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.contato.representantes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
