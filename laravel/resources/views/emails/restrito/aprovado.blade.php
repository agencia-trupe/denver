<!DOCTYPE html>
<html>
<head>
    <title>Denver · Cadastro Aprovado</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-size:16px;line-height:1.5;font-family:Verdana, sans-serif;'>
        Olá {{ $nome }},
        <br>
        Seu cadastro foi aprovado com sucesso. Acesse o link abaixo para fazer login.
        <br><br>
        <a href="{{ route('restrito.login') }}">Área Restrita</a>
    </span>
</body>
</html>
