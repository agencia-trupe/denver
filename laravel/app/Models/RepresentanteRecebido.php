<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepresentanteRecebido extends Model
{
    protected $table = 'representantes_recebidos';

    protected $guarded = ['id'];
}
