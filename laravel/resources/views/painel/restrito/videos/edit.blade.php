@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Vídeos /</small> Editar Vídeo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.videos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.videos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
