<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DetalhesTecnicosRequest;
use App\Http\Controllers\Controller;

use App\Models\DetalheTecnico;
use App\Models\DetalheTecnicoCategoria;

use App\Helpers\Tools;

class DetalhesTecnicosController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = DetalheTecnicoCategoria::ordenados()->lists('titulo_pt', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (DetalheTecnicoCategoria::find($filtro)) {
            $registros = DetalheTecnico::where('detalhes_tecnicos_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = DetalheTecnico::leftJoin('detalhes_tecnicos_categorias as cat', 'cat.id', '=', 'detalhes_tecnicos_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('detalhes_tecnicos.*')
                ->ordenados()->get();
        }

        return view('painel.detalhes-tecnicos.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.detalhes-tecnicos.create', compact('categorias'));
    }

    public function store(DetalhesTecnicosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'arquivos/detalhes-tecnicos/');
            }

            DetalheTecnico::create($input);

            return redirect()->route('painel.detalhes-tecnicos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(DetalheTecnico $registro)
    {
        $categorias = $this->categorias;

        return view('painel.detalhes-tecnicos.edit', compact('registro', 'categorias'));
    }

    public function update(DetalhesTecnicosRequest $request, DetalheTecnico $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = Tools::fileUpload('arquivo', 'arquivos/detalhes-tecnicos');
            }

            $registro->update($input);

            return redirect()->route('painel.detalhes-tecnicos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(DetalheTecnico $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.detalhes-tecnicos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
