@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Área Restrita / Tabelas de Preço /</small> Editar Tabela</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.restrito.tabelas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.restrito.tabelas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
