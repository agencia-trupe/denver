<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosEmbalagensRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo_pt' => 'required',
            'sku' => 'required',
        ];
    }
}
