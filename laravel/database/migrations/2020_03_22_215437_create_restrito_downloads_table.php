<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestritoDownloadsTable extends Migration
{
    public function up()
    {
        Schema::create('restrito_downloads_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('restrito_downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('downloads_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('perfil');
            $table->string('titulo');
            $table->string('capa');
            $table->string('arquivo');
            $table->foreign('downloads_categoria_id')->references('id')->on('restrito_downloads_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('restrito_downloads');
        Schema::drop('restrito_downloads_categorias');
    }
}
