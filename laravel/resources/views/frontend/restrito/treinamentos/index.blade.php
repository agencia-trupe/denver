@extends('frontend.restrito.template')

@section('main')

    <div class="videos">
        @include('frontend.restrito._title')

        <div class="videos-grid">
            @foreach($treinamentos as $video)
            <div>
                <a href="{{ route('area-restrita.treinamentos', $video->id) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/treinamentos-restritos/'.$video->capa) }}" alt="">
                    </div>
                    <span>{{ $video->titulo }}</span>
                </a>
            </div>
            @endforeach
        </div>
    </div>

@endsection
