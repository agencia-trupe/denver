<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Consultoria;
use App\Models\ConsultoriaIndicacao;

class ConsultorVirtualController extends Controller
{
    public function index($tipo = null, $consultoria = null, $indicacao = null)
    {
        if (!$tipo) {
            return view('frontend.conteudo-tecnico.consultor-virtual.index');
        }

        if (!in_array($tipo, ['solucao', 'problema'])) {
            abort('404');
        }

        $consultorias = Consultoria::whereTipo($tipo)->ordenados()->get();

        if ($consultoria) {
            $consultoria = Consultoria::whereTipo($tipo)->findOrFail($consultoria);

            if ($indicacao) {
                $indicacao = ConsultoriaIndicacao::where('consultoria_id', $consultoria->id)
                    ->findOrFail($indicacao);
            } else if (count($consultoria->indicacoes) === 1) {
                $indicacao = $consultoria->indicacoes->first();
            }
        }

        return view('frontend.conteudo-tecnico.consultor-virtual.show', compact(
            'tipo',
            'consultorias',
            'consultoria',
            'indicacao'
        ));
    }
}
