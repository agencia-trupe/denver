<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\CidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\RevendedorCidade;
use App\Models\RevendedorEstado;

class CidadesController extends Controller
{
    public function index(RevendedorEstado $estado)
    {
        $registros = $estado->cidades;

        return view('painel.revendedores.cidades.index', compact('estado', 'registros'));
    }

    public function create(RevendedorEstado $estado)
    {
        return view('painel.revendedores.cidades.create', compact('estado'));
    }

    public function store(CidadesRequest $request, RevendedorEstado $estado)
    {
        try {

            $input = $request->all();

            $estado->cidades()->create($input);

            return redirect()->route('painel.revendedores.estados.cidades.index', $estado)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(RevendedorEstado $estado, RevendedorCidade $registro)
    {
        return view('painel.revendedores.cidades.edit', compact('estado', 'registro'));
    }

    public function update(CidadesRequest $request, RevendedorEstado $estado, RevendedorCidade $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.revendedores.estados.cidades.index', $estado)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(RevendedorEstado $estado, RevendedorCidade $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.revendedores.estados.cidades.index', $estado)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
