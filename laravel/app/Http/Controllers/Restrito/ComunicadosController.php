<?php

namespace App\Http\Controllers\Restrito;

use App\Http\Controllers\Controller;
use App\Models\Restrito\Comunicado;

class ComunicadosController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            $comunicado = Comunicado::findOrFail($id);

            return $comunicado->hasPerfil
                ? response()->download($comunicado->filePath)
                : abort('404');
        }

        $comunicados = Comunicado::perfil()->ativos()->ordenados()->get();

        return view('frontend.restrito.comunicados.index', compact('comunicados'));
    }
}
