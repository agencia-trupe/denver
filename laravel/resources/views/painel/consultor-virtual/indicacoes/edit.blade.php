@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Conteúdo Técnico / Consultor Virtual / {{ $consultoria->titulo_pt }} /</small> Editar Indicação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.consultor-virtual.indicacoes.update', $consultoria->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.consultor-virtual.indicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
