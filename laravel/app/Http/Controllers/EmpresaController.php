<?php

namespace App\Http\Controllers;

use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        return view('frontend.empresa', [
            'empresa' => Empresa::first()
        ]);
    }
}
