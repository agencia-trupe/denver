@extends('frontend.conteudo-tecnico.template')

@section('main')

    <div class="perguntas-frequentes">
        <h1>
            <span>{{ t('nav.conteudo-tecnico') }}</span>
            {{ t('nav.faq') }}
        </h1>

        <div class="container">
            <div class="categorias">
                @foreach($categorias as $cat)
                <a href="{{ route('conteudo-tecnico.perguntas-frequentes', $cat->slug) }}" @if($cat->id === $categoria->id) class="active" @endif>
                    {{ tobj($cat, 'titulo') }}
                </a>
                @endforeach
            </div>

            <div class="perguntas">
                @foreach($perguntas as $pergunta)
                <div data-id="{{ $pergunta->id }}">
                    <h3>{{ tobj($pergunta, 'pergunta') }}</h3>
                    <p>{!! tobj($pergunta, 'resposta') !!}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
