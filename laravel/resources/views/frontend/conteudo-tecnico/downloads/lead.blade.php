<div class="downloads-lead-lightbox">
    <form action="{{ route('conteudo-tecnico.downloads.lead.post', $download->id) }}" method="POST" id="form-download-lead">
        <p>{{ t('conteudo-tecnico.download-email') }}</p>

        <input type="text" name="nome" placeholder="{{ t('contato.nome') }}" required>
        <input type="text" name="profissao" placeholder="{{ t('conteudo-tecnico.profissao') }}">
        <input type="email" name="email" placeholder="{{ t('contato.email') }}" required>
        <input type="text" name="telefone" placeholder="{{ t('contato.telefone') }}">

        <div class="checkboxes">
            <p>{{ t('conteudo-tecnico.segmento') }}:</p>
            <div class="checkboxes-grid">
                @foreach(\App\Models\DownloadLead::segmentos() as $s)
                <label>
                    <input type="checkbox" name="segmento[]" value="{{ $s }}">
                    {{ t('conteudo-tecnico.segmentos.'.$s) }}
                </label>
                @endforeach
            </div>
        </div>

        <div class="opt-in">
            <label>
                <input type="checkbox" name="opt-in" value="1" required>
                {{ t('conteudo-tecnico.opt-in') }}
            </label>
        </div>

        {!! csrf_field() !!}
        <button type="submit">{{ t('conteudo-tecnico.receber-material') }}</button>
    </form>
</div>
