@extends('frontend.common.template')

@section('content')

    <div class="restrito restrito-auth restrito-register">
        <h2>
            <span>Olá!</span>
            Cadastro
        </h2>

        {!! Form::open(['route' => 'restrito.register.post']) !!}
            <input type="hidden" name="token" value="{{ request()->get('token') }}">

            @if($errors->any())
            <div class="flash flash-error">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            <div class="restrito-register-wrapper">
                <div class="box">
                    <div class="row">
                        <input type="text" name="nome" placeholder="nome completo" value="{{ old('nome') }}" required>
                    </div>
                    <div class="row">
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cpf" placeholder="CPF" value="{{ old('cpf') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="empresa_nome_fantasia" placeholder="empresa - nome fantasia" value="{{ old('empresa_nome_fantasia') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="empresa_razao_social" placeholder="empresa - razão social" value="{{ old('empresa_razao_social') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cnpj" placeholder="CNPJ" value="{{ old('cnpj') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cep" placeholder="CEP" value="{{ old('cep') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="endereco" placeholder="endereço" value="{{ old('endereco') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="numero" placeholder="número" value="{{ old('numero') }}" required>
                        <input type="text" name="complemento" placeholder="complemento" value="{{ old('complemento') }}">
                    </div>
                    <div class="row">
                        <input type="text" name="bairro" placeholder="bairro" value="{{ old('bairro') }}" required>
                    </div>
                    <div class="row">
                        <input type="text" name="cidade" placeholder="cidade" value="{{ old('cidade') }}" required>
                        <input type="text" name="uf" placeholder="UF" value="{{ old('uf') }}" maxlength="2" required>
                    </div>
                </div>

                <div class="box">
                    @foreach(Tools::restritoPerfis() as $perfil => $label)
                    <label class="checkbox">
                        <input type="checkbox" name="perfil[]" value="{{ $perfil }}" @if((count(old('perfil')) && in_array($perfil, old('perfil')))) checked @endif>
                        <span></span>
                        {{ $label }}
                    </label>
                    @endforeach
                </div>

                <div class="box">
                    <div class="row">
                        <input type="password" name="password" placeholder="senha" required>
                    </div>
                    <div class="row">
                        <input type="password" name="password_confirmation" placeholder="confirmar senha" required>
                    </div>
                </div>

                <input type="submit" value="CADASTRAR">
            </div>
        {!! Form::close() !!}
    </div>

@endsection
