<?php

namespace App\Http\Controllers\Painel\Restrito;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArquivoController extends Controller
{
    public function show($dir, $file)
    {
        $filePath = storage_path('app/restrito/'.$dir.'/'.$file);

        if (file_exists($filePath)) {
            return response()->file($filePath);
        }

        return response('Arquivo não encontrado.', 404);
    }
}
