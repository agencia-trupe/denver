@extends('frontend.common.template')

@section('content')

    <div class="restrito restrito-auth restrito-login">
        <h2>
            <span>Olá!</span>
            Esqueci a senha
        </h2>

        <div class="box box-login">
            {!! Form::open(['route' => 'restrito.reset.email']) !!}
                @if(session('sent'))
                    <div class="flash flash-success" style="margin-bottom:0">{{ session('sent') }}</div>
                @else
                    @if($errors->any())
                    <div class="flash flash-error">{{ $errors->first() }}</div>
                    @endif
                    @if(session('error'))
                    <div class="flash flash-error">{{ session('error') }}</div>
                    @endif

                    <div class="row">
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    </div>
                    <div class="row">
                        <input type="submit" value="REDEFINIR SENHA">
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

@endsection
