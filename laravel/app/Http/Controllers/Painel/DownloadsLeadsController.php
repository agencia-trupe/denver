<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use App\Models\DownloadLead;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class DownloadsLeadsController extends Controller
{
    public function index()
    {
        $registros = DownloadLead::ordenados();

        $this->applyFilters($registros);

        $registros = $registros->paginate(30);

        return view('painel.downloads.leads.index', compact('registros'));
    }

    public function destroy(DownloadLead $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.downloads.leads.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function exportar()
    {
        $registros = DownloadLead::with('download')->ordenados();

        $this->applyFilters($registros);

        $registros = $registros->get()->map(function ($model) {
            return [
                'Data'      => $model->created_at->format('d/m/Y H:i'),
                'Nome'      => $model->nome,
                'Profissão' => $model->profissao,
                'Telefone'  => $model->telefone,
                'E-mail'    => $model->email,
                'Segmentos' => $model->segmento,
                'Material'  => $model->download->titulo_pt,
            ];
        });

        $fileName = 'Denver-Leads_'.date('d-m-Y_His');

        Excel::create($fileName, function ($excel) use ($registros) {
            $excel->sheet('leads', function ($sheet) use ($registros) {
                $sheet->fromModel($registros);
            });
        })->download('xls');
    }

    protected function applyFilters($query)
    {
        $inicio = null;
        $final  = null;

        try {
            if (request('data_inicio') && Carbon::createFromFormat('d/m/Y', request('data_inicio'))) {
                $inicio = Carbon::createFromFormat('d/m/Y', request('data_inicio'))->format('Y-m-d');
            }
            if (request('data_final') && Carbon::createFromFormat('d/m/Y', request('data_final'))) {
                $final = Carbon::createFromFormat('d/m/Y', request('data_final'))->format('Y-m-d');
            }
        } catch (\Exception $e) {
        }

        if ($inicio) {
            $query->whereDate('created_at', '>=', $inicio);
        }
        if ($final) {
            $query->whereDate('created_at', '<=', $final);
        }

        if ($segmento = request('segmento')) {
            $query->where('segmento', 'LIKE', "%$segmento%");
        }

        if ($material = request('material')) {
            $query->whereHas('download', function($q) use ($material) {
                $q->where('titulo_pt', 'LIKE', "%$material%");
            });
        }
    }
}
