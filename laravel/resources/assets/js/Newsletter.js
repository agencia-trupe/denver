export default function() {
    $('#form-newsletter').submit(function newsletter(event) {
        event.preventDefault();

        const $form = $(this);

        if ($form.hasClass('sending')) return false;

        $form.addClass('sending');

        return $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: {
                email: $form.find('input[name=email]').val(),
                perfil: $form.find('select[name=perfil] option:selected').val(),
            },
        })
            .done(data => {
                alert(data.message);

                $form[0].reset();
            })
            .fail(data => {
                const { responseJSON: res } = data;

                let txt = '';

                Object.keys(res).map(key =>
                    res[key].forEach(msg => {
                        txt += `${msg}\n`;
                    })
                );

                alert(txt);
            })
            .always(() => $form.removeClass('sending'));
    });
}
