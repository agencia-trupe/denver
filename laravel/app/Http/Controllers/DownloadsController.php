<?php

namespace App\Http\Controllers;

use App\Http\Requests\Restrito\DownloadsLeadsRequest;
use Illuminate\Support\Facades\Mail;

use App\Models\Download;
use App\Models\DownloadLead;
use App\Models\DownloadCategoria;

class DownloadsController extends Controller
{
    public function index(DownloadCategoria $categoria)
    {
        if (!$categoria->exists) {
            $categoria = DownloadCategoria::ordenados()->firstOrFail();
        }

        $categorias = DownloadCategoria::ordenados()->get();
        $downloads  = $categoria->downloads()->paginate(10);

        return view('frontend.conteudo-tecnico.downloads.index', compact(
            'categorias',
            'categoria',
            'downloads'
        ));
    }

    public function lead(Download $download)
    {
        if (! $download->categoria->exige_cadastro) {
            abort('404');
        }

        return view('frontend.conteudo-tecnico.downloads.lead', compact('download'));
    }

    public function leadPost(DownloadsLeadsRequest $request, Download $download)
    {
        if (! $download->categoria->exige_cadastro) {
            abort('404');
        }

        $data = $request->except('opt-in');

        $data['segmento']    = implode(', ', $data['segmento']);
        $data['download_id'] = $download->id;

        $lead = DownloadLead::create($data);

        $lead->load('download');

        Mail::send('emails.download', $lead->toArray(), function($m) use ($lead)
        {
            $m->to($lead->email, $lead->nome)
               ->subject('Arquivo para Download');
        });

        return response()->json([
            'message' => t('conteudo-tecnico.material-enviado')
        ]);
    }
}
