@extends('frontend.common.template')

@section('content')

    <div class="empresa">
        <div class="center">
            <div class="grid">
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_1) }}" alt="">
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_2) }}" alt="">
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_3) }}" alt="">
                <div>
                    <div>{!! tobj($empresa, 'texto_1') !!}</div>
                </div>
            </div>

            <div class="texto-2">{!! tobj($empresa, 'texto_2') !!}</div>

            <div class="texto-3">
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_4) }}" alt="">
                <div>{!! tobj($empresa, 'texto_3') !!}</div>
            </div>

            <div class="missao-visao-valores">
                <div>
                    <h2>{{ t('empresa.missao') }}</h2>
                    <p>{!! tobj($empresa, 'missao') !!}</p>
                </div>
                <div>
                    <h2>{{ t('empresa.visao') }}</h2>
                    <p>{!! tobj($empresa, 'visao') !!}</p>
                </div>
                <div>
                    <h2>{{ t('empresa.valores') }}</h2>
                    <p>{!! tobj($empresa, 'valores') !!}</p>
                </div>
            </div>

            <div class="box">
                <div>
                    <h2>{{ tobj($empresa, 'box_titulo') }}</h2>
                </div>
                <div>
                    <p>{!! tobj($empresa, 'box_texto') !!}</p>
                </div>
            </div>

            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_5) }}" class="full-width" alt="">
        </div>
    </div>

@endsection
