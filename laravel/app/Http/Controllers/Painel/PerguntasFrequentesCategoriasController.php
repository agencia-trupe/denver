<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerguntasFrequentesCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\PerguntaFrequenteCategoria;

class PerguntasFrequentesCategoriasController extends Controller
{
    public function index()
    {
        $categorias = PerguntaFrequenteCategoria::ordenados()->get();

        return view('painel.perguntas-frequentes.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.perguntas-frequentes.categorias.create');
    }

    public function store(PerguntasFrequentesCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            PerguntaFrequenteCategoria::create($input);
            return redirect()->route('painel.perguntas-frequentes.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(PerguntaFrequenteCategoria $categoria)
    {
        return view('painel.perguntas-frequentes.categorias.edit', compact('categoria'));
    }

    public function update(PerguntasFrequentesCategoriasRequest $request, PerguntaFrequenteCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.perguntas-frequentes.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(PerguntaFrequenteCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.perguntas-frequentes.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
