<?php

namespace App\Http\Controllers;

use App\Http\Requests\InteressesRecebidosRequest;

use App\Models\Evento;
use App\Models\Treinamento;
use Illuminate\Database\Eloquent\Model;

class CalendarioDeEventosController extends Controller
{
    public function index()
    {
        return view('frontend.conteudo-tecnico.calendario-de-eventos.index', [
            'eventos' => Evento::ordenados()->get(),
            'treinamentos' => Treinamento::ordenados()->get(),
        ]);
    }

    public function evento(Evento $evento)
    {
        return view('frontend.conteudo-tecnico.calendario-de-eventos.evento', compact('evento'));
    }

    public function treinamento(Treinamento $treinamento)
    {
        return view('frontend.conteudo-tecnico.calendario-de-eventos.treinamento', compact('treinamento'));
    }

    public function interesse(InteressesRecebidosRequest $request, Model $model)
    {
        $data = $request->all();
        $model->interesses()->create($data);

        return response()->json([
            'message' => t('contato.sucesso-interesse')
        ]);
    }
}
