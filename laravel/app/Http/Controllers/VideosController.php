<?php

namespace App\Http\Controllers;

use App\Models\Video;

class VideosController extends Controller
{
    public function index()
    {
        $videos = Video::ordenados()->paginate(12);

        return view('frontend.conteudo-tecnico.videos.index', compact('videos'));
    }
}
